import { Component, ChangeDetectionStrategy, OnInit, OnChanges, SimpleChanges, AfterViewInit, Input } from '@angular/core';

import * as JsPlugIns from './../../_js-utils';
import { MonitorData, MonitorAstronaut, CockpitCamera, StreamCommNoise, StreamConditions } from './../../app.interfaces';

// Use JQuery on this component...?
declare var $: any;
// Import JSON file, in '*.json' format, with 'require':
declare var require: any;

@Component({
    selector: 'monitor-comms',
    templateUrl: './monitor-comms.html',
    styleUrls: ['./monitor-comms.css']
    // changeDetection: ChangeDetectionStrategy.OnPush
})
export class MonitorCommsComponent implements OnInit, OnChanges, AfterViewInit {

    // Our Database ;-) for a complex and extense network of communications, with CAMERAS and comm NOISES, we just set up from Mars to Earth.
    communicationsData = require('./../../_data/communications-network.json');
    // Rover's Cockpit and Mars set of Cameras, that will [stream VIDEO] at top monitors of this Class:
    setOfCameras = this.communicationsData.cameras as Array<CockpitCamera>;
    // Communications noise, that can exist, or not, on each stream of VIDEO, that will affect top SONY monitor, and big SONY Video Hall screen, image clearence:
    communicationsNoise = this.communicationsData.commNoises as Array<StreamCommNoise>;

    // The 2 types of information that this Class monitors will render on their '.screen's (HTML tag)
    @Input() private dataToPrint: MonitorData;
    @Input() private streamToVideo: MonitorAstronaut;

    initMsg = 'Angular\'s \'MonitorComms\' mounted.';
    initRenderingMsg = 'Listening to \'App\' data...';

    constructor() { }

    ngOnInit() {
        // 1st message on bottom monitor:
        this.msgToSanyoDataMonitor(this.initMsg);
    }

    ngOnChanges(propChanged: SimpleChanges) {

        if (propChanged && propChanged.dataToPrint && !propChanged.dataToPrint.firstChange) {
            this.printData(this.dataToPrint);
            // Maybe the user is watching Rover's journey DATA => always pull monitor content down, when overflow starts:
            this.pullBottomMonitorMessagesDown();
        }

        if (propChanged && propChanged.streamToVideo && !propChanged.streamToVideo.firstChange) {
            this.streamVideo(this.streamToVideo);
            // Maybe the user is watching Rover's journey through the big VIDEO hall! => update camera switching
            // Welll... now its ALWAYS updated (on this.switchToCamera()), independently of being pull down or not.
        }
    }

    ngAfterViewInit() {
        this.msgToSanyoDataMonitor(this.initRenderingMsg);
        this.initMonitorControlDropdowns();
    }

    /**
     * Always pull DATA monitor content down, when overflow starts
     */
    pullBottomMonitorMessagesDown() {
        const
            monitorContent = this.getHTMLscreen('toPrintData')
            , height = monitorContent.clientHeight
            , scrollHeight = monitorContent.scrollHeight
            // And so:
            , vpHeight = scrollHeight - height
            // max height (in px) of the blocks of writing that are coming in, through the 'monitor-cable':
            , delta = 600
            , isScrolledToBottom = vpHeight <= monitorContent.scrollTop + delta
        ;
        if (isScrolledToBottom) {
            monitorContent.scrollTop = vpHeight;
        }
    }

    /**
     * Sets the Cockpit camera that '.monitor-video-streamer' will be streaming, on each user choice.
     * 'evt' parameter, brings "data-camera" value, of each of the HTML option element clicked
     *
     * Switch to the chosen one - we will apply no noise communications, since we are on mother Earth! ;-)
     * and show it also on Video Hall Monitor - if it's pulled down.
     * Also style clicked HTML item as active.
     *
     * @param evt the BS4 Dropdown selector clicked option
     */
    setMonitorControlToCamera(evt: MouseEvent) {
        const
            optionChosen = evt.target as HTMLElement
            , allOptions = [].slice.call(optionChosen.parentElement.children)
            // So, with
            , cameraId = Number(optionChosen.dataset.camera)
            // we setup specific stream conditions for this Dropdown camera switch
            // NOTE that, allthough nId: null, you'll ALWAYS have a quick 'flick' of COMMUTATION noise - not communication one.
            , sonyDropdownStream = { cId: cameraId, nId: null, tImg: 0 }
            // ;-)
            , funStream = { cId: cameraId, nId: 2, tImg: 2000}

        ;
        // We:
        this.switchToCamera(sonyDropdownStream);
        // ... and already updates monitorBigVideoHall

        // Finally DOM concerning:
        allOptions.forEach( (option) => {
            option.className = option.className.replace(' active', '');
        });
        optionChosen.className += ' active';

        // Wait! Have some fun with the user...! ;-)
        // 5000 ms after camera's user choice was made, if he/she is still looking at the monitor...
        // background img will vanish from the screen (for 2000 ms), and a HUGE (2) noise will start!!!! ;-)
        // Wait! ;.) Make this happening NOT always, but randomly to chosen "cameraId"!
        const
            cameras = this.setOfCameras
            , camerasIdArray = cameras.map((camera) => camera.id)
            , randomCameraId = camerasIdArray[Math.floor(Math.random() * cameras.length)]
            // Must initialize noise, so previous img doesn't come back:
            , timeBeforeRandomNoise = 4000
            , uniqueTimerId = `#${new Date().getTime()}`
        ;
        // console.log(randomCameraId, cameraId);
        if (randomCameraId === cameraId) {
            // setTimeout(    <= better! Use 'waitForFinalEvent()' plug-in!
            // Don't let previous choice (if random noise was out for that camera) influence (add noise) eventualy another choice, with no noise randomly attributed
            // JsPlugIns.waitForFinalEvent (
            //     () => this.switchToCamera(funStream)
            // , timeBeforeRandomNoise, uniqueTimerId);
        }
    }

    /**
     * Once big SONY Video Hall monitor needs/we want to pull it down/up (including from AppComponent!)
     * one MUST come to this next Method and NEVER do JQuery/JS malabarices - here we know DOM is ready; from other "places" we don't! ;-)
     */
    pullVideoHallMonitor(to: 'down' | 'up') {
        switch (to) {
            case 'down':
                $('#monitor-video-hall').addClass('is-down');
                break;

            case 'up':
                $('#monitor-video-hall').removeClass('is-down');
                break;

            default:
                break;
        }
    }
    // Just in case you want to have the facility of "click outside" (BS4 Dropdown) to pull up the Video Sony Hall Monitor
    clickJoystickMonitor() {
        $('.joystick-dropdown-wrapper > .button-control').click();
    }

    /**
     * Delete all Data Monitor messages currently on 'section.monitor-screen-printer > pre' bottom Monitor
     * Its a 'monitor-cable' business: order it!
     */
    deleteDataContent() {
        this.getHTMLscreen('toPrintData').innerHTML = '<div class="pre-div-content">' + this.initMsg + '</div>';
    }

    // ====================
    // AUX funtions:
    // ====================

    /**
     * Get each of the screens of the monitors hold by this Class
     *
     * @param whatFor:
     *     'toPrintData' SANYO - will print JSON data and messages exchanged between all envolved Components)
     *     'toStreamVideo' SONY  - will render video streams from Mars)
     *     'toVideoHall' VIDEO HALL - a big wide SONY screen, to user play with while following the entire expedition to Mars)
     */
    private getHTMLscreen (whatFor: 'toPrintData' | 'toStreamVideo' | 'toVideoHall'): HTMLElement {
        switch (whatFor) {
            case 'toPrintData':
                return document.querySelector(`section.monitor-screen-printer > pre`);
            case 'toStreamVideo':
                return document.querySelector(`section.monitor-video-streamer > div.screen`);
            case 'toVideoHall':
                return document.querySelector(`#monitor-video-hall  div.screen`);

            default:
                return null;
        }
    }

    /**
     * Print previous content + separator + jsonToPrint (msg + data) Data, using a JS stringify beautifier plug-in,
     * into SANYO "this.getHTMLscreen('toPrintData')" HTML's tag monitor.
     *
     * @param jsonToPrint - what to print NEW, cumulated with PREVIOUS, that can have a string msg and/or a specific type of JSON data (see the interface Type!)
     */
    private printData(dataToPrint: MonitorData) {
        // TEST - printing what's coming from our high-teck, universe-wide, "network-data-point", were/whenever the expedition point is going through right now!
        // const regexHTMLfilter = /(<([^>]+)>)/ig;
        // console.error('"network-DATA-POINT" printing IN!', dataToPrint.msg.replace(regexHTMLfilter, ''), dataToPrint.data);

        const
            // Header wit the string 'msg' to the previous (and still current) data on the screen:
            cumulatedData = this.cumulateData(dataToPrint.msg)

        ;
        if (dataToPrint.data === null) {
            // No JSON 'data' was passed in to Print. What about 'msg'...?
            dataToPrint.msg !== null ?
                this.doPrintData(cumulatedData, null)
                :
                this.doPrintData(`<div class="pre-div-content">ERROR...? No 'msg' or 'data' were provided!</div>`, null)
            ;
        } else {
            // We have JSON 'data' for sure! 'msg' is not important - if null was aready converted to '':
            this.doPrintData(cumulatedData, dataToPrint.data);
        }
    }

    /**
     * Adds previous Data content to a 'separator', followed by 'dataMsg' msg, if any
     * (JSON's dataToPrint.data is NOT cumulated here)
     *
     * @param fromElement - the HTML tag container to print data to
     * @param dataMsg - to add at the end of 'fromElement' content, if any
     */
    private cumulateData (dataMsg: string) {
        const separator = '<span class="separator"><hr></span>';
        return this.getHTMLscreen('toPrintData').innerHTML + separator + (dataMsg !== null ? `<div class="pre-div-content">${dataMsg}</div>` : '');
    }

    private doPrintData(str: string, jsonData: any) {
        this.getHTMLscreen('toPrintData').innerHTML =  jsonData ? str + JsPlugIns.prettyPrintJson(JSON.stringify(jsonData, undefined, 4)) : str;
    }

    // Say to dear users, Sony screen is up and ready to go!
    private msgToSanyoDataMonitor(msg: string) {
        setTimeout(() => {
            this.printData(
                { msg: msg, data: null }
            );
        }, 2000);

    }

    /**
     * Get each of the Rover's Cockpit camera, by its Id.
     *
     * @param cameraId the cockpit's camera Id
     */
    private getCockpitCameraById (cameraId: number): CockpitCamera {
        return this.setOfCameras.filter(
            (eachCam: CockpitCamera) => eachCam.id === cameraId
        )[0];
    }

    /**
     * Get any Camera by it's DOM class name.
     *
     * @param className the cockpit's camera DOM class name
     */
    private getCockpitCameraByClassName (className: string): CockpitCamera {
        return this.setOfCameras.filter(
            (camera: CockpitCamera) => camera.cssImgClass === className
        )[0];
    }
    /**
     * Get each type of noise any stream can have.
     * Communications noise, that can exist, or not, on each stream of VIDEO, will affect top SONY monitor image clearence:
     * @param noiseId the cockpit's camera noise Id
     */
    private getStreamCommNoiseById (noiseId: number): StreamCommNoise {
       return this.communicationsNoise.filter(
           (eachNoise) => eachNoise.id === noiseId
        )[0];
    }

    /**
     * Get any noise duration (prop "cssAnimationTime") IN MILISECs,
     * by it's known Id
     *
     * "cssAnimationTime" property is (at "communications-network.json") a string with an arythmetic calculous to be done,
     * returning a time in SECONDS => transform into MILISECs
     *
     * <network-video-point /> will come here @ "broadcastOrbitingInitOfTimes()"
     *
     * @param noiseId the cockpit's camera noise Id
     */
    getStreamCommNoiseDurationById(noiseId: number): number {
        return(
            // It's a trusted source, TSLINT - believe me! I know it's dangerous, but let me use eval(), for christ sake! https://stackoverflow.com/a/18082175/2816279
            // tslint:disable-next-line:no-eval
            eval (
                this.communicationsNoise.filter(
                    (commNoise: StreamCommNoise) => commNoise.id === noiseId
                )[0].cssAnimationTime
            ) * 1000
        );
    }

    /**
     * Stream each step of Rover's expedition on Mars,
     * rendering the Cockpit's switched camera that is "on" now.
     * This camera switch happens on 'space-expedition' at each appropriated step.
     *
     * It's up to Rover to emmit to us on which camera it's broadcasting now, from Mars,
     * depending on the MonitorAstronaut data.
     *
     * @param ofThisFrame each Rover's movement of interest, streamed from Mars
     */
    private streamVideo(ofThisFrame: MonitorAstronaut) {
        // TEST - screening what's coming from our high-teck, universe-wide, "network-video-point", were/whenever the expedition point is going through right now!
        // console.error('"network-VIDEO-POINT" streaming IN!', ofThisFrame);

        const
            videoScreen = this.getHTMLscreen('toStreamVideo')
            , roverAstronaut = videoScreen ? videoScreen.querySelector('.rover-astronaut') : null
            , isFirstChange = ofThisFrame.frameToStream.astronautStatus.firstChange
        ;
        if (!roverAstronaut) { console.error('ERROR: HTML does NOT have "section.monitor-video-streamer > div.screen" selector...'); return; }

        // Update big Video HAll monitor sub titles:
        this.renderSubTitles(ofThisFrame.frameLabel.localTime, ofThisFrame.frameLabel.text);

        let effectsCsstimer: number;

        if (ofThisFrame.frameToStream.astronautStatus.isOnOrbiting) {
            // Welcome to Sony Monitor, Mars from outter space! ;-)
            this.switchToCamera(ofThisFrame.frameToStream.videoScenario.videoStreamSetUp);
        } else {
            // Stop orbiting, Astronaut!
            // Well... Astronauts do not orbit; it's planes and shuttles that Orbit!
        }

        if (ofThisFrame.frameToStream.astronautStatus.isOnCockpit) {
            // Welcome to Sony Monitor, Automatic Pilot from the (just new born, fake Rover replacer) Rover's Cockpit! ;-)
            this.switchToCamera(ofThisFrame.frameToStream.videoScenario.videoStreamSetUp);
        } else {
            // Stop being on Cockpit, for some auto manouvers, Astronaut!
            // Well... Astronauts are NOT automatic pilots... these are computer machines and former is a human being!
        }

        if (ofThisFrame.frameToStream.astronautStatus.isDeployed) {
            // Its first streaming, if Astronaut has just 'isDeployed' => Astronaut gets on tely and we have to set CSS animation duration!
            if (isFirstChange) {
                effectsCsstimer = ofThisFrame.frameToStream.videoScenario.cssTimer;
                const
                    walkCSSduration: string = ((effectsCsstimer * 2) / 1000).toFixed(2) + 's'
                    , deploymentPlaceColor = ofThisFrame.frameToStream.astronautPos.deploymentPlace.color

                ;
                // Welcome to Sony Monitor, Astronaut! ;-)
                roverAstronaut.querySelector('i.fas').setAttribute('style', 'display: inline-block; animation-duration: ' + walkCSSduration);
                // Astronaut pace on each step, doesn't have to be the same as each Rover's step;
                // all we want is for the Astroanut to walk, on Mars, and Rover to keep following him, by air.
                //
                // REMEMBER we'll always need to very much tune EACH of this CSS effects, in order to obtain a realistic effect!

                // Change CSS coloring Mars soil, by updating 'root:' CSS var
                // - that supplies 'box-shadow' color, on selector 'div.screen > div.rover-astronaut.is-deployed:before'
                setTimeout(() => {
                    document.documentElement.style.setProperty('--jpf-deployment-video-color-on', 'var(--' + deploymentPlaceColor + ')');
                }, effectsCsstimer / 2);
            }
            // Put a little deployment point "mark" underneath, Astronaut! And switch camera
            roverAstronaut.classList.add('is-deployed');
            this.switchToCamera(ofThisFrame.frameToStream.videoScenario.videoStreamSetUp);

        } else {
            // Take off your deployment point "mark" underneath, Astronaut!
            roverAstronaut.classList.remove('is-deployed');
        }

        if (ofThisFrame.frameToStream.astronautStatus.isExploringMars) {
            if (isFirstChange) {
                // Everything ("animation-duration") was set for Astronaut to walk on Mars!
            }
            // Start walking, Astronaut!
            // Put a little deployment point "mark" underneath, Astronaut! And switch camera
            roverAstronaut.classList.add('is-walking-on-mars');
            roverAstronaut.classList.add('is-deployed');
            this.switchToCamera(ofThisFrame.frameToStream.videoScenario.videoStreamSetUp);

        } else {
            // Stop walking, Astronaut!
            roverAstronaut.classList.remove('is-walking-on-mars');
        }

    }

    /**
     * First thing, disconnect all Rover's Cockpit cameras from the screen, and remove any existent communication noise.
     * Then refresh 'monitor-comms' top Monitor screen - set streaming noise, first, if caller so wishes to.
     *
     * NOTE that you will ALWAYS see a quick 'flick' of noise,
     * even if the caller sets 'nId' to null, on the passed StreamConditions - that's the REALISTIC idea! A switch ALLWAYS brings noise - this time by COMMUTATION act! ;-)
     * Why? As HTML monitor CONTAINER has the CSS noise @ its background img,
     * once code calls "this.removeCameraAndNoiseFromScreen", the current 'in front' camera CSS Class (CONTENT background img) is removed => you see CONTAINER's noise.
     */
    private switchToCamera(ofThisStream: StreamConditions) {
        const
            videoScreen = this.getHTMLscreen('toStreamVideo')
            , wantedCamera = this.getCockpitCameraById(ofThisStream.cId)
            , wantedNoise = ofThisStream.nId ? this.getStreamCommNoiseById(ofThisStream.nId) : null

            , timeToScreenSwith = ofThisStream.tImg
            , uniqueTimerId = `Switch Id=${new Date().getTime()}`

        ;
        this.removeCameraAndNoiseFromScreen(videoScreen);

        // Make it assynchronous - DOM needs to settle previous monitor's screen cleaning:
        setTimeout(() => {
            if (wantedNoise) {
                videoScreen.classList.add(wantedNoise.cssNoiseEffectClass);
            } else {
                videoScreen.parentElement.classList.add('is-clean-of-noise');
            }
            // Don't let previous img choice influence eventualy another camera switch:
            JsPlugIns.waitForFinalEvent ( () => {
                videoScreen.classList.add(wantedCamera.cssImgClass);
                // Parent conditions return to default state, whatever asked for:
                videoScreen.parentElement.classList.remove('is-clean-of-noise');

                // Finally always keep "monitorBigVideoHall", independent of being pull down, or not, updated with Sony top Monitor images/classes:
                // (just give it time to do above lines DOM changes:)
                JsPlugIns.waitForFinalEvent ( () => {
                    this.updateMonitorBigVideoHall(`Camera #${wantedCamera.id}`, `"${wantedCamera.name}"`);
                }, 1000, uniqueTimerId);

            }, timeToScreenSwith, uniqueTimerId);
        }, 0);
    }

    /**
     * Cleans the VIDEO monitor screen from camera streaming (background Img)
     * and any communications noise.
     *
     * @param thisMonitor the '.screen' to clean
     */
    private removeCameraAndNoiseFromScreen (thisMonitor: HTMLElement) {
        for (let n = 0; n < this.setOfCameras.length; n++) {
            const each: CockpitCamera = this.setOfCameras[n];
            thisMonitor.classList.remove(each.cssImgClass);
        }
        for (let n = 0; n < this.communicationsNoise.length; n++) {
            const each: StreamCommNoise = this.communicationsNoise[n];
            thisMonitor.classList.remove(each.cssNoiseEffectClass);
        }
    }

    /**
     * Set BIG Video Hall Monitor's screen - it's ALWAYS the same as getHTMLscreen('toStreamVideo') current one!
     * Only bigger and zoomed...
     * ... and now with inputted left/right strings for the sub-titles div.
     *
     * Remove all classes from monitorBigVideoHall
     * Apply the ones in videoTopMonitor
     */
    private updateMonitorBigVideoHall(textLeft: string, textRight: string) {
        const
            videoTopMonitor = this.getHTMLscreen('toStreamVideo')
            , currentImgClass = videoTopMonitor.classList
            , monitorBigVideoHall = this.getHTMLscreen('toVideoHall')

        ;
        monitorBigVideoHall.classList.forEach(
            (videoHallClass) => monitorBigVideoHall.classList.remove(videoHallClass)
        );
        currentImgClass.forEach(
            (currentClass) => monitorBigVideoHall.classList.add(currentClass)
        );

        this.renderSubTitles(textLeft, textRight);

    }

    /**
     * Do not let its BS4 Dropdown container to close, on clickig inside.
     * Video hall Monitor should follow Dropdown state, going up/down with it
     */
    private initMonitorControlDropdowns() {

        $('#joystick-pad + .dropdown-menu').on('click', function (evt: MouseEvent) {
            evt.stopPropagation();
        });

        $('.joystick-dropdown-wrapper').on('show.bs.dropdown', (evt: MouseEvent) => {
            this.pullVideoHallMonitor('down');
        });
        $('.joystick-dropdown-wrapper').on('hide.bs.dropdown', (evt: MouseEvent) => {
            this.pullVideoHallMonitor('up');
        });
    }

    /**
     * We now have sub titles to render on big Sony Video Hall Monitor!
     * They will fade-out, like REAL TV sub titles!;-)
     *
     * Their content can be related to each stream of video of the Astronaut/Rover/Shutle on the expedition
     * ('frameLabel' prop of MonitorAstronaut type data object) IF the screen is of THAT camera
     * or updated with current switched camera Id and Name.
     *
     * @param textLeft what's to render on left part of the Bib Sony Video Hall monitor
     * @param textRight what's to render on right part of the Bib Sony Video Hall monitor
     */
    private renderSubTitles (textLeft: string, textRight: string) {
        const
            currentLastEnteringStream: MonitorAstronaut = this.streamToVideo
            , videoHallScreen = this.getHTMLscreen('toVideoHall')
            , classArray = videoHallScreen.classList
            , uniqueTimerId = `Render number #${new Date().getTime()}`
            // The sub titles DOM containers envolved:
            , videoHallContainer = videoHallScreen.parentElement.parentElement
            , subTitlesVideoHall = videoHallContainer.querySelector('section.monitor-controls > div.sub-titles-container > .sub-title-label')
            , frameLabelTime = subTitlesVideoHall.querySelector('.label-time')
            , frameLabelText = subTitlesVideoHall.querySelector('.label-text')

        ;
        // Find which cameraId is currently on big Sony Video Hall.
        // If it's NOT the "currentLastEnteringStream" one (user might be playing with camera's switch!), DON'T show the stream frame label - show Camera ID + Name
        //
        // There's a "certain" (0) time, since "switchToCamera()" does "videoScreen.classList.add(wantedCamera.cssImgClass);" DOM changes ASSYNCHRONOUSLY
        // (already with a setTimout()) => use a callback, after "some" DOM time:
        this.getCurrentVideoHallCamera (classArray, (currentCamera: CockpitCamera | null) => {
            let textToRenderAtLeft: string, textToRenderAtRight: string;

            if (currentCamera) {
                if (currentCamera.id === currentLastEnteringStream.frameToStream.videoScenario.videoStreamSetUp.cId) {
                    textToRenderAtLeft = currentLastEnteringStream.frameLabel.localTime;
                    textToRenderAtRight = currentLastEnteringStream.frameLabel.text;
                } else {
                    textToRenderAtLeft = textLeft;
                    textToRenderAtRight = textRight;
                }
                // Options were made - either show current video stream 'frameLabel' text, or a fixed (Cmera # + Name), here inputted, one:
                frameLabelTime.innerHTML = textToRenderAtLeft;
                frameLabelText.innerHTML = textToRenderAtRight;
                // Help small screens; Mobiles will have to read them at home! ;-)
                subTitlesVideoHall.setAttribute('title', textToRenderAtLeft + ' ' + textToRenderAtRight);

                // On fade-out, remove any content - will be seen, if not removed, once 'fade-out' class is removed!
                const timeToRemoveSubTitles = 6501;
                // Upper must be HIGHER than coded CSS 'transition-delay' + 'transition-duration' so fade-out effect can entirely be seen by user
                subTitlesVideoHall.classList.add('fade-out');

                // Must remove 'fade-out' class... it will be applied everytime method is called!
                JsPlugIns.waitForFinalEvent ( () => {
                    frameLabelTime.innerHTML = '';
                    frameLabelText.innerHTML = '';
                    subTitlesVideoHall.classList.remove('fade-out');
                }, timeToRemoveSubTitles, uniqueTimerId);
            }
        });
    }

    private getCurrentVideoHallCamera (classArray: DOMTokenList, callback: any) {
        const uniqueTimerId = `Fetching camera try #${new Date().getTime()}`;

        JsPlugIns.waitForFinalEvent ( () => {
            for (let n = 0; n < classArray.length; n++) {
                const currentCamera: CockpitCamera = this.getCockpitCameraByClassName(classArray[n]);
                callback(currentCamera ? currentCamera : null);
            }
        }, 500, uniqueTimerId);
    }
}
