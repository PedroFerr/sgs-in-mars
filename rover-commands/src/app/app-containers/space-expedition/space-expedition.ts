import { Component, OnChanges, AfterViewInit, Input, Output, EventEmitter, ViewChild, ElementRef, Renderer2 } from '@angular/core';

import { GPS, VehicleInstructions, UnitPathMove, JourneyRoadMap, JourneyRoadMapInitializer, MoveReport, LandingOperationsData } from '../../app.interfaces';

// Use JQuery on this component...?
declare var $: any;

@Component({
    selector: 'space-expedition',
    templateUrl: './space-expedition.html',
    styleUrls: ['./space-expedition.css']
})
export class SpaceExpeditionComponent implements OnChanges, AfterViewInit {

    // Grab this component's HTML - it wlll be manipulated a lot - and our great 'exploring-vehicle' star #rover
    tpl: HTMLElement;
    rover: HTMLElement;

    // First of all, we have to land our 'shuttle-conveyor', orbiting on 'deployed-expedition' @ Mars 'planet'. Next is an Angular's component Input to it:
    sendDataToShuttleConveyor: LandingOperationsData;

    // ============================================================================================

    // Receive instructions, from app-root, containing a line of driving commands, a restricted area to drive and the Deployment bulls eye coordinates:
    @Input() instructions: VehicleInstructions;

    // Say to the world (via <network-data-point />) VehicleInstructions just arrived from 'app' component, with a previous little preparation, so it can be screened:
    preparedToPrintVehicleInstructions: VehicleInstructions;

    // And the deployment center, where the entire gear will be deployed @ Mars, and Rover will drive from:
    // Will be override, now we have 'mars-places.json' - its value is now coming on EACH (ngOnChanges()) set of Instructions, set up by the user
    marsDeployedPos = { } as GPS;

    // ============================================================================================

    // Once the entire gear gets deployed on 'planet', and having to do with the consecutive user planned journeys submission,
    // we have to keep sending to the 'deployed-expedition' what is the begining data to start exploring (not only the init position...)
    currentJourneyRoadMap: JourneyRoadMap;
    // ... and, in the end of each journey, the same "currentJourneyRoadMap" should return, but updated by 'exploring-vehicle's Engine and Cockpit:
    endOfJourneyRoadMap: JourneyRoadMap;

    // ============================================================================================

    // In the end of each journey, on 'exploring-vehicle's last "eachMove", sent back final journey results:
    @Output() private finishExplorationJourney = new EventEmitter<MoveReport>();

    // ============================================================================================

    // The entire road map of all Rover's journeys, is being plotted HERE, on an extended/shrinked layer, by click. Once you click outside of it, should close.
    isPlotterExtended = false;
    @ViewChild('plotterData') plotterData: ElementRef;

    private areRulersOn: boolean;

    // 'planet' can suffer several CSS transform (translate, rotate and scale) by user set of clicks.
    // At some crucial (landing, most) moments, we should not allow for user to alter planet fixed coordinates - we disable "section.ui-ux-controller > buttons-container":
    clickedTransformation: { askedMovement: string, cumulateClicksTillTime: number };
    enableToClickTransformationButtons = false;

    constructor(
        private thisDOM: ElementRef,
        private renderer: Renderer2
    ) {
        this.renderer.listen('window', 'click', (evt) => {
            // If clicked OUTSIDE:
            if (evt.path.indexOf( this.plotterData.nativeElement ) === -1) {
                // Was it open?
                if (this.isPlotterExtended === true) {
                    this.clickToExpandRoadMap();
                }
            }
        });
    }

    ngOnChanges() {

        if (this.instructions && this.instructions.charCommand) {
            /**
             * Let's start some action!
             *
             * We have a few things to be done: ;-)
             *    1) Update #rover CSS 'transition-duration' to the user inputted 'lagTime' value
             *       (Rover's astronaut explorer animation will be set-up @ 'monitor-comms')
             *    2) Set our deployment point in Mars
             *    3) Take a few seconds to prepare initial "this.position", so CSS can work creating a "land operations" animation.
             *       This time will be important for Rover's both Cockpit (timmed moves) and icon animation, according to the options user did at 'command-center' Input form.
             *       One of its (1st of 2) moments is when Rover's Astronaut is considered deployed on Mars.
             *       The end of this 'cockpitMoments.totalLandRoverOnMarsTime' time will dictate the "startJourney()" call => Astronaut will REALLY start walking on Mars
             *       And at the end of each of these journeys, 'exploring-vehicle' will emit back to "this.receiveEndOfJourneyReport()", passing, again, trough all components.
             *    4) Design our grid, placing it @ initial Rover's "this.position" - on {0,0} draw our restricted area square
             *    5) Turn rulers on, so sympathetic user can appreciate the outmost incredible work done here!
             *    6) Send a DATA comm, via 'app', saying Rover has received Instructions
             *       - it's just about to "startJourney()" @ 'exploring-vehicle's ngOnChanges on "startExplorationJourney" received boolean
             *    7) Disable each of "section.ui-ux-controller > buttons-container", so user can not transform the 'planet' CSS
             *       till Astronaut is deployed on planet soil @ 'exploring-vehicle'
             */
            // 1)
            // DONE @ 'exploring-vehicle', just before "this.startJourney();" @ ngOnChanges() ------------
            // this.renderer.setStyle(this.rover, 'transition-duration', efectsCSStimer);

            // 2)
            this.marsDeployedPos = Object.assign( {}, this.instructions.deployment.coordinates);

            // 3)
            // Send "this.currentJourneyRoadMap" right to Rover's 'exploring-vehicle' - so each journey step can be registered, streamed, REALLY seen, etc.
            // Check the cumulating journeys flag set (or not!) by the user on this journey ('command-center' form's radio click):
            if (this.instructions.isToCumulateJourneys === 'false' as unknown as boolean) {
                this.currentJourneyRoadMap = Object.assign( {}, JourneyRoadMapInitializer,
                    { journeysMoves: [] }
                    , { lastPos: this.instructions.roverPosition }
                );
            } else {
                this.currentJourneyRoadMap = Object.assign( {}, JourneyRoadMapInitializer, this.endOfJourneyRoadMap );
            }

            // Next one, "this.sendDataToShuttleConveyor", is to be delivered at 'vehicle-fleet-operations', for the landing 0perations animation.
            const shuttleInstructions: LandingOperationsData = {
                    startingPointOnPlanet:  this.currentJourneyRoadMap.lastPos
                    , speedOnPlanet: this.instructions.roverLimits.lagTime
                    , isToCumulateJourneys: this.instructions.isToCumulateJourneys
                }
            ;
            // As so, we can land Shuttle on the right spot, with a perfect timming.
            // this.sendDataToShuttleConveyor = shuttleInstructions;
            // Mind you, user can keep pressing submit, with no changed data. If "isTCumulateJourneys", we should see Rover moving from last journey's arrived point
            // => "this.sendDataToShuttleConveyor" has to be always on a different memory position, to assure ngOnChanges() will act on next children components
            this.sendDataToShuttleConveyor = Object.assign( {}, shuttleInstructions);

            // 4)
            // DONE @ 'deployed-expedition'! ------------
            // this.doMarsGrid(cockpitMoments.totalLandRoverOnMarsTime);

            // 5)
            if (!this.areRulersOn) { setTimeout(() => $('ui-switch').click(), 1000); }

            // 6)
            this.preparedToPrintVehicleInstructions = Object.assign( {}, this.instructions, {
                // Transform 'true'/'false' into real boolean, so coloring works @ "monitor-comms" bottom Monitor
                isToCumulateJourneys: (this.instructions.isToCumulateJourneys.toString() === 'true')
                // At the same Data monitor, the command units string, once into an Array, get a lot of vertical space! => get it back to a plain string property:
                , charCommand: '[' + this.instructions.charCommand.toString() + ']'
            });

            // 7)
            this.enableToClickTransformationButtons = false;
        }

    }

    ngAfterViewInit() {
        this.tpl = this.thisDOM.nativeElement.querySelector('main');
    }

    receiveEndOfJourneyReport (lastJourneyStepReport: MoveReport) {
        // Update JourneyRoadMap values with THIS journey expedition ones, just arrived from LAST 'exploring-vehicle's Cockpit move step:
        this.endOfJourneyRoadMap = lastJourneyStepReport.journeyRoadMap;

        // Keep emiting back, till it reaches 'app' component:
        this.finishExplorationJourney.emit(lastJourneyStepReport);
    }

    /**
     * This next method is EXCLUSIVELY called from where Rover's movements on 'planet' are effectively being drawn:
     * whatelse if not from 'exploring-vehicle'...?
     *
     * NOTE that this Input params are DYNAMIC! They keep changing on every movement/step Rover does on planet!
     * So, on EACH step, 'exploring-vehicle' is coming here to an update of the "Rover's Road Map" listing of movements.
     *
     * @param nthCommand the 'nth' letter 'exploring-vehicle' is, now, being interpreted by Cockpit, from submitted VehicleInstructions,
     *                     in a "for (let nCommandChar = 0; nCommandChar < this.instructions.charCommand.length; nCommandChar++) {...}" cycle
     * @param thisJourneyMoveData concerning this particular set of commands entered by the user, Rover has reached this Data, after EACH move.
     *                           props "lastPos" and "lastPosIndex", as being updated by Cock+pit's movement, reflect, allready, current real position
     */
    plotJourneyJourneyRoadMap(nthCommand: number, thisJourneyMoveData: MoveReport) {
        const
            plotterData = this.tpl.querySelector('.plotter-data')
            , currentRoadMap = thisJourneyMoveData.journeyRoadMap.journeysMoves
            // Mind you, "journeysMoves" array is ALWAYS entirely computed by Mercedes Engine, as soon as Instructions have arrived @ 'exploring-vehicle'
            , roadMapNmove = thisJourneyMoveData.journeyRoadMap.lastPosIndex

        ;
        // Now, inside the journey that is currently happening, what we need to plot this "nthCommand" JourneyRoadMap:
        const
            position = currentRoadMap[roadMapNmove].to
            , iconCheck = thisJourneyMoveData.isRestrictedAreaCrossed ?
                '<i class="far fa-times-circle" style="color: var(--danger);"></i>' : '<i class="far fa-check-circle" style="color: var(--success);"></i>'
        ;
        if (nthCommand === 0) { // 1st move of a journey - either cumulated or not - from the just received instructions:
            plotterData.innerHTML = plotterData.innerHTML +
                ( (roadMapNmove === 0) ?
                    `<div class="plotter-line blockquote-footer">GO from INIT @ `
                    :
                    `<div class="plotter-line blockquote-footer">GO from PREVIOUS @ `
                ) + `(${this.instructions.roverPosition.coordinates.x}, ${this.instructions.roverPosition.coordinates.y}) `
                        + `<span class="text-primary">${this.instructions.roverPosition.orientation}</span>`
                        + `<span class="deployment-mars-place bg-${this.instructions.deployment.color} text-white" title="${this.instructions.deployment.place}">`
                            + `${this.instructions.deployment.place}`
                        + `</span>`
                    + `</div>`
            ;
        }

        plotterData.innerHTML = plotterData.innerHTML +
            `<div class="plotter-line">`
                + `<span>#${roadMapNmove + 1}</span>`
                + `<span>${this.instructions.charCommand[nthCommand]}</span> `
                + `<span>(${position.coordinates.x}, ${position.coordinates.y})</span>`
                + `<span class="text-primary">${position.orientation}</span>`
                + `<span>${iconCheck}</span>`
                +
            `</div>`
            ;

        plotterData.innerHTML += thisJourneyMoveData.isFinalMove ? `<div class="plotter-line blockquote-footer journey-end">Rover has finished journey<div>` : '';
    }

    /**
     * On clicking, expand/shrink the road map recorder list, on Mars,
     * to the class 'extended' - HTML template
     */
    clickToExpandRoadMap() {
        const
            plotterData = this.tpl.querySelector('.plotter-data')
            , parentContainer = this.tpl.querySelector('section.ui-ux-controller')
            , lastSibblingContainer = this.tpl.querySelector('.user-commands-container')

        ;
        this.isPlotterExtended = !this.isPlotterExtended;

        if (this.isPlotterExtended === true) {
            this.renderer.setStyle(parentContainer, 'overflow', 'visible');
            this.renderer.setStyle(parentContainer, 'justify-content', 'flex-end');
            this.renderer.setStyle(lastSibblingContainer, 'width', 'calc(66% - 2rem)');
            this.renderer.setStyle(lastSibblingContainer, 'overflow', 'hidden');

            // On setting next responsive behaviour, please REMEMBER that CSS has 'transition' 500ms duration and non linear 'timing-function'!
            setTimeout(() => this.checkExtendedPlotterDataBottom(), 600);

        } else {
            this.renderer.setStyle(parentContainer, 'overflow', '');
            this.renderer.setStyle(parentContainer, 'justify-content', '');
            this.renderer.setStyle(lastSibblingContainer, 'width', '');
            this.renderer.setStyle(lastSibblingContainer, 'overflow', '');

            this.renderer.setStyle(plotterData, 'top', '');
        }
    }

    /**
     * Show/hide Mars ruler grid
     *
     * @param evt true => show; false => hide.
     */
    clickToRulersOn(evt: boolean) {
        // Do a little bit of Jquery, for fun... ages passed since I touch DOM with JQuery! ;-)
        const
            isMarsAlwaysOn = true
            , opacity = evt ? 1 : 0
            , $compassCollection = isMarsAlwaysOn && !evt ? $('.grid-center:not(#mars-compass)') : $('.grid-center')
            , $gridItems = $('.grid-items-wrapper')
        ;
        [].slice.call($compassCollection.add($gridItems))
            .forEach(
                (compass: HTMLElement) =>
                    setTimeout(() =>
                        this.renderer.setStyle(compass, 'opacity', opacity)
                    , evt ? 400 : 0)
            )
        ;
        this.areRulersOn = evt;
    }

    /**
     * Cumulate user clicks, to CSS transform '.grid-container' layer to any pos/value @ Mars 'planet'.
     *
     * We compute the 'add up' result and apply NEW CSS 'transform' to '.grid-container'
     *  @ 'planet's "clickToTransformPlanet()" via HTML [clickedToTransformPlanet]= "this.clickedTransformation" var
     *
     * @param askedMovement input from HTML template, selecting the pre-defined type of 'transform' movement user is asking for @ Mars
     */
    clickToTransformMars(askedMovement: string) {
        const
            $userButtonsColl = [].slice.call($('.buttons-container')) as Array<HTMLElement>
            , cumulateClicksTillTime = 1000

        ;
        if ($userButtonsColl[0].className.indexOf('disabled') >= 0) {
            return;
        } else {
            // Do it, passing an input, on 'planet' component children - HTML to CSS Transform is there:
            this.clickedTransformation = { askedMovement: askedMovement, cumulateClicksTillTime: cumulateClicksTillTime };

            // After maximum 'cumulateClicksTillTime' timeout, either by user inactivity, or not, no more clicks:
            setTimeout(() =>
                $userButtonsColl.forEach( (userButtons: HTMLElement) => userButtons.classList.add('disabled') )
            , cumulateClicksTillTime);
            // An emiter should come, IN THE END, to un-block, again, all 'userButtons' - next method.
        }

    }
    // Allow new set of user clicks, for user to CSS Transform the planet whenever after:
    planetCSSTransformFinish() {
        const $userButtonsColl = [].slice.call($('.buttons-container')) as Array<HTMLElement>;
        $userButtonsColl.forEach( (userButtons: HTMLElement) => userButtons.classList.remove('disabled'));
    }

    // ====================
    // AUX funtions:
    // ====================

    checkExtendedPlotterDataBottom() {
        const
            plotterData = this.tpl.querySelector('.plotter-data') as HTMLElement

            , plotterDataBottom = plotterData.getBoundingClientRect().bottom
            , roverInMarsMainBottom = this.tpl.getBoundingClientRect().bottom
            , tooMuchBottom: number = plotterDataBottom - roverInMarsMainBottom

        ;
        if (tooMuchBottom > 0) {
            this.renderer.setStyle(plotterData, 'top', (plotterData.offsetTop - tooMuchBottom).toFixed(0) + 'px');
        } else {
            this.renderer.setStyle(plotterData, 'top', '');
        }

    }
}
