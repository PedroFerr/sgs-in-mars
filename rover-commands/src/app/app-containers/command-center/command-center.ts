import { Component, Input, Output, EventEmitter, AfterViewInit, OnChanges, SimpleChanges } from '@angular/core';

import { GPS, Position, OrientationChar, Instructions, LimitedBy, MoveReport, DeploymentPlace } from '../../app.interfaces';

// Use JQuery on this component...?
declare var $: any;

// Import JSON file, in '*.json' format, with 'require':
declare var require: any;

@Component({
    selector: 'command-center',
    templateUrl: './command-center.html',
    styleUrls: ['./command-center.css']
})
export class CommandCenterComponent implements OnChanges, AfterViewInit {

    // User can define the deployment center, where the entire gear will be deployed @ Mars and Rover will drive from to explore Mars:
    deploymentPoints: Array<DeploymentPlace> = require('./../../_data/mars-places.json');
    // (we choose an init one from here - for instances dthe "Central Point" one)
    marsDeploymentPoint = this.deploymentPoints[2] as DeploymentPlace;
    // ... to fill, on badge Modal's click:
    clickedPoint: string = null;
    clickedColor: string = null;

    // Next will hold the init (user typed) conditions where Rover can be commanded from, on Mars soil:
    // restricted area (W x H) and lag (inverse of velocity) which should Rover do its moves
    roverLimits = { area: {}, lagTime: 1000 } as LimitedBy;
    // init position, before starting
    roverPosition = { coordinates: {}, orientation: {cardinal: null}} as { coordinates: GPS, orientation: {cardinal: OrientationChar } };
    // memorize last (before user changed them) Roover starting point (GPS and orientation):
    initialStartingPoint = {} as { coordinates: GPS, orientation: {cardinal: OrientationChar } };    // ;-) receive user input as Object, pass it as simple, plain string!

    // submiting a second sequence of commands, will trigger Rover:
    // from where it is, or from its init position...? Next should flag it:
    isToCumulateJourneys = {} as { journey: boolean };                                       // ;-) receive user input as Object, pass it as simple, plain boolean!
    // User typed (to validate) str command router, so Rover starts journey:
    strCommand = '';

    // Enables, or echoes @ HTML, editing the '.rover-init-position' <div /> inside '.define-journey' <section />
    isEditMode = true;
    // Top markup panel has a text resuming all user input initial coordinates and direction. Can be hidden or to show:
    showInstructionsText = false;

    // That's it! Switch to tell this Component one more journey has finished and affairs can be taken care of:
    hasJourneyFinished =  false;

    /**
     * MISCELANEOUS:
     */

    // @ 'show-instructions-panel', you can either see the command string in letters (A, L, R) or (arrow) moves:
    seeMoves = true;

    // Max height 'section.define-journey' (our Rover initial conditions form) will have to go up & down
    // It's value is directly related to the Grid this component is inserted into 'app-component'~
    // ==============================================================
    // CHANGE IT only HERE
    // USE it ONLY from this VAR - will influence a lot of App's UX behaviour!
    // ==============================================================
    formMaxHeight = '45rem';

    /**
     * IN / OUT
     */

    // Sends driving command instructions, and the defined restricted area, to 'app-root'
    // ... and to <network-data-point> access, so they can be printed @ 'monitor-comms' bottom monitor
    @Output() private sendSubmitedInstructionsToRover = new EventEmitter<Instructions>();
    sendSubmitedInstructionsToPrint: Instructions;

    // After journey finishes, our 'app' emits to HERE final report, in particularly each JourneyRoadMap (they both will be used at Template's HTML):
    @Input() finalResults: MoveReport;
    lastReachedPos: Position;

    // Communicate reached measurements to 'app-root', that will re-size 'rove-in-mars' component:
    @Output() private resizeMarsArea = new EventEmitter();

    constructor() { }

    ngOnChanges(propChanged: SimpleChanges) {
        // Print final results data, if Input() has come:
        if (propChanged && propChanged.finalResults && !propChanged.finalResults.firstChange) {
            this.hasJourneyFinished = true;
            // We can unlock "isToCumulateJourneys" option TRUE, now disabled (to prevent user to choose it BEFORE submiting Instructions)
            document.getElementById('radio-true').removeAttribute('disabled');
        }

    }

    ngAfterViewInit() {

        /**
         * Besides Angular logic to emit valid strings/numbers to 'app-root', next pure DOM JS method
         * will FIX an ancient HTML5 BUG:
         *     * an integer is different than a float; a number is different from a string!
         * not EVEN allowing to type, at any input box, NON-ALLOWED chars.
         */
        this.doValidateTypedInputs();

        /**
         * Initiate '.fa-question-circle' BS 4 tooltips HELPERS @ '<section class="define-journey">'
         */
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('input[rel="txtTooltip"]').tooltip({ container: 'section.define-journey' });
        });

        /**
         * Clicking on any <fieldset />, passthrough the event ('click') to the Input children.
         * Nice for
         *    * radio's - you want the (to check) click to also work @ the radio label/span
         *    * Dropdon trigger - not necessarilly a <button />; might be... an input text! ;-)
         */
        $('fieldset').on('click', function(evt) {
            const fieldsetInput: HTMLElement = $(this).find('input');
            // The form ('section.define-journey') might be pulled up => no height => no visible inputs => no need to account for clicks!
            // (an error (obviously!) will be thrown on trying to 'dispatchEvent()' or 'click()' on something that does not exists)
            if (fieldsetInput[0]) {

                if ($(this).children().hasClass('dropdown')) {
                    fieldsetInput[0].dispatchEvent(new Event('click'));
                } else {
                    fieldsetInput[0].click();
                }

            }
        });
        // On this specially constructed Dropdown, which has an Inpt as the trigger,
        // [Space] key should also be allowd to trigger Dropdown:
        $('label.inp.dropdown > input').on('keypress', function(evt) {
            if (evt.which === 32) { $(this).click(); }
        });

    }

    /**
     * User has selected a (string - better formated for HTML manipulation)
     * point in Mars to deploy Rover on '#marsDeploymentPoints' BS4 Modal.
     *
     * The string should be transformed on a GPS object, and then,
     * searching on 'deploymentPoints' of all possible places @ Mars,
     * be completed to a 'DeploymentPlacer' type.
     *
     * The pseudo ':after' selector of '.label', where the Input 'name="deploymentPoint"' is,
     * has, at it's CSS, the coloring indexed to CSS var '--jpf-deployment-color-on'.
     * Update it, according to the chosen 'this.marsDeploymentPoint.color'.
     *
     * @params the chosen GPS point, by the user's '.modal-footer' button click
     */
    setDeploymentTo(chosenPoint: string) {
        const
            cleanString = (chosenPoint.replace('{', '').replace('}', '')).split(',')
            , coordinates: GPS = {x: Number(cleanString[0]), y: Number(cleanString[1]) }

        ;
        this.marsDeploymentPoint = this.deploymentPoints.filter(
            (point) => point.coordinates.x === coordinates.x && point.coordinates.y === coordinates.y
        )[0];
        setTimeout(() => this.clickedPoint = null, 500);

        document.documentElement.style.setProperty('--jpf-deployment-color-on', 'var(--' + this.marsDeploymentPoint.color + ')');
    }

    /**
     * Next, is replacing Validators - internally existent in any Angular's framework Reactive Form.
     * If you notice, we didn't markup ANY <form /> @ HTML's template! ;-)
     *
     * But we are already doing validation on upper called "this.doValidateTypedInputs()".
     * It all started when fixing the HTML5 bug (dealing and not allowing some inputed chars),
     * but as we were already there... we did the complete job.
     *
     * @param key - the property of the template's Input to update here (or not! => Validation!)
     * @param evt - the value of the porperty, coming from some template's Input Event
     */
    changeInputTo(key: string, evt: KeyboardEvent | MouseEvent) {
        // What's to collect:
        let objToChange;
        // ... of user chosen/typed value:
        let chosenValue = evt.target['value'] || evt.srcElement;

        switch (key) {
            case 'width':
            case 'height':
                objToChange = this.roverLimits.area;
                chosenValue = Number(chosenValue);
                break;
            case 'lagTime':
                objToChange = this.roverLimits;
                chosenValue = Number(chosenValue);
                break;
            case 'x':
            case 'y':
                objToChange = this.roverPosition.coordinates;
                chosenValue = Number(chosenValue);
                break;

            case 'cardinal':
                objToChange = this.roverPosition.orientation;
                break;

            case 'journey':
                objToChange = this.isToCumulateJourneys;
                break;

            default:
                console.error('ERROR! Some Input form (just added...?!?!?!) is NOT being validated @ "changeInputTo()"!');
                break;
        }
        /*
         * It's here that Angular's Reactive Forms reject, or accepts, the value
         * and adds/removes classes to its Input's markup HTML (ng-valid, ng-touch, etc.)
         * (our upper plain JS DOM's "this.doValidateTypedInputs()" already did the job)
         *
         * So... plain and simple: modify the collection, according to user chosen/typed option:
         */
        // Object.assign(objToChange, { [key]: chosenValue});
        // console.log(`User has chosen «${chosenValue}» for ${key.toUpperCase()}.`);

        // Well... not so simple! ;-)
        // 1) If 'this.isToCumulateJourneys', we have to change "this.roverPosition" to the ones arrived @ 'finalResults'
        // 2) If !"this.isToCumulateJourneys" we have to change it to the previous/latest known, not user changed, ones (this.initialStartingPoint)
        if (key === 'journey' && chosenValue === 'true') {
            // User wants to cumulate journeys, after 1st submit => if we have this.finalResults, use them::
            if (this.finalResults && this.hasJourneyFinished) {
                this.lastReachedPos = this.finalResults.journeyRoadMap.lastPos;
                Object.assign(
                    this.roverPosition
                    , this.lastReachedPos
                    , { orientation: Object.assign({}, { cardinal: this.lastReachedPos.orientation  as OrientationChar}) }
                );
            }
        }

        if (key === 'x' || key === 'y' || key === 'cardinal') {
            // User is CHANGING some property of Rover's starting point:
            // => Disable TRUE option to cumulate journeys:
            document.getElementById('radio-true').setAttribute('disabled', 'true');
            // Memorize the PREVIOUS starting point, till user has change any of those:
            Object.assign(this.initialStartingPoint, this.roverPosition);
        }

        if (key === 'journey' && chosenValue === 'false' && this.initialStartingPoint) {
            Object.assign(this.roverPosition, this.initialStartingPoint);
        }
        // Now we can!
        Object.assign(objToChange, { [key]: chosenValue});
        // console.log(`User has chosen «${chosenValue}» for ${key.toUpperCase()}.`);
    }

    /**
     * Catch the commands (chars) that the user is typing, to route the driving of Rover on Mars
     *
     * @param evt - brings the typed char
     */
    userIsTyping(evt: any) {
        this.strCommand = evt.target.value;
        if (this.strCommand && evt.key && evt.key.length === 1) { // Middle case => user didn't type (choose a value from native browser's inputs history dropdown...)
            // Don't need to echo on non valid control chars:
            // console.log(`User is building command: «${this.strCommand}»`);
        }
    }

    /**
     * We've start using Bootstrap.
     * So we need to set Dropdown's trigger value
     * and put it in trigger's "value", so form can go.
     *
     * After value successfully gets to the trigger,
     * we need to trigger, there, a "change", so element's
     * (change)="changeInputTo('cardinal', $event) can work
     *
     * @param val - passed in via HTML's wired code
     * @param evt - the Dropdown's option clicked event
     */
    setDropdownVal(val: string, evt: MouseEvent) {
        const
            cardinalInput = document.getElementsByName('cardinal')[0] as HTMLElement
            , optionChosen = evt.target as HTMLElement
            , allOptions = [].slice.call(optionChosen.parentElement.children)    // [].slice.call() transforms HTMLCollection in a simple JS array

        ;
        cardinalInput.setAttribute('value', val);
        cardinalInput.dispatchEvent(new Event('change'));
        // Style as active, the item clicked:
        allOptions.forEach( (option) => {
            option.className = option.className.replace(' active', '');
        });
        optionChosen.className += ' active';
    }

    /**
     * Once you press submit form, this one is pulled up
     * so we gain some space for plotting the journey, and 'show-instructions-panel' will un-hide.
     * And, off course, there's a button (up) to pull it down, again, and hide 'show-instructions-panel'
     *
     * At any of the cases, it's 'app-root' job to re-dimension 'space-expedition' component
     * so, inclusing responsive behaviour, everything is still fitting perfectly on the current user's viewport.
     *
     * @param whatHTML - tag to pull up/down
     * @param isItUp - is it...? Can also be to pull down!
     */
    pullUp_showInstr(whatHTML: HTMLElement = null, isItUp: boolean) {
        const
            // With smooth CSS (trnsition on max-height) pull next tag up/down 'this.formMaxHeight' height:
            elemHTML: HTMLElement = whatHTML ? whatHTML : document.querySelector('section.define-journey')

        ;
        elemHTML.style.maxHeight = isItUp ? '0' :  this.formMaxHeight;

        // 'show-instructions-panel' will un-hide/hide because of HTML toggle of 'showInstructionsText' (true/false)
        if (isItUp) {
            setTimeout(() => {
                // Give it CSS time (1000) to 'show-instructions-panel' after 'elemHTML' completely shrinks:
                this.showInstructionsText = isItUp;
            }, 1000);

        } else {
            /**
             * I have no idea why, but here we have a small bug:
             * when the form panel goes down (it was with 'elemHTML.style.maxHeight = 0')
             * Angular looses the value of the clicked rado button.
             *
             * Could it be because of height null? Well it's 'maxHeight' style prop and not 'height'!
             * I don't now what's happening.
             *
             * But as we have, here, the user chosen value, previous to Rover travel,
             * we can click on the right radio button:
             */
            $('#radio-' + this.isToCumulateJourneys.journey)[0].click();
            /**
             * Et voilá! Angular has the value at its HTML.
             * A little of black magic always goes well with Angular! ;-)
             */

            // Hide right away!
            this.showInstructionsText = isItUp;
        }

        // don't forget CSS transition time! Will always happen, always CSS 1000 ms, either going up or down!
        setTimeout(() => {
            // Only NOW it's the perfect time to tell 'app-root' to re-size (max-height) 'rove-in-mars' component:
            this.resizeMarsArea.emit();
        }, 1000 );
    }

    /**
     * Time to send Instructions interface data to 'command-center'
     * No params - just pick this component's vars.
     *
     * Tasks:
     * 1) prepare instructions Obj to emit back, so Rover in Mars gets it and strats rollin
     * 2) If 'isToCumulateJourneys', enter !this.isEditMode - user shoudn't be able to edit '.rover-init-position' div (initial coordinates and orientation)
     *     So lift up <section.define-journey /> (where this entire markup form is), hiding it, and show 'show-instructions-panel'
     *     to get extra space for the 'space-expedition' component, that will get extar height.
     * 3) Disable HTML template's upper "lift-form-command" (pulls DOWN/UP) button, until FINAL journey results arrive, setting "hasJourneyFinished" to "true"
     */
    clickToSubmitInstructions() {
        const
            objToEmit: Instructions = Object.assign({}
                , {
                    deployment: this.marsDeploymentPoint
                    , roverLimits: this.roverLimits
                    , roverPosition: Object.assign(
                        this.roverPosition
                        , { orientation: this.roverPosition.orientation.cardinal as OrientationChar }
                    )
                    , command: this.strCommand
                    , isToCumulateJourneys: this.isToCumulateJourneys.journey   // ;-) just to pass in changeInputTo() as an Input "text" or "select"...
                }
            )
            , formContainer = document.querySelector('section.define-journey') as HTMLElement
            , textInstructionsPanel = document.querySelector('section.show-instructions-panel') as HTMLElement

        ;
        this.isEditMode = (objToEmit.isToCumulateJourneys === 'true' as unknown as boolean) ? false : true;
        this.pullUp_showInstr(formContainer, true);
        this.hasJourneyFinished = false;

        this.sendSubmitedInstructionsToRover.emit(objToEmit);
        this.sendSubmitedInstructionsToPrint = objToEmit;
    }

    /**
     * After submiting, when setting objToEmit.isToCumulateJourneys === 'true',
     * GPS coords && cardinal get hidden => fixed reache position is to show ('this.isEditMode=false').
     * On clicking the other (2nd, false, not to cumulate) option,
     * selector gets back again so initial Rover conditions can be chosen again.
     *
     * @param isToShow - it's a boolean, symetric to "objToEmit.isToCumulateJourneys"
     */
    showSectionDefineJourney(isToShow: boolean) {
        if (this.finalResults) { this.isEditMode = isToShow; }
    }

    /**
     * Next will translate each command unit letter (A, L or R)
     * to nice direction arrows:
     * 'A' => &uarr; | L' => &larr; | 'R'=> &rarr
     *
     *  If 'movesLimit' is passing in, the returned string will be ellipsis if has higher length
     *
     * @param movesLimit - number of moves/arraows that will return
     */
    translateMovesToArrows(movesLimit: number = null): string {
        const routeWithArrows = this.limitStringToChars(this.strCommand, movesLimit);
        // ... then return the translation:
        return routeWithArrows
            .replace(/A/g, '&uarr; &nbsp;')
            .replace(/L/g, '&larr; &nbsp;')
            .replace(/R/g, '&rarr; &nbsp;')
        ;
    }

    limitStringToChars(stringToLimit: string, nChars: number) {
        return stringToLimit.slice(0, nChars) + (stringToLimit.length > nChars ? '...' : '');
    }

    // ====================
    // AUX funtions:
    // ====================

    /**
     * Besides Angular logic to emit valid strings/numbers to 'app-root', next pure DOM JS method
     * will FIX an ancient HTML5 BUG:
     *     * an integer is different than a float; a number is different from a string!
     * not EVEN allowing to type, at any input box, NON-ALLOWED chars.
     *
     * Allthough HTML5 "number" Inputs have "min", "max" and "step" configs, unfortunately VERY wrongly, they keeps allowing
     *     * for the "-" symbol to be inputed or even "pasted" into the box
     *     * for the "." / "," (as decimal) to be inputed or even "pasted" into the box
     *     * the worst, for any STRING to be "pasted" into the box
     *
     * As we are here, we do the complete job - check also:
     *     1) for "commandInput": business logic only allowing 'A', 'L' or 'R'.
     *
     * Running "this.doValidateTypedInputs()"", we have full warranty of having INPUTED VALID "roverCommand"
     * (like driving route info, restricted area to drive, starting coordinates and facing cardinal point, etc.)
     * at 'the app-root' to send to our lonely, brave and far away Rover in Mars!
     * It's the XXIst century thing!
     */
    private doValidateTypedInputs() {
        const
            commandInput = document.querySelectorAll('[name="command"]')
            , areaInput = document.querySelectorAll('.rover-area-input + div input[type="number"]')
            , coordinatesInput = document.querySelectorAll('.rover-init-position input[type="number"]')

        ;
        // Mind you we have now defined 3 Arrays (collectionIdx = 0 | 1 | 2), each with 1 or more (idx's) HTML Objs to add an event listener!
        [commandInput, areaInput, coordinatesInput].forEach( (objs, collectionIdx) => {
            objs.forEach( (obj: HTMLElement) => {
                obj.addEventListener('keypress', (evt: KeyboardEvent) => {
                    const
                        charsAllowed = ['A', 'L', 'R']
                        , figuresAllowed = Array(10).fill(0).map( (v, i) => i)

                    ;
                    let
                        patternArray: {supposeToBeNumber: boolean, patternArray: Array<any>}
                        , strNotAllowed: string

                    ;
                    switch (collectionIdx) {
                        case 0:
                            patternArray = { supposeToBeNumber: false, patternArray: charsAllowed };
                            strNotAllowed = `Sorry! Rover wouldn't understand «${evt.key}»! Please type A, L or R...`;
                            break;
                        case 1:
                            patternArray = { supposeToBeNumber: true, patternArray: figuresAllowed };
                            strNotAllowed = obj.getAttribute('name') === 'lagTime' ?
                                `Sorry! Rover lag time is a positive integer number; «${evt.key}» not allowed...`
                                + `this lag will allow maximum speed (less speed, when increased - a lag of 800ms is advisable).`
                                :
                                `Sorry! Rover area restrictions must be defined by positive integer numbers; «${evt.key}» not allowed...`
                            ;
                            break;

                        case 2:
                            patternArray = { supposeToBeNumber: true, patternArray: figuresAllowed };
                            strNotAllowed = `Sorry! Rover coordinates must be defined by positive integer numbers; «${evt.key}» not allowed...`;
                            break;

                        default:
                            break;
                    }
                    // Check it! If it's to reject, console.log input strNotAllowed:
                    this.isToRejectChar(obj.getAttribute('name'), evt, patternArray, strNotAllowed);
                });
            });
        });
    }

    /**
     * Analyses the `inputName` Input, currently being (user) typed some `event` key.
     * We should run it aginst the passed `allowedChars` (Object carring aditional information)
     * If event.key DO NOT match, echo the `echoStrNotAllowed`
     *
     * @param inputName - analyse it
     * @param event - whats being typed
     * @param allowedChars - what are the chars are allowed
     * @param echoStrNotAllowed - console.log if NOT allowed.
     */
    private isToRejectChar(inputName: string, event: KeyboardEvent, allowedChars: {supposeToBeNumber: boolean, patternArray: Array<any>}, echoStrNotAllowed: string): void {
        // console.warn(`Analysing ${inputName.toUpperCase()} Input, with typed «${event.key}» against ${allowedChars.patternArray}`);
        let strToConsole = '';
        // Don't echo control chars, like 'Backspace', 'Tab', 'Shift', etc. (where evt.key.length is surely > 1!)
        switch (inputName) {
            default:
                if (allowedChars.patternArray.indexOf(allowedChars.supposeToBeNumber ? Number(event.key) : event.key) < 0 && event.key.length === 1) {
                    event.preventDefault();
                    strToConsole = echoStrNotAllowed;
                }
                break;
        }
        // strToConsole is still ''..? Then it's NOT to reject the inputed char:
        if (strToConsole) { console.warn(strToConsole); }
    }

}
