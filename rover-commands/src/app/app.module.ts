import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// APP MAIN Containers division:
import { CommandCenterComponent } from './app-containers/command-center/command-center';
import { MonitorCommsComponent } from './app-containers/monitor-comms/monitor-comms';
import { SpaceExpeditionComponent } from './app-containers/space-expedition/space-expedition';

// Import the MODULES for ALL containers/child-containers that have child-containers/components inside:
// import { CommandCenterModule } from './app-containers/command-center/_command-center.module';
// import { SpaceExpeditionModule } from './app-containers/space-expedition/_space-expedition.module';
// import { PlanetModule } from './child-containers/planet/_planet.module';
// import { DeployedExpeditionModule } from './child-containers/deployed-expedition/_deployed-expedition.module';
// import { VehicleFleetOperationsModule } from './child-containers/vehicle-fleet-operations/_vehicle-fleet-operations.module';

// Import MODULE's main COMPONENTS, you want to 'bootstrap' at 1st loading:
import { PlanetComponent } from './child-containers/planet/planet';
import { DeployedExpeditionComponent } from './child-containers/deployed-expedition/deployed-expedition';
import { SpeakerCommsComponent } from './child-containers/speaker-comms/speaker-comms';
import { VehicleFleetOperationsComponent } from './child-containers/vehicle-fleet-operations/vehicle-fleet-operations';
import { ExploringVehicleComponent } from './components/exploring-vehicle/exploring-vehicle.component';

// ... and the cross-containers Components:
import { ShuttleConveyorComponent } from './components/shuttle-conveyor/shuttle-conveyor.component';

import { VehicleMercedesEngineComponent } from './components/vehicle-mercedes-engine/vehicle-mercedes-engine.component';
import { VehicleCockpitComponent } from './components/vehicle-cockpit/vehicle-cockpit.component';

import { NetworkVoicePointComponent } from './components/network-voice-point/network-voice-point.component';
import { NetworkDataPointComponent } from './components/network-data-point/network-data-point.component';
import { NetworkVideoPointComponent } from './components/network-video-point/network-video-point.component';

import { MonitorJoystickComponent } from './components/monitor-joystick/monitor-joystick.component';

// UI-UX components:
import { SwitchComponent } from './ui-components/switch/switch.component';

@NgModule({
    declarations: [
        AppComponent
        , CommandCenterComponent
            , NetworkDataPointComponent
        , MonitorCommsComponent
            , MonitorJoystickComponent
        , SpaceExpeditionComponent
            , PlanetComponent
                , DeployedExpeditionComponent
                    , SpeakerCommsComponent
                    , VehicleFleetOperationsComponent
                        , ShuttleConveyorComponent
                        , ExploringVehicleComponent
                            , VehicleMercedesEngineComponent
                            , VehicleCockpitComponent
                            , NetworkVoicePointComponent
                            // , NetworkDataPointComponent
                            , NetworkVideoPointComponent
        // ui-components
        , SwitchComponent
    ],
    imports: [
        BrowserModule,
        NgbModule.forRoot(),

        AppRoutingModule                    // => the 3 main app-containers + MonitorJoystickComponent NetworkVideoPointComponent

        // , CommandCenterModule               // => NetworkDataPointComponent
        // , SpaceExpeditionModule             // => SwitchComponent, PlanetComponent (ALREADY IMPORTED HERE NetworkDataPointComponent)
        // , PlanetModule                      // => DeployedExpeditionComponent
        // , DeployedExpeditionModule          // => VehicleFleetOperationsComponent
        // , VehicleFleetOperationsModule      // => ExploringVehicleComponent, VehicleMercedesEngineComponent, VehicleCockpitComponent
                                            // (ALREADY IMPORTED HERE NetworkVideoPointComponent and NetworkDataPointComponent)
    ],
    providers: [],

    /**
     * Besides "AppComponent", you need to 'bootstrap' ALL the main Module's Components you want to be seen at 1st loading!
     * Later on we'll use "Lazy Loading" for SOME of them - only loaded on some user action event!
     *
     * By then, we'll only bootstrap what's absolutely essential to be seen on 1st loading, besides 3 main app-containers
     * - for instances, "ExploringVehicleComponent" is not essential to be bootstrap now, when shuttle is circuling around on planet orbit... waiting for user Instructions!
    // (this component only needs to se life lights @ VehicleFleetOperationsModule Module loading)
    */
    bootstrap: [
        AppComponent
        // , SpaceExpeditionComponent
            // , NetworkDataPointComponent
            // , PlanetComponent
                // , DeployedExpeditionComponent
                //     , VehicleFleetOperationsComponent
                //         , ExploringVehicleComponent
                            // , VehicleMercedesEngineComponent
                            // , VehicleCockpitComponent
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
