import { Component, ElementRef, Renderer2, AfterViewInit, HostListener, ViewChild } from '@angular/core';

import { SpaceExpeditionComponent } from './app-containers/space-expedition/space-expedition';
import { MonitorCommsComponent } from './app-containers/monitor-comms/monitor-comms';

import {
    InitOfTimesTimmings, Instructions, VehicleInstructions
    , MoveReport
    , MonitorData, MonitorAstronaut, SpeakerVoice
} from './app.interfaces';

import * as JsPlugIns from './_js-utils';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
    // tslint:disable:variable-name

    // Put first ever landing (animation) moments on the tely ('monitor-comms' top monitor's '.screen')
    // through our high-tecck, through out the entire App, 'network-video' access point! ;-)
    sendInitOfTimesMoments: InitOfTimesTimmings;

    // Local App var that will hold any, but particularly the first ever, submited Instructions each journey, by user
    // Some children Components will pick them up from here:
    userSetUpInstructions: Instructions;

    // TO 'command-center'
    sendJourneyResults: MoveReport;
    // TO 'space-expedition':
    sendInstructions: VehicleInstructions;

    // ---------------------------------------
    // Now the DATA and VIDEO Networks (VOICE network Objects @ 'deployed-expedition')
    // ---------------------------------------
    // TO 'monitor-comms'
    sendDataToPrint: MonitorData;
    sendStreamToVideo: MonitorAstronaut;

    // This component's DOM:
    appMain: HTMLElement;
    // Responsive code will exist for the next 3 components (from upper one, we only need its measures)
    // to compete in between themselves for the viewport's total area - not causing its overflow!
    // So, grab the (<main />) DOM child components, inside this parent-of-all one.
    commandCenter: HTMLElement;
    spaceExpeditionMain: HTMLElement;
    monitorCommsMain: HTMLElement;

    constructor(
        private thisDOM: ElementRef,
        private renderer: Renderer2
    ) {}

    @ViewChild(SpaceExpeditionComponent) spaceExpeditionMethods: SpaceExpeditionComponent;
    @ViewChild(MonitorCommsComponent) monitorCommsComponentMethods: MonitorCommsComponent;

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.triggerResizeMars();
    }

    ngAfterViewInit() {
        this.appMain = this.thisDOM.nativeElement.querySelector('main.main-app');
        this.commandCenter = this.thisDOM.nativeElement.querySelector('command-center > main');
        this.spaceExpeditionMain = this.thisDOM.nativeElement.querySelector('space-expedition > main');
        this.monitorCommsMain = this.thisDOM.nativeElement.querySelector('monitor-comms > main');

        // Do our first measurement (form 'command-center') / adjustment (to 'space-expedition' and 'monitor-comms'):
        this.triggerResizeMars();

        // FIRST streaming video EVER to mother Earth!
        // Define here (ngAfterViewInit() components L.C.H) the moments we will have, to send to the installed, and just ready to work, network video
        // (make it assynchronous so DOM can settle down the complex structure is has in front)
        setTimeout(
            () => this.sendInitOfTimesMoments = {
                tryingToConnectTime: 0
                , hugeNoiseTime: 5000
            }
        , 0);

        // Pull down big Sony Video Hall screen, so user gets all the animation:
        setTimeout( () => {
            // this.monitorCommsComponentMethods.pullVideoHallMonitor('down');
            // Pulling it down is easy... but client might want to pull it up, right away...
            // On "click outside" (BS4 Dropdown), you would need to fill the whole viewport of "addEventListener()s"...!
            //  ;-)
            this.monitorCommsComponentMethods.clickJoystickMonitor();
            // Done! ;-) BS4 Dropdown already does this for us!
        }, 1000);

    }

    /**
     * ===========
     * TODO:
     * ===========
     *
     * Show/hide 'left-wing' ('monitor-comms' component) or 'right-wing' ('command-center' + 'space-expedition')
     * of this 'app.component' BS4 Grid,
     * by applying same 'space-expedition-has-full-wing' class
     * to both (each wing) Angular 'top-component' components and 'space-expedition' ('bottom-component' of 'left-wing')
     * with opposite CSS coding on each of the <main /> component's tag.
     *
     * @param component the column class of this 'app-component' BS4 Grid to toggle show/hidden
     */
    onClickToggle(columnWing: string) {
        const
            onBS4Grid = (this.appMain.parentElement.querySelector('.container-fluid'))
            , extendSpaceExpedition = 'space-expedition-is-full-wing'
        ;
        if (columnWing === 'right-wing') {
            // $onBS4Grid
            //     .find(columnWing)
            //         .toggleClass(extendSpaceExpedition)
            //         .end()
            //     .find('space-expedition')
            //         .find('main')
            //             .toggleClass(extendSpaceExpedition)
            //             .end()
            //         .end()
            // ;
        }

        if (columnWing === 'left-wing') {
            // if (this.this.commandCenter.classList.contains())
            // this.renderer.setStyle(this.commandCenter, 'height', 0);
        }

        // this.renderer.setStyle(, 'min-width', 'calc(100% - 5.5rem)');
        // this.renderer.setStyle(this.appMain.nextSibling.querySelector('.right-wing'), 'max-width', '5.5rem');
    }

    /**
     * Rover's Instructions received FROM 'command-center' => update global "this.userSetUpInstructions"
     * Send them to 'space-expedition' so Rover can start the expedition by exploring Mars, step-by-step (move-by-move)
     *
     * If it's NOT to cumulate journeys, clean current 'monitor-comms' bottom screen data.
     *
     * @param evtData  - the instructions that will put Rover moving in Mars
     */
    receiveSubmitedInstructions(evtData: Instructions) {
        if (evtData.isToCumulateJourneys.toString() === 'false') { this.monitorCommsComponentMethods.deleteDataContent(); }

        this.userSetUpInstructions = evtData;

        const
            objToEmit: VehicleInstructions = Object.assign( evtData
                , { charCommand: this.prepareInstructions(evtData.command) }
            )
            // Simulate Instruction's "some" preparation time:
            , timeToPrepareInstructions = this.userSetUpInstructions.roverLimits.lagTime // 1000
            , echoStr = `... and were RETRANSMITED (after ${(timeToPrepareInstructions / 1000).toFixed(2)}s of preparing time) to \'space-expedition\'!`
        ;
        setTimeout(() => {
            // console.log(echoStr, objToEmit);
            this.sendInstructions = objToEmit;
        }, timeToPrepareInstructions);
    }

    /**
     * Last step of he journwy Data was received from 'space-expedition'.
     * Send it to 'command-center', so a few things can be triggered over there.
     *
     * @param journeyData each move (this in particular is LAST one) of Rover's exploration Data.
     */
    receiveJourneyFinalResults(journeyReport: MoveReport) {
        this.sendJourneyResults = journeyReport;
    }

    // =======================================================================

    // Voice communications FROM ANY 'network-voice-point' treated @ 'deployed-expedition'

    /**
     * Data communications FROM ANY 'network-data-point'
     *
     * Receives Rover Instructions or data to report, once each Rover's movement gets accomplished.
     * On end of journey (final journey move),
     * aditional msgs are to be printed, and 'command-center' to be flagged uppon, on 'this.sendJourneyResults'
     *
     * @param evtData - the Rover report, plus 'msg', once it started its journey on Mars, on each journey move
     */
    receiveDataComm(evtData: MonitorData) {
        // On communications printing, give it time enough for the CSS "Type writter effect:* to write the complete sentence.
        const dataCommunicationsTime = 2 * this.userSetUpInstructions.roverLimits.lagTime;

        setTimeout( () => this.sendDataToPrint = Object.assign({}, evtData), dataCommunicationsTime );

    }

    /**
     * Video streaming communications FROM ANY 'network-video-point'
     *
     * Receives streaming updates, from any point of the installed, in several HTML markup places, VIDEO NETWORK
     * to update video streaming on 'monitor-comms' top monitor's Sony big wide '.screen'.
     *
     * @param evtStreaming - the current 'frameLabel', 'astronautPos', 'astronautStatus' and 'videoScenario' props
     */
    receiveStreamingComm(evtStreaming: MonitorAstronaut) {
        // When we talk about streaming video images, we must be much faster, than printing Data! ;-)
        const videoStreamingTime = 1000; // this.userSetUpInstructions.roverLimits.lagTime + 500;

        // NOTE that THIS time, contrary to 'evtStreaming.frameToStrem.videoScenario.cssTimer', has NOTHING to do with the streamed VIDEO MONITOR animation!
        // It's just a DELAY - which is cool! - till new stream frame starts to be animated at 'monitor-comms' top Monitor!
        //
        // On the other hand, "dataCommunicationsTime" @ "this.sendDataToPrint", has, yes!, to do with ANIMATION but in the OTHER DATA bottom MONITOR:
        // "... give it time enough for the CSS "Type writter effect:* to write the complete sentence." as commented above.

        setTimeout( () => this.sendStreamToVideo =  Object.assign( {}, evtStreaming), videoStreamingTime );
    }

    /**
     * Next method has two reasons, that naturally, if you think about it, tend to merge one into another:
     *     1) Due to the "mouse and cat" hide/show game between the 2 sections of top 'command-center' component
     *         ('.show-instructions-panel' can hide/show while '.define-journey' travelling Inputs shows/hides inversely)
     *         we want, on measuring 'command-center' (<main />) height at some moment, to calculate
     *         how much of the viewport height has been left available, to be occupied by 'space-expedition' (<main /> HTML tag) component.
     *
     *     2) Due to the natural needed components responsiveness, when user fiddles around with viewport's width/height,
     *         we want al the 3 main components ('command-center', 'space-expedition' and 'monitor-comms') to maintain the BS4 grid structure
     *         aspect ratio, with NO overflow from neither axys.
     *         So, for instances, if 'command-center' panel of '.define-journey' travelling Inputs shows, that component will go taller
     *         => 'monitor-comms' should also, along, go taller to, the exact same height.
     *         And this should happen also in width AND with all 3 components!
     *
     * So, allthough we'll be in this method code for different reasons, all of them have to do with
     *     a) viewport's dimensions
     *     AND TO
     *     b) the "to occupy" dimensions for each of the 3 components, that compete in between themselves to always NOT to cause viewport overflow.
     *     (which is, again, viewport's dimensions!)
     *
     * Plus let's not forget the '.plotter-data' road map list @ 'space-expedition', of fixed, but scrollable height,
     * once classed as 'extended'  - needs to be repositioned IF it doesn't have sufficient space available into current viewport's height,
     * meaning 'space-expedition' is currently very short (maybe due to 'command-center' being very tall... or to user's playing with viewport dimensions!)
     *
     * Get's really tricky... but we managed to make it happen! ;-)
     *
     * Another worry on top is the overhead ammount of calculous, that we must avoid to the maximum.
     * CSS properties handle by DOM, keep changing in all sorts of figures, in a lot of selectors and almost permanentely!
     * So we throttle the (code of) triggering of next Method to 1s
     * => this way user can freelly play around with viewport's width; only when he/she stops for 1s, 'triggerResizeMars()' code will run
     * (it's thousands of calculous and CSS changes we avoid, here)
     *
     * ===================================================================
     * And ;-) this is ONLY important on viewport's dimensions changing
     * NOT on rearranging the occupied space by the 3 components, inside the SAME viewport's dimensions!
     * ===================================================================
     *
     * Got it...? ;-)
     * Just follow next piece of code...
     */
    triggerResizeMars() {

        setTimeout(() => {    // <= Let DOM settle down; has just been changed! => call it assyncronously.

            this.setSpaceExpeditionHeight();

            JsPlugIns.waitForFinalEvent( () =>    // <= throttle the resizing (save overhead browser calculus and DOM changes!)
                this.setMonitorHeight_plotterDataPos()
            , 1000, `Re-dimension viewport's space #${new Date().getTime()}! Please wait 1 second...`);

        }, 0);
    }

    // ====================
    // AUX funtions:
    // ====================

    prepareInstructions(word: string): Array<any> {
        const arrayOfChars = [];
        for (let n = 0; n < word.length; n++) {
            arrayOfChars.push(word[n]);
        }
        return arrayOfChars;
    }

    private setSpaceExpeditionHeight() {
        const
            appMain_height = this.appMain.getBoundingClientRect().height + 'px'
            , commandCenter_height = this.commandCenter.getBoundingClientRect().height + 'px'
            , marsUiUxController = this.spaceExpeditionMain.querySelector('section.ui-ux-controller')
            , marsUiUxController_height = marsUiUxController.getBoundingClientRect().height + 'px'
            // What we, by CSS, pretend:
            /*
            max-height: 100vh - [
                app-component's <h1 class="mars-angular-header" />
                + 'command-center's <main />
                + 'space-expedition's <section .ui-ux-controller />
                +  2 * (<body /> margin)
            ]
            */
            , pretendedHeight = `calc(100vh - (`
                + `${appMain_height} + `
                + `${commandCenter_height} + calc( `
                + `calc(${marsUiUxController_height} + 0.6rem) + `
                + `2*0.5rem + 1rem)`
                + `) )`

        ;
        this.renderer.setStyle(this.spaceExpeditionMain, 'max-height', pretendedHeight);

        // And, on the other hand, initial 'grid-container' might not have noting, or very few, data on it:
        this.renderer.setStyle(this.spaceExpeditionMain.querySelector('div.grid-container'), 'min-height', pretendedHeight);
    }

    private setMonitorHeight_plotterDataPos() {
        const
            spaceExpedition_height = this.spaceExpeditionMain.getBoundingClientRect().height + 'px'
            , commandCenter_height = this.commandCenter.getBoundingClientRect().height + 'px'

            , marsUiUxController = this.spaceExpeditionMain.querySelector('section.ui-ux-controller')
            , marsUiUxController_height = marsUiUxController.getBoundingClientRect().height + 'px'

            // We now have a top monitor streaming video, on 'monitor-comms' component!
            , monitorCommsStreamingVideo = this.monitorCommsMain.querySelector('section.monitor-video-streamer')
            , monitorCommsStreamingVideo_height = monitorCommsStreamingVideo.getBoundingClientRect().height + 'px'
            // We don't want anymore to achieve 'monitorCommsMain_height'
            // but 'monitorCommsDataPrinter_height' instead
            // which is (monitorCommsMain_height - monitorCommsStreamingVideo_height)

            , monitorCommsDataPrinter_height = `calc(`
                + `${commandCenter_height}`
                + ` + ${marsUiUxController_height}`
                + ` + ${spaceExpedition_height}`    // <= notice, only after it has (eventually) been updated, on previous setTimeout()
                // ----------------------------------------
                + ` - ${monitorCommsStreamingVideo_height} - 0.5rem`
                // ----------------------------------------
                + ` + 0.1rem`
                + `)`

        ;
        this.renderer.setStyle(this.monitorCommsMain.querySelector('section.monitor-screen-printer > pre'), 'min-height', monitorCommsDataPrinter_height);
        // Pull down screen messages content, in the end:
        this.monitorCommsComponentMethods.pullBottomMonitorMessagesDown();

        // 'space-expedition' road map listing, on user (or components setup) window resize, might be extended - needs repositioning?
        const spaceExpeditionPlotterData = marsUiUxController.querySelector('.plotter-data') as HTMLElement;
        if (spaceExpeditionPlotterData.classList.contains('extended')) {
            this.spaceExpeditionMethods.checkExtendedPlotterDataBottom();
        }
    }

}
