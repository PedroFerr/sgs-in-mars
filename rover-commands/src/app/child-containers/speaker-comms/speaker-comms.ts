import { Component, Input, OnInit, OnChanges, AfterViewInit, Renderer2, ElementRef } from '@angular/core';

import { SpeakerVoice } from 'src/app/app.interfaces';

// Use JQuery on this component...?
declare var $: any;

@Component({
    selector: 'speaker-comms',
    template: `<div class="rover-tooltip"></div>`
})
export class SpeakerCommsComponent implements OnInit, OnChanges, AfterViewInit {

    // Create a bundle of extendeed JQuery methods of our own.
    // JQuery "positionTooltip()" (using code from "cssToPositionWithDelay()" function) is, for now, our first one!
    JQueryExtensions: any;

    // Grab this component's HTML - it wlll be manipulated a lot, including also by other Components - and its main HTML tags actors:
    tpl: HTMLElement;
    vehicleTooltip: HTMLElement;

    @Input()  voiceToBeRead: SpeakerVoice;

    constructor(
        private thisDOM: ElementRef,
        private renderer: Renderer2
    ) { }

    ngOnInit() {
        // Methods of our own, to extend JQuery ones:
        this.JQueryExtensions = {
            cssToPositionWithDelay: function showTooltip(setup: { title: string, container: string, cssTransform: string, delay: number }) {
                $(this)
                    // Get rid of the previous one
                    .tooltip('dispose')

                    // Initial setup to this one to be born @ 'setup.cssTransform' CSS:
                    .tooltip(
                        {
                            container: setup.container
                            , html: true
                            , title: setup.title
                        }
                    )
                    // Bring it to life, in Mars... so BS4 Tooltip can exist to be manipulated... but will be hiden in a moment!
                    .tooltip('show')

                    // JourneyRoadMap of '.tooltip' (next sibbling element to '.rover-tooltip') at the Rover's "move to" coordinates:
                    .next()
                        .css('opacity', 0)
                        /*
                        This one was tough!
                            1) Give it SUFFICIENT TIME for:
                                * DOM to be modified by DBS4 Tooltip,
                                    returning the tooltip (now hidden) to the original 'styles.css' transform point @ 'grid-container' (~200ms)
                                * finishing CSS coded 'tramsition', taking Rover from previous to this 'transformAtPos' Position (cssTransitionDelay)
                            2) JQuery does not allow '!important' on $().css('x !important') => use $().attr('style', 'x !important') instead
                        */
                        .delay(setup.delay)
                        .queue(
                            function () {
                                $(this)
                                    .attr('style', $(this).attr('style') + ' transform: ' + setup.cssTransform)
                                    .dequeue()
                                    .css('opacity', 1)
                                    ;
                            }
                        )
                        .end()
                    .end()
                ;

                return this;
            }
        };
        // Make 'positionTooltip()' one more method accepted from the JQuery library:
        $.fn.extend({
            positionTooltip: this.JQueryExtensions.cssToPositionWithDelay
        });

    }

    ngOnChanges() {
        // console.error('speaker-comms, @ ngOnChanges()', this.voiceToBeRead);

        if (this.voiceToBeRead) {
            this.cockpitTalk(this.voiceToBeRead);
        }
    }

    ngAfterViewInit() {
        this.tpl = this.thisDOM.nativeElement;
        /**
         * These next ones can NOT be set HERE! DOM is always changing...
         *
         * Well... they SHOULD!
         * Distant parent 'vehicle-fleet-operations' HTML container will USE THEM (through @ViewChild() Injector) at
         * "calculateLandingMoments()" and "landExploringVehicleOnPlanet()", for landing animation and calculous.
         * because it's there the Shuttle Conveyour HTML '.rover-in-orbit', that will deploy our Rover on the planet to explore.
         *
         * Once needed HERE, the ALERT is to be carefull WHEN to DOM query them - HTML tag's styling are ALWAYS changing!
        */
        // this.vehicleTooltip = this.tpl.querySelector('.rover-tooltip');  // Re-queried @ cockpitTalk()

    }


    /**
     * Display the 'msg', at the Rover's top tooltip, that will follow all it's movements, on 'deployed-expedition' @ the 'planet'.
     *
     * Notice that at 'space-expedition' HTML template, our <div class="rover-tooltip" /> is marked up as a direct chid of '.grid-container',
     * our fixed, central compass where all the gear was deployed at Mars. Rover moves from this point on, according to the received Instructions
     *
     * A brief parentesis:
     *     At the beginning, we implemented Rover's tooltip as a direct child of '#rover' - it seemed quite logical... but we got into a LOT (2 days!) of trouble!
     *     BS4 Tooltip already styles tooltip 'transform' to keep it "close" to its parent.
     *     So, for instances, when Rover was turning left, the tooltip would also "turn" left (by seeing updated its CSS "transform"),
     *     staying upside down or flipped lettering vertical...That's too bad for our websurfer's spine - would have to keep nodding the head, to read tooltip text! ;-)
     *
     *     Besides (we tested it!), it was crazy to try to override BS4 tooltip CSS,
     *     because we were needing some setTimeout() for DOM to be updated (we figure it out about 200 ms).
     *     But next move was already counting next CSS transition (movement) delay time!
     *     Allthough not completely impossible, it was a bit of a code mess chasing BS4 Tooltip CSS transform (X, Y, and rotate) changes...
     *     and change them! After a tiny while... at EACH move... that could go North, East, South or West!
     *
     *     What would come handy is a "steady" tooltip, that we would control (position) on each Rover's move (X and Y ONLY) entirely and solely.
     *     So... what better container to HTML mark it into, than '.grid-container'...? ;-)
     *
     * REMEMBER BS4 tooltip still will ALWAYS do some CSS 'transform' styling (and, also, 'position: absolute', 'top: 0', 'left: 0', etc.) update,
     * every time you apply ANY '.tooltip's config option or action ('show'/'hide','toggle', 'dispose').
     * Once needed/used on code, we ALWAYS need to overwrite '.tooltip's container position {0,0} style translate to the position for Rover to reach/stay!
     * Never back at {0,0}!
     *
     * Even still having BS4 Tooltip "automatic" changes, this time, it wil NOT CHANGE automalically with (each) Rover moves, simply because it's not its container anymore.
     * It's us who change the tooltip CSS translate, playing with delay timmings as we whish - off course, in-between Rover's movement CSS delay.
     *
     * So, placing the tooltip inside '.grid-container', at each '#rover' move, we simply have to move it X/Y, in parallel, to the final position of each Rover's move.
     */
    cockpitTalk(thisContent: SpeakerVoice) {
        this.vehicleTooltip = this.tpl.querySelector('.rover-tooltip');
        // See main "styles.css" tooltip init postion of "speaker-comms > .rover-tooltip  + .tooltip" to get initial offset.
        // Both X and Y initial offset are to maintain - you only "add-up" Rover's translate X and Y.
        const initXoffSet = '0', initYoffset = '3';

        const
            roverTooltipCSStransformTo = `translate(`
                + `calc(-50% + ${initXoffSet}rem + ${thisContent.transformAtPos.coordinates.x}rem)`
                + `, calc(-50% - ${initYoffset}rem - ${thisContent.transformAtPos.coordinates.y}rem)`
                + `) !important; `

            , cssTransitionDelay = thisContent.delayCSSTransition // parseFloat($('#rover').css('transition-duration')) * 1000 // 1000
            , cssDOMupdateTime = 200

            // --------------------------------------------------------------------
            // Give it a tiny bit extra time so people have time to read it - pop it out even before arriving to posiiton.
            // Remember we have a fade in/out effect, @ tooltip, which allows to "start showing" before.
            // --------------------------------------------------------------------
            // --------------------------------------------------------------------
            , tynyLess = 0.6
            // --------------------------------------------------------------------
            // --------------------------------------------------------------------
            , timeToShowTooltip = /* 1000 */ thisContent.waitForCSSTransition ?
                cssTransitionDelay * tynyLess > cssDOMupdateTime ? cssTransitionDelay * tynyLess : cssDOMupdateTime
                :
                cssDOMupdateTime
            /**/
        ;
        // Just chain our fresh new created JQuery method @ JQueryExtensions.cssToPositionWithDelay(params)
        $(this.vehicleTooltip)
            .positionTooltip({
                title: thisContent.msg
                , container: 'speaker-comms'
                , cssTransform: roverTooltipCSStransformTo
                , delay: timeToShowTooltip
            })
        ;
    }
}
