import {
    Component, Input, Output, EventEmitter, OnChanges, SimpleChanges, AfterViewInit
    , ViewChild, ElementRef, Renderer2
} from '@angular/core';

import { ExploringVehicleComponent } from './../../components/exploring-vehicle/exploring-vehicle.component';
import {
    Position, JourneyRoadMap
    , LandingOperationsData, LandingOperationsTimmings
    , VehicleMove, VehicleMoveInitializer
    , SpeakerVoice, SpeakerVoiceInitializer, OrientationChar
    , MoveReport
} from 'src/app/app.interfaces';

@Component({
    selector: 'vehicle-fleet-operations',
    templateUrl: './vehicle-fleet-operations.html',
    styleUrls: ['./../deployed-expedition/deployed-expedition.css']
})
export class VehicleFleetOperationsComponent implements OnChanges, AfterViewInit {

    // Grab this component's HTML - it wlll be manipulated a lot - and its main HTML tags actors:
    tpl: HTMLElement;
    exploringVehicleIcon: HTMLElement;
    @ViewChild(ExploringVehicleComponent) exploringVehicleComponent: ExploringVehicleComponent;
    // And for Shuttle conveyor, that's bringing inside our Rover's exploring-vehicle':
    shuttleOrbit: HTMLElement;
    shuttleConveyorIcon: HTMLElement;

    // Is it to do a nice impec landing? Or was already Rover's Cockpit deployed before?
    isShuttleStillOnOrbit = false;
    // There will ALWAYS be a 1st time for landing:
    landingOperationsTimmings = {} as LandingOperationsTimmings;
    // ... using 'landingData' data, coming from parent 'deployed-expedition':
    @Input() landingData: LandingOperationsData;
    // ... we can set Shuttle's '.waiting-orbit' orbiting area HTML style, also coming from parent 'deployed-expedition':
    @Input() nXcells: number;

    /**
     * ============================
     * 1st Rover's "move", STILL inside Shuttle Conveyor, from Deployed point to "init position",
     * in auto-pilot time, just before Rover starts its exploration journey,
     * on EACH new user Input Instructions submission.
     * ============================
     * We'll need, from CHILD "exploringVehicleComponent" some "doMoveRover()" with some BS4 Tooltip "talk"
     * AND we'll need some DIRECT "talk" WITHOUT ANY movement from Rover!
     */
    initEachMove: VehicleMove = Object.assign( {}, VehicleMoveInitializer);
    initNetworkVoice: SpeakerVoice = Object.assign( {}, SpeakerVoiceInitializer);

    // After all landing operations have finished, will be the time for the 'exploring-vehicle' to start doing its JOURNEY over planet:
    landingOperationsFinish = false;
    // And put landing (animation) moments on the tely ('monitor-comms' top monitor's '.screen')! ;-)
    landingMomentsTimerObj: LandingOperationsTimmings;

    @Input() currentJourneyRoadMap: JourneyRoadMap;

    @Output() private sendPlanetToZoomOut = new EventEmitter();
    @Output() private sendlandingMomentsTimerObj = new EventEmitter<LandingOperationsTimmings>();
    // In the end of each journey, on 'exploring-vehicle's last "eachMove",
    // signal back Astronaut is no longer on Mars soil and sent back final journey results:
    @Output() isAstronautOnMarsSoil = new EventEmitter<boolean>();
    @Output() finishExplorationJourney = new EventEmitter<MoveReport>();


    constructor(
        private thisDOM: ElementRef,
        private renderer: Renderer2
    ) { }

    ngOnChanges(propChanging: SimpleChanges) {

        if (propChanging && propChanging.nXcells && !propChanging.nXcells.firstChange) {
            // Draw the orbit, around Mars, for 'shuttle-conveyor':
        }

        if (propChanging && propChanging.landingData && !propChanging.landingData.firstChange) {
            // We should have our 'landingData' content, arrived from 'deployed-expedition'. So, we can calculate
            /**
             * ==============================================================
             * 1st move form DEPLOYMENT point to STARTING to explore "init position" one,
             * ==============================================================
             * in auto-pilot time, just before Rover starts its exploration journey.
             *
             * // this.calculateLandingMoments();
             *
             * With the ENTIRE landing moments CALCULATED, at this landing animation, put them available to whomever is needing it:
             */
            this.landingMomentsTimerObj = this.calculateLandingMoments();
            /**
             * Who will need them..?
             *
             * 1) @ 'deployed-expedition' PARENT
             *        so "doMarsGrid()" can draw/re-draw all action grid in Mars - user MIGHT have changed Instructions => re-draw action grid.
             * 2) @ 'network-video-point' CHILDREN of 'exploring-vehicle' CHILDREN:
             *        we can broadcast each, right away, through our 'exploring-vehicle's access point of the installed network of video!
             * 3) @ 'exploring-vehicle' CHILDREN:
             *        we can use total 'totalLandRoverOnMarsTime' to start exploring journey, right on planet's soil!
             */
            // 1) // Went to "calculateLandingMoments()" IF "this.isShuttleStillOnOrbit"
            // NO IT DID NOT!!!! EVERY time there's a user SUBMISSION, grid MIGHT need to be re-drawn!
            this.sendlandingMomentsTimerObj.emit(this.landingMomentsTimerObj);

            // 2) See template use of 'landingMomentsTimerObj' as a <network-video-point /> Input
            //     We put directly this same "this.landingMomentsTimerObj" on 'exploring-vehicle's [getLandingMomentsTimerObj],
            //     that will be [landingVideoStreamMoments] @ <network-video-point /> installed close to 'exploring-vehicle'

            // 3)
            this.landingOperationsFinish = false;    // Provoke a ngOnChange(); nothing will happen, but allow us to provoke an effective one, after "totalTime"
            const totalTime = this.landingMomentsTimerObj.totalLandRoverOnMarsTime;
            setTimeout(() => this.landingOperationsFinish = true, totalTime );

            /**
             * ==============================================================
             */

            // For the voice network (BS4 Tooltips) to be synchronized, we need each vehicle's movement step time on 'space-expedition':
            Object.assign(this.initNetworkVoice
                , { delayCSSTransition: this.landingData.speedOnPlanet}
            );
            // Now "this.initNetworkVoice" is properly initiated!
            // - don't need to repeat any property, in special 'delayCSSTransition' (but also 'waitForCSSTransition' or 'toShow'), UNLESS to override.
            // 'msg' to show on BS4 Tooltip will be always override, on each new assign, obviously.
        }
    }

    ngAfterViewInit() {
        this.tpl = this.thisDOM.nativeElement;
        // These next ones can NOT be set HERE! DOM is always changing....
        // this.exploringVehicleIcon = this.tpl.querySelector('#rover');
    }

    private calculateLandingMoments(): LandingOperationsTimmings {
        // (see top of 'space-expedition' css for further details)

        this.shuttleOrbit = this.tpl.querySelector('.waiting-orbit');
        this.shuttleConveyorIcon = this.shuttleOrbit.querySelector('.rover-in-orbit');
        this.isShuttleStillOnOrbit = this.shuttleConveyorIcon.getAttribute('class').indexOf('stop-orbiting') === - 1;

        // If so:
        if (this.isShuttleStillOnOrbit) {
            // This is the beginning of times! Somehow shuttle must come here once!
            // 1st animation will take Rover, STILL inside Shuttle, to "this.landingData.startingPointOnPlanet"
            this.landingOperationsTimmings = this.landExploringVehicleOnPlanet(this.landingData.startingPointOnPlanet);

            // As we have all the landing moments calculated for THIS Instructions user submission,
            // we can return to emit back to 'deployed-expedition' so "doMarsGrid()" can draw/re-draw all action grid in Mars:
            return this.landingOperationsTimmings;

        } else {
            // Shuttle HAS LANDED (once) and "gone" from browser's sightseeing. In this animation, we'll use similar SpeakerVoice messages and moments,
            // AFTER "timeToLand" moment, as we did on previous "landExploringVehicleOnPlanet", beginning of all times animation.

            // Rover will either be commanded to 1) "Start at last inputted starting point" or 2) "Cumulate from arriving position"
            // 1)
            if (this.landingData.isToCumulateJourneys === 'true' as unknown as boolean) {
                // If next Init Position is already were Rover is... no diagonal to do, no more waits;  just start moving!
                // Don't forget to undo planet zooming, to operate on 'planet-bg-img', with same amplitude as in '.grid-container'
                this.sendPlanetToZoomOut.emit();
                // Well... don't start moving right, right, right away...
                // Give it a sec for the 'command-center' submit instructions panel to lift completely up - and 'planet' be as max extended as viewport height allows:
                // we can return to emit back to 'deployed-expedition' so "doMarsGrid()" can draw/re-draw all action grid in Mars:
                return { shuttleTime: 0, roverTime: 0, astronautTime: 0, totalLandRoverOnMarsTime: 1000 };

            // 2)
            } else {
                this.exploringVehicleIcon = this.exploringVehicleComponent.Cockpit.exploringVehicleIcon;
                const
                    // Memorize current CSS transition time:
                    previousSpeed = this.exploringVehicleIcon.style.transitionDuration

                    // To add some UX, simulate, here, the same ('timeToLearnHowToDriveRover' + 'readySetGoDelay') @ "landExploringVehicleOnPlanet()"
                    // Which is... last delta timming of 'totalLandRoverOnMarsTime', NOW an Object of timmings.
                    , autoPilotDrivingTime = this.landingOperationsTimmings.totalLandRoverOnMarsTime - this.landingOperationsTimmings.astronautTime // 4000
                    // Take the time frame when automatic pilot has entered:
                    , timeToMoveToInitPos = this.landingOperationsTimmings.autoPilotTime

                ;
                /**
                 * Reaching this point code, Rover has to DRIVE, again, to Init Position - "Start at last inputted starting point"
                 *
                 * What about if last journey's reached position (where its standing now) is... miles away from the Init Position...?!?!??
                 * It can take AGES for Rover to reach it! So...? How to assure you can avoid it...?
                 * So:
                 * 1)
                 * Move, first, within the minimum time possible - one step movement unit - from where its now standing to {0, 0} Earth reference coordinates
                 * which is the deployment point (remember Astronaut - Rover and Shuttle - is, now, in the air, not touching Mars soil)
                 * This {0, 0} Position is already @ default's "this.initEachMove" - no need to repeat it; just state properties to CHANGE those default ones.
                 * 2)
                 * and, then, to journey's "calculateStartingPosition()" this.position, just like when it was deployed from the first time.
                 */
                // 1)
                this.exploringVehicleComponent.Cockpit.doMoveRover(
                    Object.assign( {}, this.initEachMove, {
                        msg: `Oh... sugar!`
                            + `<br>Have to get <strong>Automatic pilot</strong> from deployment point...`
                            + `<br>Need it to get me into the Init Position!`
                        , delayCSSTransition: previousSpeed
                        , waitForCSSTransition: false
                    })
                );

                setTimeout(() => {
                    // 2)
                    const toThisPosition: Position = this.landingData.startingPointOnPlanet;
                    // Undo planet zooming, to operate on 'planet-bg-img', with same amplitude as in '.grid-container'
                    this.sendPlanetToZoomOut.emit();
                    // Start automatic pilot towards Init Position (you are now on {0,0}, or arrived position of last journey):
                    // --------------------------------------------------------------------------------------------------------
                    this.renderer.setStyle(this.exploringVehicleIcon, 'transition-duration', (timeToMoveToInitPos / 1000).toFixed(2) + 's');
                    // --------------------------------------------------------------------------------------------------------
                    this.exploringVehicleComponent.Cockpit.doMoveRover(
                        Object.assign( {}, this.initEachMove, {
                            pointToGoTo: toThisPosition
                            , msg: `Ahead to the initial position @ <strong>{${
                                this.landingData.startingPointOnPlanet.coordinates.x}, ${this.landingData.startingPointOnPlanet.coordinates.y}}</strong>, `
                            + `<span class="text-info"><strong>`
                            +     `${(this.exploringVehicleComponent.Cockpit.upperCase(this.landingData.startingPointOnPlanet.orientation as OrientationChar))}`
                            + `</strong></span>.`
                            , delayCSSTransition: timeToMoveToInitPos
                        })
                    );
                    setTimeout(() =>
                        // Return to Rover's (user set lag time) speed:
                        // --------------------------------------------------------------------------------------------------------
                        this.renderer.setStyle(this.exploringVehicleIcon, 'transition-duration', previousSpeed)
                        // --------------------------------------------------------------------------------------------------------
                        // Start exploring journey: the last (below) return LandingOperationsTimmings timer Object SHOULD have trigger it!
                    , timeToMoveToInitPos);
                }, autoPilotDrivingTime);

                // we can return to emit back to 'deployed-expedition' so "doMarsGrid()" can draw/re-draw all action grid in Mars:
                return {
                    shuttleTime: 0
                    , roverTime: 0
                    , astronautTime: timeToMoveToInitPos
                    , totalLandRoverOnMarsTime: timeToMoveToInitPos + autoPilotDrivingTime
                };
            }
        }

    }

    /**
     * Istructions have been received and we have to put an Astronaut exploring Mars, by these Instructions.
     * Meanwhile Rover (the fake one, now baptized 'shuttle-conveyor') is beeing randomly circling around.
     *
     * TODO:
     * ==============================================================================================================
     * Should be nice, while still circling, to "move", first, its orbiting circle to our journey grid (indexed to the number of unit commands),
     * where, now, yes, Rover shuttle will dive into, uppon these just arrived commands.
     * ==============================================================================================================
     *
     * We have two important moments on this "land Rover on Mars" operations time:
     * 1) Stop (fake) Rover to be circling around, land it and get prepared for 1st take off position
     * 2) Once there, real Rover gets born ;-), carrying an Astronaut inside, to explore Mars.
     *    So, this is when our Astronaut puts on a (same Rover color) space suit and REALLY gets deployed to Mars soil!
     *
     * @param thisPosition 1st take off position in EACH of the user submited Rover's journey Instructions.
     */
    private landExploringVehicleOnPlanet(thisPosition: Position): LandingOperationsTimmings {
        // Fake Rover gets zoomed out (like landing!) to center position
        // and then, the original one, to commanded travelling init position.
        // --------------------------------------------------------------------------------------
        // About TIMMINGS - NOTE that startJourney() depends on this total setup timming number!
        // --------------------------------------------------------------------------------------
        const
            timeToCloseOrbit = 1000
            , timeToLand = 1400
            , timeToPark = 1000
            // Give it sufficeint time for the user to read first "Greetings from Rover\'s cockpit!" message.
            , timeToStartMovingtoInitPos = 5000
            // Next SpeakerVoice, after "timeToStartMovingtoInitPos", will be already synchronized:
            // we'll be using, at 'exploring-vehicle's Cockpit, "doMoveRover()"s method and not single "eachMoveSpeak" emitter to "network-voice-point" access)

            // Will go "quickly" on a diagonal; to calculate real time, use Pythagoras theorem:
            , timeToMoveToInitPos = this.landingData.speedOnPlanet * Math.sqrt(
                Math.pow(thisPosition.coordinates.x, 2) + Math.pow(thisPosition.coordinates.y, 2)
            )
            , timeToLearnHowToDriveRover = 3000 // ;-)
            , readySetGoDelay = 3000

            // And so, we have our landing moments in time - return them ASAP, so show can go on! ;-)
            // ===================================================================================================================================
            , timeToShuttleLands = timeToCloseOrbit + timeToLand
            , timeToRoverIsDeployed = timeToShuttleLands + timeToPark
            , timeToAstronautIsDeployed = timeToRoverIsDeployed + timeToStartMovingtoInitPos + timeToMoveToInitPos
            , timeToStartJourney = timeToAstronautIsDeployed + timeToLearnHowToDriveRover + readySetGoDelay
            // ===================================================================================================================================

        ;
        // --------------------------------------------------------------------------------------
        // About DOM TAGS envolved in this animation, at CURRENT DOM STATUS (no good to get them @ ngAfterViewInit()!):
        // --------------------------------------------------------------------------------------
        // this.exploringVehicleIcon = this.tpl.querySelector('#rover');
        // Previous is now grabbed by @ViewChild; already exists, at this code point (definately much after component's 'ngAfterViewInit()')
        this.exploringVehicleIcon = this.exploringVehicleComponent.Cockpit.exploringVehicleIcon;
        this.shuttleOrbit = this.tpl.querySelector('.waiting-orbit');
        this.shuttleConveyorIcon = this.shuttleOrbit.querySelector('.rover-in-orbit');

        // =====================================================================================
        // So, we can start the landing operations ANIMATION:
        // =====================================================================================
        this.renderer.addClass(this.shuttleOrbit, 'stop-orbiting');
        // DONE @ 'planet'! ------------ marsBgdImg.classList.add('is-zooming');
        setTimeout(() => {
            this.renderer.addClass(this.shuttleConveyorIcon, 'stop-orbiting');
            setTimeout(() => {
                // Right on the {0,0} deployment center, independently of where, in Mars, that is located:
                this.renderer.setStyle(this.shuttleConveyorIcon, 'transform', 'translateX(-50%) translateY(-50%) rotate(0deg)');

                const roverBornInsideShuttleVoice: SpeakerVoice = Object.assign( this.initNetworkVoice, {
                    msg: `Greetings from Rover\'s cockpit!`
                        + `<br>Superb landing - thanks, <strong>\'command-center\'</strong>!`
                        + `<br>`
                        + `<br>Starting engines... <span class="text-success"><i class="far fa-check-circle"></i></span>`
                        + `<br><strong>Automatic pilot on</strong> to Init Position... <span class="text-success"><i class="far fa-check-circle"></i></span>`
                        + `<br>We have a go! <span class="text-success"><i class="far fa-check-circle"></i></span>`
                    , transformAtPos: Object.assign( this.initNetworkVoice.transformAtPos, { coordinates: {x: 0, y: 2} })
                    , waitForCSSTransition: false
                });
                this.exploringVehicleComponent.Cockpit.eachMoveSpeak.emit(roverBornInsideShuttleVoice);

                // Now it's time to park the shuttle:
                setTimeout(() => {
                    this.renderer.setStyle(this.shuttleOrbit, 'animation-iteration-count', 1);
                    this.renderer.setStyle(this.shuttleConveyorIcon, 'display', 'none');

                    // Now we can bring the original to life! In Mars...
                    // -------------------------------------------------------------------------------------------------------
                    this.renderer.setStyle(this.exploringVehicleIcon, 'display', 'flex');
                    // -------------------------------------------------------------------------------------------------------
                    // And face it to the command init pretended orientation:
                    setTimeout(() => {
                        // Rover, most probaby, will do a diagonal; adjust it's speed JUST for this movement:
                        const previousSpeed = this.exploringVehicleIcon.style.transitionDuration;
                        // --------------------------------------------------------------------------------------------------------
                        this.renderer.setStyle(this.exploringVehicleIcon, 'transition-duration', (timeToMoveToInitPos / 1000).toFixed(2) + 's');
                        // --------------------------------------------------------------------------------------------------------
                        this.exploringVehicleComponent.Cockpit.doMoveRover(
                            Object.assign( {}, this.initEachMove, {
                                pointToGoTo: thisPosition
                                , msg: `Initial position to be reached @ <strong>{${thisPosition.coordinates.x}, ${thisPosition.coordinates.y}}</strong>, `
                                + `<span class="text-info"><strong>`
                                    + `${this.exploringVehicleComponent.Cockpit.upperCase(thisPosition.orientation as OrientationChar)}`
                                + `</strong></span>.`
                                + `<br>Astronauts will start exploring <strong>Mars</strong> planet!`
                                , delayCSSTransition: timeToMoveToInitPos
                            })
                        );
                        // );
                        setTimeout(() => {
                            // -------------------------------------
                            // This is the time where Rover's driver astronaut is born!
                            // -------------------------------------
                            this.sendPlanetToZoomOut.emit();    // To operate on 'planet-bg-img', with same amplitude as in '.grid-container'
                            // -------------------------------------
                            setTimeout(() => {
                                // Have a joke! ;-)))). Get back "normal" Rover speed:
                                // -------------------------------------------------------------------------------------------------------
                                this.renderer.setStyle(this.exploringVehicleIcon, 'transition-duration', previousSpeed);
                                // -------------------------------------------------------------------------------------------------------
                                const roverIsFindingGearVoice: SpeakerVoice = Object.assign( this.initNetworkVoice, {
                                    msg: `<strong><span class="text-danger">HELP!</span></strong>`
                                        + `<br>Where's the gear, <strong>Ambrósio</strong>...?!?!?`
                                    , transformAtPos: thisPosition
                                    , waitForCSSTransition: false
                                });
                                this.exploringVehicleComponent.Cockpit.eachMoveSpeak.emit(roverIsFindingGearVoice);

                                setTimeout(() =>
                                    this.exploringVehicleComponent.Cockpit.eachMoveSpeak.emit(
                                        Object.assign( this.initNetworkVoice, {
                                            msg: ''
                                            , transformAtPos: thisPosition
                                            , waitForCSSTransition: false
                                            , toShow: false
                                        })
                                    )
                                , readySetGoDelay);

                            }, timeToLearnHowToDriveRover);
                        }, timeToMoveToInitPos);
                    }, timeToStartMovingtoInitPos);
                }, timeToPark);    // CSS transition smoothing time - after reaching {0,0} stop spinning and assume FontAwesome natural init facing (E)
            }, timeToLand);        // CSS transition smoothing time - shrink fake Rover and repalce it by original!
        }, timeToCloseOrbit);      // CSS transition smoothing time - wait for orbit to converge to center

        // The 4 moments needed timmings at 'space-expedition':
        return  {
            shuttleTime: timeToShuttleLands
            , roverTime: timeToRoverIsDeployed
            , astronautTime: timeToAstronautIsDeployed
            , totalLandRoverOnMarsTime: timeToStartJourney
            // Need this Pythagoras theorem calculated, and isolated, time:
            , autoPilotTime: timeToMoveToInitPos
        };
    }

}
