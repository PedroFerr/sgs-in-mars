import { Component, Input, Output, EventEmitter, OnChanges, SimpleChanges, AfterViewInit, Renderer2, ElementRef } from '@angular/core';

import {
    GPS, JourneyRoadMap
    , CSStransformPos, CSSzeroPos
    , DeploymentPlace, LandingOperationsData
    , MoveReport
} from 'src/app/app.interfaces';

import * as JsPlugIns from './../../_js-utils';

// Import JSON file, in '*.json' format, with 'require':
declare var require: any;
// Use JQuery on this component...?
declare var $: any;

@Component({
    // IMPORTANT! Check it: https://stackoverflow.com/a/42019477/2816279
    // A '-' dash is COMPOLSURY at selector's name!
    // Otherwise 'CUSTOM_ELEMENTS_SCHEMA' won't WORK at this module '...module.ts' file!
    selector: 'planet-x',
    templateUrl: './planet.html',
    styleUrls: ['./planet.css']
})
export class PlanetComponent implements OnChanges, AfterViewInit {

    // Grab this component's HTML - it wlll be manipulated a lot:
    tpl: HTMLElement;
    planet: HTMLElement;

    // User can choose the deployment center, from an array of them:
    deploymentPoints: Array<DeploymentPlace> = require('../../_data/mars-places.json');

    // First of all, we have to land our 'shuttle convoyer', stopping its '.waiting-orbit' @ 'deployed-expedition'. Next is an Angular's component Input to it:
    @Input() landingData: LandingOperationsData;
    // ... which we'll fill in with 'landingData' Data, coming from parent 'space-expedition':
    sendDataToShuttleConveyor: LandingOperationsData;

    @Input() receiveDeployedPos: GPS;
    currentDeployedPos: GPS;
    sendDeployedPos: GPS;

    @Input() currentJourneyRoadMap: JourneyRoadMap;

    @Input() clickedToTransformPlanet: {
        askedMovement:  'L' | 'R' | 'U' | 'D' | 'no-translation' | 'in' | 'out' | 'no-zoom' | '+90º' | '-90º' | 'no-rotation'
        , cumulateClicksTillTime: number
    };
    // And, once set of cliskc is processed:
    @Output() finishTransformPlanetSetOfClicks = new EventEmitter();

    // Next will be the one "suffering" translation/rotaion moves of 'planet' - will cumlate user set of clicks to see the planet spinning (clickToTransformPlanet()):
    private gridContainerPos = Object.assign({}, CSSzeroPos) as CSStransformPos;

    // Once real Rover gets born ;-), carrying an Astronaut inside, to explore Mars,
    // will get to Instructions "init position" => the astronaut is considered deployed on Mars
    // isAstronautDeployed = false;
    // After being deployed, next boolean can be:
    // * TRUE: if Astronaut is touching Mars soil, it will have to stick with Mars cartesian cardinals
    //         - if MARS rotates (translates, zoom), Astronaut (and Rover) rotates (translates, zoom) the same with it.
    // * FALSE: if Rover is still travelling, on the air, not having Astronaut fixed at Mars soil, it will follow Earth compass
    //         - it doesn't matter if Mars rotates (translates, zoom), it should NEVER alter its EARTH (#deployment-point-compass) relative position.
    isAstronautOnMarsSoil = false;
    // In the end of each journey, on 'exploring-vehicle's last "eachMove",
    // sent back final journey results:
    @Output() finishExplorationJourney = new EventEmitter<MoveReport>();

    // Now we have the most complex issue of the all App:
    // Figure it out what will be the achieved position AFTER a rotated angle is translated or after a translation is rotated by some angle.
    // So we console log it for better tracking/debugging - if you want, activate it here by changing its boolean.
    // tslint:disable:variable-name
    private log_rotatedTranslations_translatedRotations_report = false;

    constructor(
        private thisDOM: ElementRef,
        private renderer: Renderer2
    ) { }

    ngOnChanges(propChanging: SimpleChanges) {
        if (propChanging && propChanging.landingData && !propChanging.landingData.firstChange) {
            /**
             * Pass to 'shuttle-conveyor' the 'landingData', so it can deploy 'exploring-vehicle' @ 'deployed-expedition' point, on the right time.
             *
             * Meanwhile, as Instructions have come, start zooming IN this 'planet'.
             * We'll zoom it OUT (to scale(1)) as soon as 'shuttle-conveyor' tell us to
             */
            this.sendDataToShuttleConveyor = Object.assign( {}, this.landingData);
            this.zoomPlanet('in');
        }

        if (propChanging && propChanging.receiveDeployedPos && !propChanging.receiveDeployedPos.firstChange) {
            // Carry on to 'deployed-expedition' (to do '.grid-container' CSS transform)
            this.sendDeployedPos = Object.assign( {}, this.receiveDeployedPos);

            // TODO!!!   TODO!!!   TODO!!!   TODO!!!   TODO!!!   TODO!!!   TODO!!!   TODO!!!   TODO!!!
            //
            // WAIT! ================== DO WE..??????????????????? ==================
            // We also need it here, and maybe it get CHANGED @ this.clickToTransformPlanet() - see end of it:
            this.currentDeployedPos = Object.assign( {}, this.receiveDeployedPos);
            //  ================== PLEASE CHECK IT! ==================
        }

        if (propChanging && propChanging.clickedToTransformPlanet && !propChanging.clickedToTransformPlanet.firstChange) {
            this.clickToTransformPlanet(this.clickedToTransformPlanet);

        }
    }

    ngAfterViewInit() {

        this.tpl = this.thisDOM.nativeElement;
        this.planet = this.thisDOM.nativeElement.querySelector('.planet-bg-img');

        /**
         * Initiate '.fa-map-marker-alt' BS 4 tooltips LABELS @ '<div class="mars-points-container">'.
         * Avoid rendering problems in more complex components (like our input groups, button groups, etc).
         */
        $('[data-toggle="tooltip-marker"]').tooltip(
            {
                container: '.mars-points-container'
                , html: true
            }
        );
        $('input[rel="txtTooltip"]').tooltip({ container: 'body' });

    }

    /**
     * REMEMBER zooming this planet background-image in/out (by CSS class '.is-zooming')
     * not only has a transition time defined, as a 'timing-function' curve.
     * => count with them on any animations setup!
     *
     * @param type zoom 'in' (CSS transform'ing planet scale > 1) or 'out' (CSS transform'ing planet scale = 1)
     *      We'll zoom it OUT (to scale(1)) as soon as 'shuttle-conveyor' tell us to,
     *      by 'deployed-expedition's 'space-vehicle-fleet' child-container,
     *      @Output() sendPlanetToZoomOut EventEmitter().
     */
    zoomPlanet(type: 'in' | 'out') {
        switch (type) {
            case 'in':
                this.renderer.addClass(this.planet, 'is-zooming');
                break;
            case 'out':
                this.renderer.removeClass(this.planet, 'is-zooming');
                break;

            default:
                break;
        }
    }

    updateIsAstronautOnMarsSoil(evt: boolean) {
        this.isAstronautOnMarsSoil = evt;
    }

    /**
     * Cumulate user clicks, to CSS transform '.grid-container' layer to any pos/value @ Mars.
     *
     * First separate each kind of 'transform'ation the user pretends, on clicking each specific icon.
     * We compute the 'add up' result and apply NEW CSS 'transform' to '.grid-container'
     *
     * Keep in mind NOT to apply successive cumulated CSS style properties, but the SAME CSS 'transform' COMPLETE property methods, with a cumulated input.
     * Also don't forget we have codded CSS transitions in most layers => so wait for the 'askedMovement' to fully has it pretended effects before allowing a new one
     *
     * Then we'll have to "undo" EACH of these '.grid-container' moves/rotates/zooms...
     * Let's see:
     * Once we start 'transform'ing '.planet-bg-img' ('.grid-container's {0,0} deployment referential on Mars),
     * by applying 'gridContainerPos' var to its CSS, '.grid-container' will move AGAIN, not by CSS apply, but because it ALWAYS follows it's {0,0} deployment referential! ;-)
     * So... we have to apply a 2nd UNDO CSS 'transform'ation on '.grid-container'!
     *
     * By doing these 2 'transform'ing CSS apply onto 'grid-container', and decopling each 'transition-delay', we get a nice parallax effect!
     * Anyway, at the end... '.planet-bg-img' and 'grid-container' MUST ALWAYS, under ANY circunstance, ALWAYS be EXACTLY at the SAME point.
     * They still have to relate the same as before user click actions - but Mars planet (background image) will, visually, be 'transform'ed! ;-)
     *
     * In fact... NOTHING happened! ;-))
     * And that's what it should happen -the only way to change Rover position @ Mars is by 'command-center' command input form.
     * But user got a nice, incredible, 2D USER EXPERIENCE! That's the reason Front End coders are so needed..! ;-)
     *
     * @param askedMovement the pre-defined type of 'transform' movement user is asking for @ Mars
     */
    clickToTransformPlanet(
        userInput: {
            askedMovement: 'L' | 'R' | 'U' | 'D' | 'no-translation' | 'in' | 'out' | 'no-zoom' | '+90º' | '-90º' | 'no-rotation'
            , cumulateClicksTillTime: number
        }
    ) {
        const
            // On EACH user SET of clicks, start a CLEAN CSStransformPos Object,
            // so we can CSS revert, at the end of each SET, what's beeing cumulated on "this.gridContainerPos".

            // CSS 'transform' style can be either on translate, scale or rotate - sticking only with "this.gridContainerPos" cumulator,
            // we would never knew details of what had happen in the pass - it's NOT ONLY the LAST user set of clicks to undo...
            // 'toUndoPos' creation, and correct filling, revealed to be absolutely crucial to achieve some spectacular amusing UI/UX parallax geometric effects!

            // If Rover is touching Mars soil, no need to undo nothing related to its deployed point
            //     - it is already following it; just move Mars, that will move the deployment point, and Rover will go along
            // IF it's on the air, you need to "undo" the advanced "this.currentDeployedPos"
            //     - you move Mars and points get transformed; Rover is still following the (not moving with Mars) Earth compass (#deployment-point-compass)
            toUndoPos = this.isAstronautOnMarsSoil ?
                Object.assign({}, CSSzeroPos) as CSStransformPos
                :
                Object.assign({}, CSSzeroPos, this.currentDeployedPos) as CSStransformPos

        ;
        switch (userInput.askedMovement) {
            case 'L':
            case 'R':
                this.transformOn('x', userInput.askedMovement, toUndoPos);
                break;
            case 'U':
            case 'D':
                this.transformOn('y', userInput.askedMovement, toUndoPos);
                break;
            case 'no-translation':
                Object.assign(this.gridContainerPos, {x: 0, y: 0});
                Object.assign(toUndoPos, {x: 0, y: 0}, { rotate: -1 * this.gridContainerPos.rotate, scale: this.gridContainerPos.scale });
                break;

            case 'in':
            case 'out':
            case 'no-zoom':
                this.transformZoom(userInput.askedMovement, toUndoPos);
                break;

            case '+90º':
            case '-90º':
            case 'no-rotation':
                this.transformRotation(userInput.askedMovement, toUndoPos);
                break;

            default:
                break;
        }

        // Buffer the user clicks - only enter CSS changing after 'cumulateClicksTillTime' time of cumulating user clicks:
        let nClicks = 0;
        setTimeout(() => {

            const
                // What we have to do concerning GPS transform styling:
                cssTransform = '' +
                    `translate(`
                        + `${this.gridContainerPos.x}rem, ${this.gridContainerPos.y}rem`
                        + `) scale(${this.gridContainerPos.scale})`
                        + ` rotate(${this.gridContainerPos.rotate}deg)`

                // On Mars, background img has it's "center" at top left container's corner => Movement along Y (+/- 'top') is always negative, below '0'
                , transformTranslate = this.isAstronautOnMarsSoil ?
                    `${this.currentDeployedPos.x}rem, calc(-1 * ${this.currentDeployedPos.y}rem)`
                    :
                    `calc(${this.currentDeployedPos.x}rem + ${toUndoPos.x}rem), calc(-1 * calc(${this.currentDeployedPos.y}rem + ${toUndoPos.y}rem))`

                , cssUndoTransform = `translate(` + transformTranslate + `)`
                    + ` scale(${toUndoPos.scale})`
                    + ` rotate(${toUndoPos.rotate}deg)`

                // ... and concerning timmings to apply CSS transform and undoTransform:
                , marsTransitionDuration = 3
                , parallaxDeepness = 1.5

            ;

            nClicks ++;
            let lastClick = true;
            // Identify EACH of these 2nd setTimeout()s with a unique string Id, so you can track them:
            const uniqueTimerId = `Click #${nClicks}`;
            JsPlugIns.waitForFinalEvent ( () => {
                if (lastClick) {

                    $('.planet-bg-img')
                        // Set parallax deepness, on both related referentials
                        // NOTE that Rover should move SLOWER, giving the (parallax effect!) REAL impression it's being dragged by Mars 'transform'!
                        .css('transition-duration', `${marsTransitionDuration}s`)
                        .find('.grid-container')
                            .css('transition-duration', `${marsTransitionDuration * parallaxDeepness}s`)
                            .end()

                        // Apply transform @ Mars:
                        .css('transform', cssTransform)

                        // Keep Rover on the same place - its reference movement ('.planet-bg-img') will pull it!
                        .find('.grid-container')
                            .css('transform', cssUndoTransform)
                            .end()
                        .end()

                    ;
                    // -------------------------------------------------------------------------------
                    // If you need, you can debug both CSS transforms - switch is on top of this TS code:
                    if (this.log_rotatedTranslations_translatedRotations_report) {
                        console.warn('Achieve:');
                        console.log(cssTransform);
                        console.warn('to UNDO:');
                        console.log(cssUndoTransform);
                    }
                    // -------------------------------------------------------------------------------

                    // Only do ONCE this inner setTimeout() code - the "last" one (that's why need to control each)
                    lastClick = false;
                    // Emit bak to 'space-expedition' to allow new set of user clicks:
                    this.finishTransformPlanetSetOfClicks.emit();

                    // Pass the var to the Template so 'deployed-expedition' can also UPDATE the '.grid-container' CSS transform
                    // this.sendDeployedPos = Object.assign( {}, this.receiveDeployedPos);
                }
            }, (userInput.cumulateClicksTillTime / 2), uniqueTimerId);

        }, userInput.cumulateClicksTillTime);
    }

    /**
     * Next are a series of block codes of upper "switch(userInput.askedMovement)" that execute the possible actions user can do
     * by clicking on any 'buttons-container' button, of THIS template's 'section.ui-ux-controller'.
     * Just a code clearence.
     */
    private transformOn(overAxis: string, askedMovement: 'L' | 'R' | 'U' | 'D', toUndoPos: CSStransformPos) {
        const
            newValue = Number(
                askedMovement
                    .replace('L', '1').replace('R', '-1')
                    .replace('D', '1').replace('U', '-1')
            )

        ;
        this.gridContainerPos[overAxis] = this.gridContainerPos[overAxis] + Math.round(newValue);
        const
            newTranslation: GPS = this.checkRotatedTranslations(this.gridContainerPos.rotate, this.gridContainerPos)
            , newFactorZoom = (1 / this.gridContainerPos.scale)    // Equals to "1" if if no zoom was acumulated

        ;
        /**
         * The result comming from 'checkRotatedTranslations()' is the magic trick:
         * to completely "undo" CSS so far cumulated for the X axis, you need to do it in both axys - Y could also have been cumulating, meanwhile.
         * And maybe user has also asked for a rotation, some point back... or even some zooming! ;-)
         * Get's complex...
         *
         * But there's more; and if he/she did asked from some previous (to this translation) rotation...we have a lot more hypothesis to account for.
         * Again axis values:
         * they might have been (orientation) switched, between Rover and Mars => when Mars is growing up the Y axis (Y++), Rover might be down the X axis (X--)...
         *
         * So, what we really have to do is to re-calculate EVERYTHING back, knowing what was done,
         * to be able to UNDO it, independently of the "what" ANYTHING that was touched by this user's click set.
         */

        // Undo everything:
        Object.assign(toUndoPos, {
            x: -1 * newTranslation.x * newFactorZoom
            , y: newTranslation.y * newFactorZoom    // This would be (-1 * (-1 * newTranslation.y * newFactorZoom))
            , rotate: -1 * this.gridContainerPos.rotate
            , scale: this.gridContainerPos.scale
        });

    }

    private transformZoom(askedMovement: 'in' | 'out' | 'no-zoom', toUndoPos: CSStransformPos) {
        let newValue: number;
        const zoomUnit = 0.1;

        if (askedMovement === 'in') {
            newValue = this.gridContainerPos.scale + zoomUnit;
        } else if (askedMovement === 'out') {
            // Mind you a scale(0) demand (when current zoomUnit = 0.1) will make obj to disappear; and there are no negative zoomings (just < 1, to zoom OUT)
            newValue = (this.gridContainerPos.scale !== zoomUnit) ? (this.gridContainerPos.scale - zoomUnit) : zoomUnit;
        } else if (askedMovement === 'no-zoom') {
            newValue = 1;
        }
        Object.assign(this.gridContainerPos, { scale: Number(newValue.toFixed(1)) });

        const
            newTranslation: GPS = this.checkRotatedTranslations(this.gridContainerPos.rotate, this.gridContainerPos)
            , newFactorZoom = (1 / this.gridContainerPos.scale)    // Equals to "1" if no 'no-zoom' is asked for

        ;
        Object.assign(toUndoPos, {
            scale: Number(newValue.toFixed(1))
            , x: -1 * newTranslation.x * newFactorZoom
            , y: newTranslation.y * newFactorZoom
            , rotate: -1 * this.gridContainerPos.rotate });
    }

    private transformRotation(askedMovement: '+90º' | '-90º' | 'no-rotation', toUndoPos: CSStransformPos) {
        let newValue: number;
        /**
         * Again, ROTATION is VERY tricky, my friend... particulary when combinated with TRANSLATIONs!
         * And, on top, mind you it gets different either after, or before, a rotation is going to be/was done
         *
         * Next "checkRotatedTranslations()" (that could be also called "checkTranslatedRotations()" ) is the heart if this hole clickToTransformPlanet() method
         * Again... recomended nice to read (I did!):
         *     - applying sequential transform's: https://stackoverflow.com/a/10765771/2816279
         *     - understanding the Axis and rotate CSS command : https://css-tricks.com/get-value-of-css-rotation-through-javascript/
         */
        if (askedMovement === '+90º') {
            newValue = this.gridContainerPos.rotate + 90;
        } else if (askedMovement === '-90º') {
            newValue = this.gridContainerPos.rotate - 90;
        } else if (askedMovement === 'no-rotation') {
            newValue = 0;
        }

        const
            newRotation: GPS = this.checkRotatedTranslations(newValue, this.gridContainerPos)
            , newFactorZoom = (1 / this.gridContainerPos.scale)    // Equals to "1" if no zoom was acumulated
        ;

        Object.assign(this.gridContainerPos, { rotate: newValue });
        Object.assign(toUndoPos, {
            rotate: -1 * newValue
            , x: -1 * newRotation.x * newFactorZoom
            , y: newRotation.y * newFactorZoom
            , scale: this.gridContainerPos.scale
        });
    }

    /**
     * When you pretend to translate an object already rotated
     * or
     * pretend to rotate an object already translated
     * ...
     * You need to relate the Axys directions of the "to-position" (given here by the input 'coordinates')
     * Axis values might switch => when before GPS coordinates were growing up the Y axis (Y++),
     * after a CSS 'transform'ation, they could be down the X axis (X--)... or up... or the other way around... or neither! ;-)
     *
     *              ---- // ----
     * Note that this could be easily and automatically done (as it has been, so far, with all referentials) by CSS rotating relative to the
     * CENTER (calc(+/-50% +/- xrem)) of the parent... IF the parent was flex-center diplayed. :-(
     * But we simply CANNOT flex-center <section class="mars" />, because of BS4 grid, and corresponding responsiveness,
     * that is supporting the complete structure - not only 'section.mars' @ 'space-expedition' <main />.
     * If you inspect, 'deployment-point-compass' is the only refrential being centered by absolute position top/left: 50% - not by flex-box
     *
     * The center of the center (;-) here is the top left corner of the parent.
     *
     * -------------------------------------------------------------------------------
     * Due to it's tracking complexity
     * (keep inspecting CSS transformed strings in 2 compasses HTML objects, combined with what you just have done (clicked) previously)
     * we provide a special console.log of the action done and the results given by the code.
     * It's activated on a top var boolean ("log_rotatedTranslations_translatedRotations_report")
     * -------------------------------------------------------------------------------
     *
     * @param rotAngle the angle that will be translated OR the rotation of some previous translation
     * @param coordinates the "to-translate", or the "to-rotate", or "to-zoom", Position after whatever suffered transform
     */
    private checkRotatedTranslations (rotAngle: number, coordinates: GPS) {
        // ... or "checkTranslatedRotations()" could be also be a good name for this method! ;-)
        let newTranslation: GPS;
        if (this.log_rotatedTranslations_translatedRotations_report) {
            console.error('-----------------------');
            console.log('Angle to/being rotated', rotAngle % 360);
        }
        switch (rotAngle % 360) {
            case 90:
            case -270:
                // Axys will exchange directions (x|y) => Mars -y = Rover +x
                newTranslation = { x: coordinates.y , y:  -1 * coordinates.x };
                break;
            case 0:
            case -0:
            case 360:
            case -360:
                newTranslation = { x: coordinates.x , y: coordinates.y };
                break;
            case 180:
            case -180:
                newTranslation = { x:  -1 * coordinates.x , y:  -1 * coordinates.y };
                break;
            case -90:
            case 270:
                newTranslation = { x:  -1 * coordinates.y , y: coordinates.x };
                break;

            default:
                break;
        }
        return newTranslation;

    }


}
