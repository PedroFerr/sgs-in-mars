import {
    Component, Optional, AfterViewInit, OnChanges, SimpleChanges
    , Input, Output, EventEmitter
    , ElementRef, Renderer2
} from '@angular/core';

import { AppComponent } from 'src/app/app.component';

import {
    GPS, JourneyRoadMap
    , LandingOperationsData, LandingOperationsTimmings
    , VehicleInstructions, Instructions, Position
    , MoveReport
    , SpeakerVoice, SpeakerVoiceInitializer
} from 'src/app/app.interfaces';

@Component({
    selector: 'deployed-expedition',
    templateUrl: './deployed-expedition.html',
    styleUrls: ['./deployed-expedition.css']
})
export class DeployedExpeditionComponent implements AfterViewInit, OnChanges {

    // Grab this component's HTML - it wlll be manipulated a lot - and its main HTML tags actors:
    tpl: HTMLElement;

    // Do our HTML Mars restricted area, limiting Rover's driving - he can cross it; but will be flagged.
    // Rover's movements, and restricted area, lie on a 'gridCells' square grid of "nXcells x nXcells",
    // depending, obviously, on the number of command units submited by 'command-center'.
    // Should allow all possible issued movements/unit commands ('L', 'R' or 'A') to the vehicle.
    // Next are vars used at this HTML template - "nXcells" will fill 'vehicle-fleet-operations' CHILDREN Input() nXcells.
    nXcells: number;
    gridCells: number[];
    // ... and the initial start point of it, where we should draw our "this.gridCells" movement's grid, CSS translating it to there from {0,0}, as Rover did:
    gridCSStranslate: string;
    // ... that will afect the same way the restricted area positioning
    restrictedAreaCSStranslate: string;
    // Style will be applied once, with all pretended changes:
    gridItemsWrapperStyle: any;


    // First of all, we have to land our 'shuttle convoyer', stopping its '.waiting-orbit'. Next is an Angular's component Input to it:
    @Input() landingData: LandingOperationsData;
    // ... which we'll fill in with 'landingData', coming from parent 'planet':
    sendDataToShuttleConveyor: LandingOperationsData;

    // Expedition Instructions, submitted by the user @ 'command-center', need to pass through this 'deployed-expedition'
    vehicleInstructions: VehicleInstructions;

    @Input() deployedPos: GPS;

    @Input() currentJourneyRoadMap: JourneyRoadMap;

    /**
     * Voice communications receiver FROM ANY 'network-voice-point'
     *
     * Sees its value filled with a 'talk' (object with params),
     * so BS4 Tooltip appears with some voice 'msg', on the right 'transformAtPos' place and at the right 'waitForCSSTransition' time moment
     */
    sendVoiceToTooltip: SpeakerVoice;

    @Output() sendPlanetToZoomOut = new EventEmitter();
    // In the end of each journey, on 'exploring-vehicle's last "eachMove",
    // signal back Astronaut is no longer on Mars soil and sent back final journey results:
    @Output() isAstronautOnMarsSoil = new EventEmitter<boolean>();
    @Output() finishExplorationJourney = new EventEmitter<MoveReport>();


    constructor(
        private thisDOM: ElementRef,
        private renderer: Renderer2,
        /**
         * Have access to Host App component (parent of all Components) Methods AS A SERVICE!
         * BUT BE CAREFULL!
         *
         * If "AppComponent" can not (still...) be found by Angular, an error will be thrown, on mounting it!
         * And it can happen NO ERROR is thrown at all - simply THIS Component won't work at all, which is even more scary...
         *
         * "Yes you can use (and MUST, here)
         *      constructor (@Host() @SkipSelf() @Optional() toys: ToysService) {...}"
         * " - check it here: https://medium.com/frontend-coach/self-or-optional-host-the-visual-guide-to-angular-di-decorators-73fbbb5c8658
         *
         * We're NOT "calling" a component!
         * We are INJECTING a SERVICE, that will bring all 'app' properties, at this Component's Class constructor!
         *
         * This also makes MANDATORY to have ANY 'template' (even if empty!) declared @Component!
         */
        // @Host() private appComponent: AppComponent <= throws an ERROR:
        //      Template parse errors: No provider for AppComponent (...): ng:///AppModule/CommandCenterComponent.html@302:4 (first <network-data-point /> component call)
        @Optional() private appComponent: AppComponent
    ) { }

    ngOnChanges(propChanging: SimpleChanges) {

        if (propChanging && propChanging.landingData && !propChanging.landingData.firstChange) {
             // We should have our 'landingData' content, arrived from 'planet':
            this.sendDataToShuttleConveyor = Object.assign( {}, this.landingData);
            // If so, expedition Instructions were also submitted to App component and are now available for us.
            // Let's transform them from Instructions to VehicleInstructions, precisele using the method App also used, when retransmiting them to Rover:
            const userSubmited: Instructions = this.appComponent.userSetUpInstructions;
            this.vehicleInstructions = Object.assign( userSubmited,
                { charCommand: this.appComponent.prepareInstructions(userSubmited.command) }
            );
            // console.error('Instructions @ "deployed-expedition" Angular container: ', userSubmited,  this.vehicleInstructions);

            /**
             * We have, NOW, all the landing moments calculated for THIS Instructions user submission => time to draw THE grid @ planet soil!
             *
             * Design a (movements referential of Rover) fixed grid, on Mars soil, with bullseye @ {0,0}.
             * Attach to it our Rover's restricted area, with bottom left corner on the referential center.
             *
             * Take into consideration:
             *     * grid must have TWICE the number of unit commands on X and on Y, to assure Rover travels within it
             *     * add a "0" center grid-item
             *     * because of final 'planet-bg-img is-zooming' zooming - finishing on 1.2 - we SHOULD give it more 20% of 'grid-item's, both tails (bigger grids)
             *
             * The grid should fill "this.gridCells: number[]" at each new received Instructions.
             */
            this.gridCells = [];
            const
                nCommUnit = this.vehicleInstructions.charCommand.length
                , sufficientAxisCells = (1 + nCommUnit * 2)
                , extraTail = Math.ceil(nCommUnit * 0.2)

            ;
            // We will SEE exactly "sufficientAxisCells", if, after zooming, 'planet-bg-img is-zooming'
            this.nXcells = sufficientAxisCells + 2 * extraTail;
            for (let n = 0; n < (this.nXcells * this.nXcells); n++) {
                this.gridCells.push(n);
            }

        }

        if (propChanging && propChanging.deployedPos && !propChanging.deployedPos.firstChange) {
            // The full expedition content's "deployedPos" can exist/change:
            // 1st at "space-expedition"s ngOnChanges() of "this.instructions"

            // ================== PLEASE CHECK IT on parent 'planet' ngOnChanges() ! ==================
            // or when we CSS TRANSFORM 'planet's on clickToTransformPlanet()
            // ========================================================================================
            this.renderer.setStyle(
                this.tpl.querySelector('.grid-container'), 'transform'
                    // On Mars, background img has it's "center" at top left container's corner
                    // => Movement along Y (+/- 'top' CSS) is always negative, below '0':
                    //     * Y increase/decrease must be inverted
                    , `translate(${this.deployedPos.x}rem, ${-1 * this.deployedPos.y}rem)`
                )
            ;

        }

        if (propChanging && propChanging.currentJourneyRoadMap && !propChanging.currentJourneyRoadMap.firstChange) {
            // console.error('A new "currentJourneyRoadMap" has arrived @ \'deployed-expedition\'!');

            // Initialize our <speaker-comms /> Template - soon it will receive SpeakerVoice Objects from ANY <network-voice-point /> access:
            // Remember "SpeakerVoiceInitializer" is a "const" - you can not change it...
            const voidVoice: SpeakerVoice = Object.assign( {}, SpeakerVoiceInitializer, {
                msg: ''
                , waitForCSSTransition: false
                , toShow: false
            });
            // this.sendVoiceToTooltip = voidVoice;
            // this.sendVoiceToSpeakerComms(Object.assign( {}, SpeakerVoiceInitializer));
            this.sendVoiceToSpeakerComms(voidVoice);

        }
    }

    ngAfterViewInit() {
        this.tpl = this.thisDOM.nativeElement.querySelector('section.expedition-wrapper');
    }

    sendVoiceToSpeakerComms(withThisTalk: SpeakerVoice) {
        this.sendVoiceToTooltip = withThisTalk;
    }

    /**
     * ---------------------------------------------------------------------------------
     * We have all the landing moments calculated @ CHILDREN 'vehicle-fleet'
     * for THIS Instructions user submission => time to draw THE grid @ planet soil!
     * ---------------------------------------------------------------------------------
     *
     * We need the user submited Instructions @ 'command-center' form
     * and init position from the previously arrived "landingData",
     * and, from this 'timerObj' param, we need the total acumulated time.
     *
     * Move/zoom the fixed 'grid-items-wrapper' grid (shrinking its '.grid-container'),
     * so it perfectly fits into '<section class="mars" />' => Rover journey is ALWAYS in sight.
     * While you do that, apply some CSS effect, by inserting a new class onto it.
     *
     * Please notice that (apart from padding/margin of <main> 'space-expedition') this section it's our user window to completely see Mars!
     * (see top of space-expedition.component.css for further details)
     *
     * @param timerObj an Object with all animation points of landing operations (nvolves 'shuttle-conveyor' and 'exploring-vehicle')
     */
    doMarsGrid(timerObj: LandingOperationsTimmings) {
        const
            atThisPosition: Position = this.landingData.startingPointOnPlanet
            , aferTimeToDrawIt: number = timerObj.totalLandRoverOnMarsTime
            , gridItemsWrapper = this.tpl.querySelector('.grid-items-wrapper')
            , restrictedArea = this.tpl.querySelector('#restricted-area')

        ;
        // Turn it off and slide to eventual new re-allocation area:
        this.renderer.addClass(gridItemsWrapper, 'is-re-allocating');
        // As it has 1s of effect and 1s of delay:
        setTimeout(() => this.renderer.removeClass(gridItemsWrapper, 'is-re-allocating'), 2000);

        // Place it on Rover's journey init position, with the size done by "this.nXcells":
        this.renderer.setStyle(gridItemsWrapper, 'transform', 'translate('
            + `calc( -50% + ${atThisPosition.coordinates.x}rem + ${(this.nXcells - 1) / 2}rem + 0.5rem ), `
            + `calc( -50% - (${atThisPosition.coordinates.y}rem - ${(this.nXcells - 1) / 2}rem - 0.5rem)`
            + `)`
        );
        this.renderer.setStyle(gridItemsWrapper, 'height', this.nXcells + 'rem');
        this.renderer.setStyle(gridItemsWrapper, 'width', this.nXcells + 'rem');


        // Draw the restricted area:
        this.renderer.setStyle(restrictedArea, 'height', `${this.vehicleInstructions.roverLimits.area.height}rem`);
        this.renderer.setStyle(restrictedArea, 'width', `${this.vehicleInstructions.roverLimits.area.width}rem`);
        // ... placed with bottom left corner @ the center point and NOT at the init position!
        this.renderer.setStyle(restrictedArea, 'transform', 'translate('
            //     // + `translateX( calc( -50% + (${this.position.coordinates.x}rem + 1rem ) ) )`
            //     // + ` translateY( calc( -50% - (${this.position.coordinates.y}rem + 1rem) ) )`
            + `calc( -50% + ${this.vehicleInstructions.roverLimits.area.width / 2}rem), `
            + `calc( -50% - ${this.vehicleInstructions.roverLimits.area.height / 2}rem)`
            + `)`
        );


        /**
         * ==========================
         *       !!  TODO !!
         * ==========================
         * For now, we do it manually triggering "Zoom" and "Drag" events on buttons.
         * Doing it atomatically... it's too complex!
         * There's a lot of vars to take into account.
         * Maybe one day... YES! It will be NEXT commit, now that we figured out the (yes!) the big complexity of it!
         * ==========================
         * I knew this was COMPLEX! Take a look at upper 'clickToTransformMars()' current code! ;-)
         * Almost done... and, yes, when it is, we'll be coming back here for the TODO!
         * ==========================
         *
         */
        // Adapt it, if necessary, so, in the end (after Rover gets into its init position 'aferTimeToDrawIt' time),
        // layers '.mars' and 'grid-items-wrapper' should exactly twin:
        // const
        //     mars = this.tpl.querySelector('section.mars')
        //     , gridContainer = mars.querySelector('.grid-container')
        //     , gridContent = gridContainer.querySelector('.grid-items-wrapper')
        //     // Memorize current styling - we will, eventually, adapt it:
        //     , gridContainerCurrentStyle = gridContainer.getAttribute('style')

        //     // See if we need to "move"/zoom gridContainer:
        //     , isToResize = gridContent.getBoundingClientRect().width > mars.getBoundingClientRect().width
        //     , isToMove = gridContent.getBoundingClientRect().top < mars.getBoundingClientRect().top
        //                     ||
        //                 gridContent.getBoundingClientRect().left < mars.getBoundingClientRect().left
        //                     ||
        //                 gridContent.getBoundingClientRect().right > mars.getBoundingClientRect().right
        //                     ||
        //                 gridContent.getBoundingClientRect().bottom > mars.getBoundingClientRect().bottom

        // ;
        // setTimeout(() => {
        //     // Rover is ready at init position! Measure and apply adapt:
        //     console.warn('gridContent:', gridContent, gridContent.getBoundingClientRect(), 'mars:', mars, mars.getBoundingClientRect());
        //     console.error('isToResize:', isToResize, 'isToMove:', isToMove, );

        //      if (isToResize ||  isToMove) {

        //         const
        //             // tslint:disable:variable-name
        //             convertToRem = (pixels: number, decimals: 0 | 1 | 2) => {
        //                 return(
        //                     Number( (pixels / 16).toFixed(decimals) )
        //                 );
        //             }
        //             // To zoom:
        //             , rH =  mars.getBoundingClientRect().height / gridContent.getBoundingClientRect().height
        //             , rW =  mars.getBoundingClientRect().width / gridContent.getBoundingClientRect().width
        //             , nZoom = (rH <= 1 || rW <= 1) ?
        //                 rH < rW ?
        //                     rH : rW
        //                 :
        //                 1
        //             , doZomm = nZoom < 1 ? nZoom : 1

        //         ;
        //         // console.error(marsBottom, gridContentBottom, bottomDiff_rem);
        //         // console.warn(gridContentLeft, marsLeft, leftDiff_rem);

        //         // Apply the CSS with this measurements:
        //         gridContainer.setAttribute('style',
        //             `${gridContainerCurrentStyle ? gridContainerCurrentStyle : ''} transform: `
        //             + `scale(${doZomm.toFixed(2)}) `
        //             // They are now with same dimensions
        //             + `translateX(calc(-50% + ${(this.nXcells / 2 ) - this.position.coordinates.x }rem)) `
        //             // They are now at same bottom X pos: X/2 - initPos.x
        //             + `translateY(calc(-50% + ${(this.nXcells / 2) + this.position.coordinates.y}rem));`
        //             // They are now at same bottom Y pos: Y/2 + initPos.y // -43rem = -(${nCommUnit + 1} + 14)
        //         );

        //     }

        // }, aferTimeToDrawIt);
    }
}
