import { Component, Input, Output, EventEmitter, HostListener, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'ui-switch',
    template: `
        <div class="switch-container" [style.minWidth]="minWidth">
            <span class="switch"
                [class.checked]="checked"
                [class.disabled]="disable"
                [class.switch-large]="size === 'large'"
                [class.switch-medium]="size === 'medium'"
                [class.switch-small]="size === 'small'"
            >
                <mark></mark>
            </span>

            <span *ngIf="!isTextHTML" class="switch-text">
                <span *ngIf="checked && enabledText" class="ui-switch-boolean enabled-text">{{ enabledText }}</span>
                <span *ngIf="!checked && disabledText" class="ui-switch-boolean disabled-text">{{ disabledText }}</span>
            </span>
            <span *ngIf="isTextHTML" class="switch-text">
                <span *ngIf="checked && enabledText" class="ui-switch-boolean enabled-text" [innerHTML]="enabledText"></span>
                <span *ngIf="!checked && disabledText" class="ui-switch-boolean disabled-text" [innerHTML]="disabledText"></span>
            </span>
        </div>
    `,
    styleUrls: ['./switch.component.scss'],
    // Next obliges us to work with immutable objects (Observables), or manually trigger (by DOM event or Method) a changes detection.
    // (the component only depends on its @inputs() initializtion/re-initialization - NOT on ALL component's changes)
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SwitchComponent {

    @Input() public checked: boolean;
    @Input() public disable: boolean;
    @Input() public minWidth: string;
    @Input() public size: 'small' | 'medium' | 'large' = 'small';

    @Input() public enabledText: string;
    @Input() public disabledText: string;
    @Input() public isTextHTML = false;

    @Output() public changed = new EventEmitter<boolean>();

    constructor() { }

    @HostListener('click') onToggle() {
        if (this.disable) { return; }

        this.checked = !this.checked;
        this.changed.emit(this.checked);
    }

}
