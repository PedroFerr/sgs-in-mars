/**
 * Many thanks to 'user123444555621' @ StackOverFlow (99.723 of reputation!)
 * that develop this JSON.stringify() with syntax highlighting
 * https://stackoverflow.com/a/7220510/2816279
 *
 * Has been updated to better serve this specific App purpose
 *
 * @param json - the JSON data to print @ HTML <pre> tag:
 */
export function prettyPrintJson(json: any) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        let cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true/.test(match)) {
            cls = 'boolean-true';
        } else if (/false/.test(match)) {
            cls = 'boolean-false';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        const keyValue = match.replace(/"/g, '') ;
        return '<span class="' + cls + '" title="' + keyValue + '">' + keyValue + '</span>';
    });
}

/**
 * Compute style in JS, from a DOM HTMLElement
 * Check it here: https://stackoverflow.com/a/2531934/2816279
 *
 * The element.style property lets you know only the CSS properties that were defined as inline in that element
 * You should get the computed style - not so easy to do it in a cross-browser way.
 * IE => element.currentStyle property. Others use document.defaultView.getComputedStyle method.
 *
 * @param el - the HTMLElement
 * @param styleProp - the styling property we want to get
 */
export function getStyle(el, styleProp) {
    let value;
    const defaultView = (el.ownerDocument || document).defaultView;
    // W3C standard way:
    if (defaultView && defaultView.getComputedStyle) {
        // sanitize property name to css notation
        // (hypen separated words eg. font-Size)
        styleProp = styleProp.replace(/([A-Z])/g, '-$1').toLowerCase();
        return defaultView.getComputedStyle(el, null).getPropertyValue(styleProp);
    } else if (el.currentStyle) { // IE
        // sanitize property name to camelCase
        styleProp = styleProp.replace(/\-(\w)/g, function (str, letter) {
            return letter.toUpperCase();
        });
        value = el.currentStyle[styleProp];
        // convert other units to pixels on IE
        if (/^\d+(em|pt|%|ex)?$/i.test(value)) {
            return (function (recursiveValue) {
                const oldLeft = el.style.left, oldRsLeft = el.runtimeStyle.left;
                el.runtimeStyle.left = el.currentStyle.left;
                el.style.left = recursiveValue || 0;
                value = el.style.pixelLeft + 'px';
                el.style.left = oldLeft;
                el.runtimeStyle.left = oldRsLeft;
                return value;
            })(value);
        }
        return value;
    }
}

/*------------------------------------------------------------------------------------------------------------------------------------------*
 *                                                                                                                                          *
 *          == DELAYED EVENT TRIGGER ==                                                                                                     *
 *                                                                                                                                          *
 *  Sometimes, for "tiny-random" trigger situations (like $(window).onresize, for instances, the overload processing for each pixel/ms      *
 *  is too much and, in most cases absolutely desnecessary.                                                                                 *
 *  In order to reduce the number of times an event is triggered, we have a little timer... if nothing happens                              *
 *  we assume our "client" request is satisfied and, only then, we trigger the event! http://stackoverflow.com/a/4541963/2816279            *
 *                                                                                                                                          *
 *  The 'timer' variable, with an unique id is a tremendous improvement!                                                                    *
 *  As so, you can call it multiple times (even from different parts of your code), with a unique id for each callback                      *
 *  (just change the text on the listener function!), that those unique IDs are used to keep all the timeout events separate.               *
 *                                                                                                                                          *
 *  JUST Great! ;-)                                                                                                                         *
 *                                                                                                                                          *
 *  This delayed trigger function is simply called inside the event binding function, any number of times, in any part of the code:         *

    $obj.on('event', function () {
        waitForFinalEvent(function(){
            alert('Only now the event has been called!');
            //...
        }, 500, "event trigger #1!");
    });
 *                                                                                                                                          *
 *------------------------------------------------------------------------------------------------------------------------------------------*/
export const waitForFinalEvent = (function() {
        const
            timers = {}

        ;
        return function(callback, ms, uniqueId) {
            if (!uniqueId) {
                uniqueId = "Don't call this twice without a uniqueId";
            }
            if (timers[uniqueId]) {
                clearTimeout (timers[uniqueId]);
            }
            // console.error(timers);
            timers[uniqueId] = setTimeout(callback, ms);
        };
    })()
;
