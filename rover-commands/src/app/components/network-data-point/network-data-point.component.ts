import { Component, Optional, Input, OnChanges, SimpleChanges } from '@angular/core';

import { AppComponent } from 'src/app/app.component';

import { Instructions, VehicleInstructions, MonitorData, JourneyRoadMap, MoveReport } from 'src/app/app.interfaces';

@Component({
    selector: 'network-data-point',
    /**
     * template NEEDS to  be SET, even if empty!
     * An ERROR will be thrown: Uncaught Error: No template specified for component NetworkVideoPointComponent
     *
     * This is happening because we're using a SERVICE at the Component's Class constructor()
     */
    template: ``
})
export class NetworkDataPointComponent implements OnChanges {

    // Icons to communication IN and OUT, either to 'command-center' or 'space-expedition':
    commInIcon = '<span class="comm-in">&rArr;</span>';
    commOUTIcon = '<span class="comm-out">&lArr;</span>';

    // Define, here, by setting this boolean, if you want msg+data to console @ browser's Inspector:
    isToConsole = false;

    @Input() submitedInstructions: Instructions;
    @Input() receivedVehicleInstructions: VehicleInstructions;
    // -----------------------------------
    @Input() startMovingData: MonitorData;
    @Input() eachMoveData: MonitorData;
    @Input() lastMoveData: MonitorData;

    constructor(
        /**
         * Have access to Host App component (parent of all Components) Methods AS A SERVICE!
         * BUT BE CAREFULL!
         *
         * If "AppComponent" can not (still...) be found by Angular, an error will be thrown, on mounting it!
         * And it can happen NO ERROR is thrown at all - simply THIS Component won't work at all, which is even more scary...
         *
         * "Yes you can use (and MUST, here)
         *      constructor (@Host() @SkipSelf() @Optional() toys: ToysService) {...}"
         * " - check it here: https://medium.com/frontend-coach/self-or-optional-host-the-visual-guide-to-angular-di-decorators-73fbbb5c8658
         *
         * We're NOT "calling" a component!
         * We are INJECTING a SERVICE, that will bring all 'app' properties, at this Component's Class constructor!
         *
         * This also makes MANDATORY to have ANY 'template' (even if empty!) declared @Component!
         */
        // @Host() private appComponent: AppComponent <= throws an ERROR:
        //      Template parse errors: No provider for AppComponent (...): ng:///AppModule/CommandCenterComponent.html@302:4 (first <network-data-point /> component call)
        @Optional() private appComponent: AppComponent,
    ) { }

    ngOnChanges(propChanging: SimpleChanges) {

        if (propChanging && propChanging.submitedInstructions && !propChanging.submitedInstructions.firstChange) {
            const
                echoStr = 'Instructions to Rover submited @ \'command-center\' to the nearest network-data access point.'
                , monitorDataToEmit: MonitorData = { msg: this.commOUTIcon + echoStr, data: null } // this.submitedInstructions }

            ;
            this.consoleLogIt('warn', echoStr); // , this.submitedInstructions);
            this.appComponent.receiveDataComm(monitorDataToEmit);
        }

        if (propChanging && propChanging.receivedVehicleInstructions && !propChanging.receivedVehicleInstructions.firstChange) {
            const
                echoStr = '"Greetings from \'space-expedition\'! Instructions just arrived!"'
                // We would be repeating, from above, the entire 'this.submitedInstructions' JSON Data, at bottom monitor - pass null; just echo de msg:
                // Well... pass null at previous and, yes, print this one! The other one is a network data access from 'command-center', not so important...
                , monitorDataToEmit: MonitorData = { msg: this.commInIcon + echoStr, data: this.receivedVehicleInstructions }

            ;
            this.consoleLogIt('warn', echoStr, monitorDataToEmit);
            this.appComponent.receiveDataComm(monitorDataToEmit);
        }

        // ========================================================================================================

        if (propChanging && propChanging.startMovingData && !propChanging.startMovingData.firstChange) {
            // Don't print "startMovingData.data.journeysMoves" - RoadMap JSON's can be quite extensive, if user continues to "isToCumulateJourneys = true", on each journey:
            // "lastPosIndex" and "lastPos" are OK, just for we to check from where this journey will start, and the number of moves of previous journeys...
            const
                jsonData = this.startMovingData.data as JourneyRoadMap
                , monitorDataToEmit: MonitorData = {
                    msg: this.commInIcon + this.startMovingData.msg
                    , data: { lastPos: jsonData.lastPos, lastPosIndex: jsonData.lastPosIndex } as JourneyRoadMap
                }; // this.startMovingData.data };
            this.consoleLogIt('warn', this.startMovingData.msg, this.startMovingData.data as MoveReport);
            this.appComponent.receiveDataComm(monitorDataToEmit);
        }

        // ========================================================================================================

        if (propChanging && propChanging.eachMoveData && !propChanging.eachMoveData.firstChange) {
            // Don't print "eachMoveData.data.journeyRoadMap" - RoadMap JSON's can be quite extensive, if user continues to "isToCumulateJourneys = true", on each journey:
            const monitorDataToEmit: MonitorData = { msg: this.eachMoveData.msg, data: this.removeRoadMap(this.eachMoveData.data as MoveReport)};
            this.consoleLogIt('log', monitorDataToEmit.msg, this.eachMoveData.data as MoveReport);
            this.appComponent.receiveDataComm(monitorDataToEmit);
        }

        if (propChanging && propChanging.lastMoveData && !propChanging.lastMoveData.firstChange) {
            const
                echoStr = '"Journey\'s FINAL move accomplished!" - says Rover.'
                // We will repeat the 'eachMoveData' and 'lastMoveData' JSON Data, at bottom monitor.
                // But never mind; they represent different App stages/milestones!
                // MIND YOU, yes, that the msg printed on top of the JSON it's different.
                , monitorDataToEmit: MonitorData = { msg: this.commInIcon + echoStr, data: null } // this.lastMoveData.data }
            ;
            this.consoleLogIt('warn', echoStr, this.lastMoveData.data as MoveReport);
            this.appComponent.receiveDataComm(monitorDataToEmit);

            // LAST "Over & Out" PLAIN MESSAGE, no data, from the exploring vehicle!
            setTimeout(() => {
                const
                    lastMoveEchoStr = '"Done! Over & out! This Mecedes Benz engine rocks!" - finishes Rover'
                    , lastMoveMonitorDataToEmit: MonitorData = { msg: this.commInIcon + lastMoveEchoStr, data: null }
                ;
                this.consoleLogIt('error', lastMoveEchoStr, this.lastMoveData.data as MoveReport);
                this.appComponent.receiveDataComm(lastMoveMonitorDataToEmit);
            } , 1000);
        }

    }

    private consoleLogIt(consoleType: string, msg: string, data: Position | Instructions | VehicleInstructions | JourneyRoadMap | MoveReport | MonitorData = null) {
        if (this.isToConsole) {
            switch (consoleType) {
                case 'warn': console.warn(msg, data);
                    break;
                case 'error': console.error(msg, data);
                    break;
                default: console.log(msg, data);
                    break;
            }
        }
    }

    private removeRoadMap(moveReport: MoveReport): MoveReport {
        const moveDataWithoutRoadMap = Object.assign({}, moveReport) as MoveReport;
        delete moveDataWithoutRoadMap.journeyRoadMap;
        return moveDataWithoutRoadMap;
    }
}
