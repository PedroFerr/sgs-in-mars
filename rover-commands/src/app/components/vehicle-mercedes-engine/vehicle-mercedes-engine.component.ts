import { Component } from '@angular/core';

import { GPS, Position, RoadMap, UnitPathMove, OrientationChar } from 'src/app/app.interfaces';

@Component({
    selector: 'vehicle-mercedes-engine',
    template: ``
})
export class VehicleMercedesEngineComponent {

    constructor() { }

    // tslint:disable-next-line:variable-name
    returnPosObj (new_x: number, new_y: number, thisPosition: Position, nextOrientation: string): Position {
        return {
            coordinates: {
                x: new_x ?
                    Number(thisPosition.coordinates.x) + new_x
                    :
                    Number(thisPosition.coordinates.x)
                , y: new_y ?
                    Number(thisPosition.coordinates.y) + new_y
                    :
                    Number(thisPosition.coordinates.y)
            }
            , orientation: nextOrientation as OrientationChar
        };
    }

    /**
     * Translate each of the thisNextPosition.orientation points into vehicles axis position to achieve;
     *
     * @param thisPosition "from" position
     * @param thisNextPosition "to" position
     * @param movesList As user can cumulate, or not, journeys, we should also cumulate, or not, the engine's movements - "this.journeysMoves" global var is `movesList` Input
     */
    commandToMovement_translator(thisPosition: Position, thisNextPosition: Position, movesList: Array<UnitPathMove>): RoadMap {
        /**
        * Independent of the given (A, L, R) command, next will ALWAYS persists
        * regarding "thisNextPosition.position" relative to "thisNextPosition.orientation":
        *
        * N = y + 1
        * E = x + 1
        * S = y - 1
        * W = x - 1
        */
        const
            thisMove = {
                moveN: thisNextPosition.orientation === 'N' ? this.returnPosObj(0, +1, thisPosition, 'N') : null,
                moveE: thisNextPosition.orientation === 'E' ? this.returnPosObj(+1, 0, thisPosition, 'E') : null,
                moveS: thisNextPosition.orientation === 'S' ? this.returnPosObj(0, -1, thisPosition, 'S') : null,
                moveW: thisNextPosition.orientation === 'W' ? this.returnPosObj(-1, 0, thisPosition, 'W') : null,
            }
        ;
        // Calculated "returnPosObj()" @ thisMove[], will be already the position of the (to DONE, @ Cockpit) movement:
        Object.assign( thisNextPosition, thisPosition, thisMove['move' + thisNextPosition.orientation] );

        movesList.push(
            { from: thisPosition, to: thisMove['move' + thisNextPosition.orientation] }
        );

        // Emit back, saving the two journey step movement tails, in case someone wants to remember them @ THIS journey's move (the next will cumulate)!
        const movesInfo: RoadMap = {
            movesList: movesList
        };
        return movesInfo;

    }

    /**
     * Fetch which nextCardinal point will be achieved if vehicle turns 'L' or 'R'
     * (so far those are the only commands that change direction - 'A' commnds movement in the same axis)
     *
     * When achieved, physically rotate our rover icon ('div#rover'), so it gets positioned on 'div.mars-grid':
     *
     * @param: commandUnit
     */
    rotateTo(commandUnit: string, currentOrientation: OrientationChar): OrientationChar {
        const compass: Array<OrientationChar> = ['N', 'E', 'S', 'W'];
        let turnSign = 0;    // commandUnit = 'A' => no turns!

        // Sign to where is the turn (or keep driving in front - turnSign = 0):
        if (commandUnit === 'L') { turnSign = -1; }
        if (commandUnit === 'R') { turnSign = +1; }

        let nOrientation: number, nNewOrientation: number;
        for (let x = 0; x < compass.length; x++) {
            if (compass[x] === currentOrientation) { nOrientation = x; }
        }
        nNewOrientation = (nOrientation + turnSign);
        // Treat extremes:
        if (nNewOrientation > compass.length - 1) { nNewOrientation = 0; }
        if (nNewOrientation < 0) { nNewOrientation = compass.length - 1; }

        return compass[nNewOrientation];
    }

    /**
     * If this one is to call ()
     * we can do a one movement test on Mercedes Benz engine,
     * with no need to use the App's form Inputs of initial driving conditions
     *
     * @param roverStatus  - inputted one, simulating the user typing/options on App's form
     * @param isToTry - activate, or not, this test from caller
     */
    tryEngineMove(
        roverStatus: {
            thisOrientation: string
            , thisPosition: GPS
            , commandChar: string
        }, isToTry: boolean
    ) {
        if (!isToTry) { return; }

        let nextOrientation: string;
        const thisNextPosition = {} as GPS;
        // Init conditions:
        console.warn(' ================ MERCEDES BENZ ENGINE on TEST MODE! ================ ');
        console.warn(`Heading «${roverStatus.thisOrientation}», with a FIRST command unit of «${roverStatus.commandChar}»...`);

        // Try new car orientation:
        nextOrientation = this.rotateTo(roverStatus.commandChar, roverStatus.thisOrientation as OrientationChar);
        // Gether with new position try, in a traceable output:
        const
            consoleOutput = this.commandToMovement_translator(
                { coordinates: roverStatus.thisPosition, orientation: roverStatus.thisOrientation as OrientationChar }
                , { coordinates: thisNextPosition, orientation: nextOrientation as OrientationChar}
                , []
            )

        ;
        console.log(`... will face Rover from ${roverStatus.thisOrientation} to ${nextOrientation}`);
        console.log(', moving it from', consoleOutput.movesList[0].from, ' to ', consoleOutput.movesList[0].to);
        console.warn(' ================ End of MERCEDES BENZ test ================ ');

        return consoleOutput;
    }

}
