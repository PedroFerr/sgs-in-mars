import { Component, OnInit, AfterViewInit } from '@angular/core';

@Component({
    selector: 'monitor-joystick',
    templateUrl: './monitor-joystick.component.html',
    styleUrls: ['./monitor-joystick.component.css']
})
export class MonitorJoystickComponent implements OnInit, AfterViewInit {

    constructor() { }

    ngOnInit() {

    }


    ngAfterViewInit() {

        // https://stackoverflow.com/q/42112571/2816279

        // https://github.com/EnricoPicci/ng-joystick
    }
}
