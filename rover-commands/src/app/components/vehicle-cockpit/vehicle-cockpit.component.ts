import { Component, OnChanges, Input, Output, EventEmitter, AfterViewInit, ElementRef, Renderer2 } from '@angular/core';

import {
    CockpitDriveInstructions, Position, JourneyRoadMap, UnitPathMove
    , MoveReport, MonitorData, VehicleMove, VehicleMoveInitializer
    , SpeakerVoice, SpeakerVoiceInitializer, MonitorAstronaut, MonitorAstronautzeroPos, VideoStreamFrame
    , OrientationChar,
    VehicleInstructions
} from 'src/app/app.interfaces';

// Plain JS function utilities to plug them in:
import * as JsPlugIns from './../../_js-utils';

@Component({
    selector: 'vehicle-cockpit',
    templateUrl: './vehicle-cockpit.component.html',
    styleUrls: ['./../../child-containers/deployed-expedition/deployed-expedition.css']
})
export class VehicleCockpitComponent implements OnChanges, AfterViewInit {

    // Grab this component's HTML - it wlll be manipulated a lot, including also by other Components - and its main HTML tags actors:
    tpl: HTMLElement;
    exploringVehicleIcon: HTMLElement;

    // Cockpit's THIS JOURNEY's operations instructions will arrive:
    @Input() receiveJourneyDriveInstructions: CockpitDriveInstructions;
    //  allowing it to:
    // --------------------------------------------------
    // * move Icon's step by step, sending back the MonitorData report, along with this journey step counter:
    initEachMove: VehicleMove = Object.assign( {}, VehicleMoveInitializer);
    eachJorneyMoveMonitorData: MonitorData;
    @Output() private eachMoveReport = new EventEmitter<{ eachMove: MonitorData, thisJourneyNmove: number }>();
    // * broadcast back a video stream frame at each step:
    initStreamingVideo: MonitorAstronaut = Object.assign({}, MonitorAstronautzeroPos);
    @Output() private eachMoveStream = new EventEmitter<MonitorAstronaut>();
    // * "talk" BS 4 Tooltiping voice expressions, also at each journey's step:
    initVoiceSpeak: SpeakerVoice = Object.assign( {}, SpeakerVoiceInitializer);
    @Output() eachMoveSpeak = new EventEmitter<SpeakerVoice>();
    // It's not private as 'vehicle-fleet-operations' has some "talk" to pick up from here

    constructor(
        private thisDOM: ElementRef,
        private renderer: Renderer2
    ) { }
    // tslint:disable:variable-name

    ngOnChanges() {
        if (this.receiveJourneyDriveInstructions && this.receiveJourneyDriveInstructions.currentJourneyRoadMap) {
            /**
             * MIND YOU
             * ==============================================
             * this component's changes should ONLY happen ONCE per JOURNEY!
             * ==============================================
             * Never more should code come here agian, except when user submits another journey @ 'command-center'!
             *
             * Cockpit will:
             * 1) Drive Rover's icon through the complete "journeyRoadMap" step by step
             * 2) Emit back (to caller 'exploring-vehicle's) "onEachMoveReport()" method) the "eachMoveReport" Data, on EACH step.
             *    From there, a few things will happen (including to keep emitting back some stuff), besides priniting Data, through the installed 'network-data-point' access.
             * 3) Stream video images through 'network-video-point' access, of the ENTIRE Mars (should switch the camera!) "journeyRoadMap" steps,
             *    on EACH step's "exploringMovementStream" Data.
             */
            const
                currentJourneyRoadMap: JourneyRoadMap = this.receiveJourneyDriveInstructions.currentJourneyRoadMap
                , movesList: Array<UnitPathMove> = currentJourneyRoadMap.journeysMoves
                , lastJourneyLastPosIndex: number = currentJourneyRoadMap.lastPosIndex

                // Next are fixed, Instructions values, submited freely by the user, that can CHANGE (only) from submited journey to journey:
                , userSetUp = this.receiveJourneyDriveInstructions.submitedInstructions
                , numberOfJourneyMoves: number = userSetUp.charCommand.length
                , roverMoveTime = userSetUp.roverLimits.lagTime

                // NOTE that next is NOT the INSTRUCTIONS "lagTime"!
                // It's this next "for" CYCLE setTimeout() SPEED - which is 'lagTime' BUT affected by a small factor @ 'exploring-vehicle's "ngOnChanges()"!
                , journeyStepSpeed: number = this.receiveJourneyDriveInstructions.engineSpeedPerMove

            ;
            // To start with, pick last known Position index - 'exploring-vehicle's "currentJourneyRoadMap" @Input() (comming from 'space-expedition') alreay accounts for it
            // ... to start fresh from deployment position ("JourneyRoadMapInitializer.lastPosIndex", which is -1), or keep moving from last journey's reached position index:
            let roadMap_nMove = lastJourneyLastPosIndex;

            for (let nCommandChar = 0; nCommandChar < numberOfJourneyMoves; nCommandChar++) {
                const
                    uniqueTimerId = `Journey move #${nCommandChar + 1}`
                    , thisJourneyStepTimeMoment = journeyStepSpeed * (nCommandChar + 1)

                ;
                // Have control of EACH move/moment in time, identifying each by a unique string Id, with waitForFinalEvent():
                JsPlugIns.waitForFinalEvent ( () => {
                    // ==============================
                    // UPDATE, by incrementing on each read nCommandChar, our JourneyRoadMap's "indexMoves" counter!
                    // On "last_nJourneyMove" LAST journey step, this counter SHOULD BE "JourneyRoadMap.journeysMoves.length - 1"
                    roadMap_nMove ++;
                    // ==============================

                    // 1)
                    // Put Rover icon to really move, by driving THIS nCommandChar step!
                    this.eachJorneyMoveMonitorData = this.driveEachMove(
                        // -----------------------------
                        roadMap_nMove  // each "currentJourneyRoadMap.lastPosIndex" will be RETURNED inside this.eachJorneyMoveMonitorData's "journeyRoadMap"s MoveReport prop
                        // -----------------------------
                        , movesList
                        , nCommandChar
                        , numberOfJourneyMoves - 1
                        , roverMoveTime
                    ) as { msg: string, data: MoveReport};
                    // console.error('dataToReport FROM Cockpit\'s driveEachMove()', this.eachJorneyMoveMonitorData);

                    // 2)
                    // Send back THIS step Data for 'exploring-vehicle' parent affairs with its own parents, along with the 2 important different COUNTERS:
                    // this "thisJourneyNmove" step counter
                    // this "roadMap_nMove" index moves counter - if user cumulates journeys, "this.indexMoves" will keep cumulating
                    const thisStepData = { eachMove: this.eachJorneyMoveMonitorData, thisJourneyNmove: nCommandChar };
                    this.eachMoveReport.emit(thisStepData);

                    // 3)
                    // In parallel we can also emit THIS "step on Mars" stream, right away - what we would call a "live transmission" ;-)
                    // "this.eachJorneyMoveMonitorData.data" has already "lastPosIndex" and "lastPos" props, of "journyRoadMap", UPDATED for THIS "nCommandChar" step/move
                    this.streamEachMove(nCommandChar, this.eachJorneyMoveMonitorData.data as MoveReport);

                }, thisJourneyStepTimeMoment, uniqueTimerId);
            }
        }
    }

    ngAfterViewInit() {
        this.tpl = this.thisDOM.nativeElement;
        /**
         * These next ones can NOT be set HERE! DOM is always changing...
         *
         * Well... they SHOULD!
         * Distant parent 'vehicle-fleet-operations' HTML container will USE THEM (through @ViewChild() Injector) at
         * "calculateLandingMoments()" and "landExploringVehicleOnPlanet()", for landing animation and calculous.
         * because it's there the Shuttle Conveyour HTML '.rover-in-orbit', that will deploy our Rover on the planet to explore.
         *
         * Once needed HERE, the ALERT is to be carefull WHEN to DOM query them - HTML tag's styling are ALWAYS changing!
        */
        this.exploringVehicleIcon = this.tpl.querySelector('#rover');    // Re-queried @ doMoveRover()
    }

    /**
     * Once you have the engine with ALL moves computed @ `roadMap`s Array<UnitPathMove> movesList,
     * REALY move our Rover (FontAwesome icon) through THIS JOURNEY on Mars.
     *
     * This method will be called on a each 'journeyStepSpeed'ms loop, once ALL journey moves are computed at Rover's Merceds engine.
     * As so, we simulate a journey, with a real driving path movement, and with a real (synchronized) time to do each step.
     *
     * MIND YOU
     * RoverRoadMap has ALL the (cumulated) moves, of SEVERAL (at minimum, this one!) JOURNEYS!
     * So, be carefull with "roadMap_nMove" (global indexMoves) and journeys "nCommandChar" move (journey's each "for" cycle).
     *
     * @param roadMap_nMove the RoadMap index - it's "0" or ALL (-1) PREVIOUS journey number of command chars! MIND YOU'll NEVER know HOW MANY were (only) from LAST journey...
     * @param roadMapListing array of unit path moves {from: Position, to: Position} returned by our Rover's Mercedes Benz @ 'exploring-vehicle'
     * @param nJourneyMove the nth command char of the caller "for" cycle index
     * @param last_nJourneyMove the LAST nth command char of the caller "for" cycle index
     * @param delayCSSTransition it's the user submited lagTime, that is directly numbering the CSS transition speed - It's NOT 'exploring-vehicle's "engineSpeedPerMove"!!!
     */
    driveEachMove(
        roadMap_nMove: number
        , roadMapListing: Array<UnitPathMove>
        , nJourneyMove: number
        , last_nJourneyMove: number
        , delayCSSTransition: number
    ): MonitorData {
        let echoStr = `ERROR! Something is WRONG @ rover-cockpit's #${roadMap_nMove} Road Map move! Please check...`;
        const
            roadMapMove: UnitPathMove = roadMapListing[roadMap_nMove]
            , orientationUpperCase = this.upperCase(roadMapMove.to.orientation as OrientationChar)

        ;
        let isLast_nJourneyMove = false;

        switch (nJourneyMove) {
            case (0):
                echoStr = `My first move #1 ${orientationUpperCase}, @ GPS {${roadMapMove.to.coordinates.x}, ${roadMapMove.to.coordinates.y}}`;
                // What about if there was only 1 single letter unit commmand...?
                if (nJourneyMove === last_nJourneyMove) {
                    isLast_nJourneyMove = true;
                    echoStr += `... and also my last one! Over & out.`;
                }
                break;

            case last_nJourneyMove:
                isLast_nJourneyMove = true;
                echoStr = `Done! On move #${nJourneyMove + 1}, journey has reached `
                    + `${orientationUpperCase} @ GPS {${roadMapMove.to.coordinates.x}, ${roadMapMove.to.coordinates.y}}`;
                break;

            default:
                echoStr = `...move #${nJourneyMove + 1} to `
                    + `${orientationUpperCase} @ GPS {${roadMapMove.to.coordinates.x}, ${roadMapMove.to.coordinates.y}}`;
                break;
        }

        // Now REALLY put it to MOVE!
        const roverMoveParams: VehicleMove = Object.assign( this.initEachMove, {
            pointToGoTo: roadMapMove.to
            , msg: echoStr
            , isLastMove: isLast_nJourneyMove
            , delayCSSTransition: delayCSSTransition
        });
        this.doMoveRover( roverMoveParams );

        // At EACH journey's STEP, after we REALLY doMoveRover() on Mars, we release a Report of what's achieved by our little powerfull Rover.
        // "lastPosIndex" and "lastPos" MoveReport props are returned UPDATED!
        // On final one, "roadMap_nMove" SHOULD BE the indexMoves to send to 'space-expedition' - on 'exploring-vehicle's "finishExplorationJourney.emit(stepReport)"
        const
            reportMove: MoveReport = {
                journeyRoadMap: {
                    journeysMoves: roadMapListing
                    , lastPosIndex: roadMap_nMove
                    , lastPos: roadMapListing[roadMap_nMove].to
                }
                , isRestrictedAreaCrossed: null    // We'll only Know if "isRestrictedAreaCrossed" @ 'exploring-vehicle' (not at the cockpit vehicle!)
                , isFinalMove: isLast_nJourneyMove
            }
            , dataToReport: MonitorData = { msg: echoStr, data: reportMove }

        ;
        return  dataToReport;
    }

    /**
     * This method is either called from:
     * ---------------------------------------------------
     * Parent 'vehicle-fleet-operations'
     * => needs the Rover to move to Init Position, not deployed, on Automatic Pilot - on EACH new user Input Instructions submission!
     * or
     * ---------------------------------------------------
     * from upper "driveEachMove()" call
     * => an ordinary "move" commanded by the Road Map, previously calculated at Mercedes Engine component.
     *
     * In both cases, all you have to do is CSS put the Rover on the 'pointToGoTo'
     * Simple. ;-)
     * At the same time, in a synchronized way, keep saying what point are you reaching to, for Mars atmosphere (BS4 Tooltip content).
     *
     * Nice to read (I did!):
     *     - applying sequential transform's: https://stackoverflow.com/a/10765771/2816279
     *     - understanding the Axis and rotate CSS command : https://css-tricks.com/get-value-of-css-rotation-through-javascript/
     *
     * IMPORTANT INPUT NOTE, for "delayCSSTransition" property:
     * ---------------------------------------------------
     * you shouldn't be always updating vehicle's CSS transition's speed at EACH step!
     * Instead, do it 1 line before calling here the set of steps (even if it's a unique one) Rover will do in a row (journey).
     * Vehicle's CSS transition's speed should ONLY be set (changed/updated) ONCE necessary and NEVER always!
     *
     * As so, note that the input 'delayCSSTransition' passed here, is EXCLUSIVELY for 'speaker-comms's "cockpitTalk()" method, through THIS "eachMoveSpeak" emitter,
     * BS4 Tooltip time to appear - NOT for vehicle's transitions on 'transform' CSS property!
     *
     * @param withParams this movement Data, even with a msg to be exhibited on Rover's BS4 Tooltip, so Rover can really CSS execute it
     */
    doMoveRover(withParams: VehicleMove) {
        this.exploringVehicleIcon = this.tpl.querySelector('#rover');

        this.renderer.setStyle(this.exploringVehicleIcon, 'transform'
            , `translate( calc(-50% + ${withParams.pointToGoTo.coordinates.x}rem), calc(-50% - ${withParams.pointToGoTo.coordinates.y}rem) )`
            // CSS for ROTATION - "compensate", first, the FontAwesome icon natural facing to East:
            + ` rotate(${this.compensateIconRotation(withParams.pointToGoTo.orientation)}deg)`
        );

        /**
         * IMPORTANT INPUT NOTE, for "toShow" property:
         * --------------------
         * AFTER each "doMoveRover()" call, 'toShow' is ALWAYS true - ALWAYS "talk" each vehicle step.
         * On the other hand, if some code line is direct calling 'speaker-comms's "cockpitTalk()" method, through "eachMoveSpeak" emitter,
         * you can, then, CHOOSE, besides tooltip "transformAtPos" Position, if the BS4 Tooltip is TO SHOW (and time to show) or not.
         */
        const toNetworkVoiceAccessPoint: SpeakerVoice =  Object.assign( {}, this.initVoiceSpeak, {
            msg: withParams.msg
            , transformAtPos: withParams.pointToGoTo
            , delayCSSTransition: withParams.delayCSSTransition
            , waitForCSSTransition: withParams.waitForCSSTransition
        });
        this.eachMoveSpeak.emit(toNetworkVoiceAccessPoint);

        // Delete Tolltip, after 5000 s, if it's last move:
        if (withParams.isLastMove) {
            setTimeout( () => this.eachMoveSpeak.emit( Object.assign( {}, this.initVoiceSpeak, {msg: ''} ) ), 5000 );
        }
    }

    /**
     * Broadcast to the closest 'network-video-point' access, each 'nMove' step On Mars,
     * trough the parent's "onEachMoveStream()".
     *
     * @param nMove - the # move to "live" stream
     * @param reportMove MoveReport to extract data from, to the broadcast content
     */
    private streamEachMove(nCommandChar: number, reportMove: MoveReport) {
        const
            userSetUp: VehicleInstructions = this.receiveJourneyDriveInstructions.submitedInstructions
            , roverPosition: Position = userSetUp.roverPosition
            , roverLagTime: number = userSetUp.roverLimits.lagTime
            , numberOfJourneyMoves: number = userSetUp.charCommand.length

            // NOTE that next is NOT the Instructions "lagTime" - is affected by a small factor @ 'exploring-vehicle's "ngOnChanges()"!
            , stepSpeed: number = this.receiveJourneyDriveInstructions.engineSpeedPerMove
            , thisStepTimeMoment: number = stepSpeed * (nCommandChar + 1)
            // And finally, most important, that made us refactor A LOT, in last 2 (currently on 4!!!) days ;-))):
            , lastReachedPos = reportMove.journeyRoadMap.lastPos

        ;
        // =================================================================
        // Video stream of EXPLORING FIRST STEP, right after "init position" reached
        // =================================================================
        if (nCommandChar === 0) {
            // ALL camera Id set up will be define @ <network-video-point /> by this "frameId" number
            const streamFrameFirstStep: VideoStreamFrame = {
                astronautPos: {
                    // On Mars, since this is the Init Position, Mars iquals, at this moment, LAST Earth steady position compass:
                    onMarsSoil: roverPosition
                    // , byEarthCompass: this.instructions.roverPosition
                    , exploringTime: 0
                    // It's first time we are initiating "exploringTime". Till now shoud be "-1", by "MonitorAstronautzeroPos" const value
                }
                // Allthough Astronaut should start walking, don't take away the 'isDeployed' deployment point color underneath:
                , astronautStatus: { firstChange: true, /* isDeployed: false,*/ isExploringMars: true }
                , videoScenario: { /*videoStreamSetUp: exploringFirstStep,*/ cssTimer: roverLagTime }
            };
            this.eachMoveStream.emit( Object.assign( {}, this.initStreamingVideo,
                    { frameId: 0 }
                    , { frameToStream: Object.assign( {}, this.initStreamingVideo.frameToStream, streamFrameFirstStep) }
                )
            );
        }
        // =================================================================
        // Video stream of EXPLORING LAST STEP - Astronaut is about to abandon Mars soil - AFTER this FINAL 'engineSpeedPerMove' (return to 'isDeployed')
        // =================================================================
        if (nCommandChar === numberOfJourneyMoves - 1) {
            setTimeout( () => {
                // ALL camera Id set up will be define @ <network-video-point /> by this "frameId" number
                const streamFrameLastStep: VideoStreamFrame = {
                    astronautPos: {
                        onMarsSoil: lastReachedPos
                        // , byEarthCompass: this.instructions.roverPosition
                        , exploringTime: thisStepTimeMoment + stepSpeed
                    }
                    , astronautStatus: { isExploringMars: false, isDeployed: true }
                    // , videoScenario: { videoStreamSetUp: exploringLastStep }
                };
                this.eachMoveStream.emit( Object.assign( {}, this.initStreamingVideo,
                        { frameId: 2 }
                        , { frameToStream: streamFrameLastStep }
                    )
                );

            }, stepSpeed);
        }
        // =================================================================
        // Video stream of EACH EXPLORING STEP - Astronaut is about to abandon Mars soil - AFTER this FINAL 'engineSpeedPerMove' (return to 'isDeployed')
        // =================================================================
        if (nCommandChar !== 0 && nCommandChar !== numberOfJourneyMoves - 1) {
            // ALL camera Id set up will be define @ <network-video-point /> by this "frameId" number
            const streamFrameEachStep: VideoStreamFrame = {
                astronautPos: {
                    onMarsSoil: lastReachedPos
                    // , byEarthCompass: this.instructions.roverPosition
                    , exploringTime: thisStepTimeMoment
                }
                , astronautStatus: { isExploringMars: true }
                // , videoScenario: { videoStreamSetUp: exploringEachStep }
            };
            this.eachMoveStream.emit( Object.assign( {}, this.initStreamingVideo,
                    { frameId: 1 }
                    , { frameToStream: streamFrameEachStep }
                )
            );
        }
    }


    // ====================
    // AUX funtions:
    // ====================

    /**
     * Cardinal points are not, physically, relative.
     * North is always north, wherever you are.
     * But here, our Rover is a FontAwesome icon that was designed facing East.
     *
     * So, as soon as Rover lands on Mars, you have to rotate him to the correct facing,
     * depending on the wanted starting point 'orientation'.
     * When reaching each commanded Position, you also have to come here to
     * compensate this "initial" miss leading rotation.
     *
     * @param orientation - the commanded orientation cardinal Rover should face to
     * @returns - the compensated rotation we should use on our code, as a string, due to it's natural icon design.
     */
    private compensateIconRotation(orientation: string): string {
        let nDegrees = null;
        switch (orientation) {
            case 'N':
                nDegrees = '-90';
                break;
            case 'S':
                nDegrees = '90';
                break;
            case 'W':
                nDegrees = '180';
                break;
            // 'E' is the default, as Rover is already designed facing East; nothing to rotate
            default:
                nDegrees = '0';
                break;
        }
        return nDegrees;
    }

    /**
     * Transform each 'orientationChar' into a cardinal point string, by extense and uppercase.
     * It's not Private because a lot of components will be fetching it here.
     *
     * @param orientationChar single char that represents a cardinal point
     */
    upperCase(orientationChar: OrientationChar ): string {
        const replacement = orientationChar.replace('N', 'NORTH').replace('S', 'SOUTH').replace('E', 'EAST').replace('W', 'WEST');
        return replacement;
    }

}
