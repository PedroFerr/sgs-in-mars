import { Component, Optional, OnChanges, SimpleChanges, Input } from '@angular/core';

import { AppComponent } from 'src/app/app.component';

import {
    InitOfTimesTimmings
    , Instructions
    , LandingOperationsTimmings
    , StreamConditions, VideoStreamFrame, AstronautScenario, MonitorAstronaut, MonitorAstronautzeroPos
} from 'src/app/app.interfaces';

@Component({
    selector: 'network-video-point',
    /**
     * template NEEDS to  be SET, even if empty!
     * An ERROR will be thrown: Uncaught Error: No template specified for component NetworkVideoPointComponent
     *
     * This is happening because we're using a SERVICE at the Component's Class constructor()
     */
    template: ``
})
export class NetworkVideoPointComponent implements OnChanges {

    // Init the MonitorAstronaut Object, with a const interface, so we don't have to keep enumerating all properties:
    // This will be our only Object to fill "this.appComponent.receiveStreamingComm()" video stream emitter to 'monitor-comms'
    streamingAstronautJourney = Object.assign({}, MonitorAstronautzeroPos) as MonitorAstronaut;

    // Start broadcasting right from the App's init of times:
    @Input() initOfTimesVideoStreamMoments: InitOfTimesTimmings;
    // Broadcast landing hyper-cool Operations:
    @Input() landingVideoStreamMoments: LandingOperationsTimmings;
    // Broadcast each step of planet exploration:
    @Input() eachMoveVideoStream: MonitorAstronaut;

    // Every time Rover's astronaut is landing or exploring Mars - touching it's soil, walking, or lifted up, inside the Rover,
    // App should update Sony's streaming video of the expedition on planet:
    // @Output() private sendSpaceExpeditionVideoStream = new EventEmitter<MonitorAstronaut>();
    // NOTE:
    // Instead of spreading (sendSpaceExpeditionVideoStream) emitters throughout ALL HTML templates that have a 'network-video-point' access on it,
    // we access directly 'app.component's Method ("receiveStreamingComm()") to video stream @ 'monitor-comms' component.

    constructor(
        /**
         * Have access to Host App component (parent of all Components) Methods AS A SERVICE!
         * BUT BE CAREFULL!
         *
         * If "AppComponent" can not (still...) be found by Angular, an error will be thrown, on mounting it!
         * And it can happen NO ERROR is thrown at all - simply THIS Component won't work at all, which is even more scary...
         *
         * "Yes you can use (and MUST, here)
         *      constructor (@Host() @SkipSelf() @Optional() toys: ToysService) {...}"
         * " - check it here: https://medium.com/frontend-coach/self-or-optional-host-the-visual-guide-to-angular-di-decorators-73fbbb5c8658
         *
         * We're NOT "calling" a component!
         * We are INJECTING a SERVICE, that will bring all 'app' properties, at this Component's Class constructor!
         *
         * This also makes MANDATORY to have ANY 'template' (even if empty!) declared @Component!
         */
        // @Host() private appComponent: AppComponent <= throws an ERROR:
        //      Template parse errors: No provider for AppComponent (...): ng:///AppModule/CommandCenterComponent.html@302:4 (first <network-data-point /> component call)
        @Optional() private appComponent: AppComponent,
    ) { }

    ngOnChanges(propChanging: SimpleChanges) {
        /**
         * "streamingAstronautJourney" will be our only Object to fill "this.appComponent.receiveStreamingComm()",
         * the EXCLUSIVELY (from there ALL App's video streams will reach to 'monitor-comms' top monitor '.screen') video stream emitter Method @ 'app' component's TS logic.
         *
         * As so, keep "this.appComponent.receiveStreamingComm()" content data CLEAN, for consecutive stream Data NOT to interfere with next/previous ones,
         * by using Object.assign( {}, ....) in ALL properties that will change - ALL the others will MAINTAIN previous Data content.
         *
         * This way, we can code ONLY the CHANGES, from stream to stream, and not the all, so far, 30 properties of MonitorAstronaut! ;-)
         */
        if (propChanging && propChanging.initOfTimesVideoStreamMoments && !propChanging.initOfTimesVideoStreamMoments.firstChange) {
            // We should have our main 'app's (ngAfterViewInit()) init of times moments (2, so far) content arrived. Broadcast them, each on a video stream:
            this.broadcastOrbitingInitOfTimes();
        }
        if (propChanging && propChanging.landingVideoStreamMoments && !propChanging.landingVideoStreamMoments.firstChange) {
            // We should have our Shuttle landing moments (animation that include our Rover's 'exploring-vehicle' component) content arrived,
            // from 'space-vehicle-fleet' child-container. Broadcast them, each on a video stream:
            this.broadcastLandingMoments(this.appComponent.userSetUpInstructions);
        }
        if (propChanging && propChanging.eachMoveVideoStream && !propChanging.eachMoveVideoStream.firstChange) {
            // Each move 'exploring-vehicle' does on planet expedition, a stream is sent to 'monitor-comms' video streaming monitor
            // via this same netwok video Angular component 'network-video-point' access.
            this.broadcastExploringPlanetStep();

        }

    }

    private broadcastOrbitingInitOfTimes() {
        // FIRST streaming video EVER to mother Earth!

        // =================================================================
        // Stream 5 star "fake" Rover orbiting VIDEO on 'monitor-comms' top Sony monitor.
        // =================================================================
        // Give it some 'timerToStartStreaming' time for the user to have a general vision of the App, and then, for starters,
        // afix the "...trying to connect" message, on same SONY top Monitor.
        // Then we do another streaming, this time, yes, showing Mars orbit video and with the most communication noise possible! ;-)
        const
            // =================================================================
            timerToStartStreaming: InitOfTimesTimmings = this.initOfTimesVideoStreamMoments // 5000
            // =================================================================
            , orbitingCamera = 0
            // NO NOISE, so far, and 'timerToStartStreaming' with NO IMG (just the '...trying to connect') on monitor's '.screen' background:
            , tryingToConnectStream: StreamConditions = { cId: orbitingCamera, nId: null, tImg: timerToStartStreaming.hugeNoiseTime }

        ;
        // =================================================================
        // Emit FIRST stream of all APP times!
        // =================================================================
        const streamFrameChanges: VideoStreamFrame = {
            astronautStatus: { firstChange: true, isOnOrbiting: true }
            , videoScenario: { videoStreamSetUp: tryingToConnectStream }
        };
        const firstStreamedFrame = Object.assign( {}, this.streamingAstronautJourney
            , { frameToStream: Object.assign( {}, this.streamingAstronautJourney.frameToStream, streamFrameChanges,
                { videoScenario: Object.assign( {}, this.streamingAstronautJourney.frameToStream.videoScenario, streamFrameChanges.videoScenario) }
            )}
            , { frameLabel: { localTime: this.returnNowsTime(), text: '1st stream of all APP times! Trying to connect...'} }
        );
        this.appComponent.receiveStreamingComm( firstStreamedFrame );

        // =================================================================
        // Now the second stream - still on orbiting (=> orbitingCamera should be now ON), but with an HUGE noise effect (2) on top
        // - all other stream conditions maintain => pick "firstStreamedFrame" and chamge its camera!
        // =================================================================
        const
            streamScenarioChanges: AstronautScenario = { videoStreamSetUp: { cId: orbitingCamera, nId: 2, tImg: 0 } }
        ;
        const secondStreamedFrame = Object.assign( {}, firstStreamedFrame
                , { frameToStream: Object.assign( {}, firstStreamedFrame.frameToStream,
                    { videoScenario: Object.assign( {}, firstStreamedFrame.frameToStream.videoScenario, streamScenarioChanges) }
                )}
                , { frameLabel: {
                    localTime: this.returnNowsTime(), text: 'Tunning Video comms: our Newtwork Enginners are doing their best, near \'space-expedition \'...'
                }}
            )
        ;
        setTimeout(() => {
            this.appComponent.receiveStreamingComm( secondStreamedFrame );

            // Wait for the horrible comm comunications to finish - they will last the applied CSS noise animation - and broadcast a 3rd and last frame.
            const
                noiseCSSdurationFinishTime: number = this.appComponent.monitorCommsComponentMethods.getStreamCommNoiseDurationById(
                    streamScenarioChanges.videoStreamSetUp.nId
                ) // 9000
            ;
            setTimeout(() => {
                const
                    toEndStreamScenarioChanges = { videoStreamSetUp: { cId: orbitingCamera, nId: null, tImg: 0 } }
                ;
                const toEndStreamedFrame = Object.assign( {}, secondStreamedFrame
                        , { frameToStream: Object.assign( {}, secondStreamedFrame.frameToStream,
                            { videoScenario: Object.assign( {}, secondStreamedFrame.frameToStream.videoScenario, toEndStreamScenarioChanges) }
                        )}
                        , { frameLabel: {
                            localTime: this.returnNowsTime(), text: 'Shuttle ORBITING with cystal comm: waiting YOUR Instructions to deploy Rover on planet!'
                        }}
                    )
                ;
                this.appComponent.receiveStreamingComm( toEndStreamedFrame );

                // And that's it! Pull big Video Hall Monitor up, if it's still down, after 10 secs:
                setTimeout(() => {
                    if (document.getElementById('monitor-video-hall').classList.contains('is-down')) {
                        this.appComponent.monitorCommsComponentMethods.clickJoystickMonitor();
                    }
                }, 10000);

            }, noiseCSSdurationFinishTime);

        }, timerToStartStreaming.hugeNoiseTime);
    }

    private broadcastLandingMoments(withThisInstructions: Instructions) {

            // =================================================================
            const timerToStartStreaming: LandingOperationsTimmings = this.landingVideoStreamMoments;
            // =================================================================

            // =================================================================
            // Video stream of "autoPilotTime" time - 'shuttle-conveyor' "fake" Rover has gone and real Rover was 'born' and is heading for Init Position:
            // =================================================================
            setTimeout(() => {

                const astronautOnCockpit: StreamConditions = { cId: 2, nId: 0, tImg: 0 };
                const streamFrameChanges1: VideoStreamFrame = {
                        astronautStatus: { firstChange: true, isOnOrbiting: false, isOnCockpit : true }
                        , videoScenario: { videoStreamSetUp: astronautOnCockpit }
                    }

                ;
                this.appComponent.receiveStreamingComm(
                    Object.assign( {}, this.streamingAstronautJourney,
                        { frameToStream: Object.assign( {}, this.streamingAstronautJourney.frameToStream,
                            { astronautStatus:  Object.assign( {}, this.streamingAstronautJourney.frameToStream.astronautStatus, streamFrameChanges1.astronautStatus) }
                            , { videoScenario: Object.assign( {}, this.streamingAstronautJourney.frameToStream.videoScenario, streamFrameChanges1.videoScenario) }
                        )}
                        , { frameLabel: { localTime: this.returnNowsTime(), text: 'COCKPIT: automatic pilot ON; real Rover heading to Init Position' } }
                    )
                );

            } , timerToStartStreaming.autoPilotTime);

            // =================================================================
            // Video stream of "astronautTime" - NOT walking, yet!
            // =================================================================
            setTimeout(() => {

                const
                    astronautDeployed: StreamConditions = { cId: 3, nId: 0, tImg: 0 }
                    , naturalIconOrientation = 'E'

                ;
                const streamFrameChanges2: VideoStreamFrame = {
                    astronautPos: {
                        deploymentPlace: withThisInstructions.deployment
                        , onMarsSoil: { coordinates: { x: 0, y: 0}, orientation: withThisInstructions.roverPosition.orientation } // '?' }
                        , byEarthCompass: { coordinates: withThisInstructions.deployment.coordinates, orientation: naturalIconOrientation }
                        // , exploringTime
                    }
                    , astronautStatus: { firstChange: true, isOnCockpit: false, isDeployed: true }
                    , videoScenario: {
                        videoStreamSetUp: astronautDeployed
                        // , cssTags
                        // , cssTransform
                        , cssTimer: withThisInstructions.roverLimits.lagTime
                    }
                };
                this.appComponent.receiveStreamingComm(
                    Object.assign( {}, this.streamingAstronautJourney,
                        { frameToStream: Object.assign( {}, this.streamingAstronautJourney.frameToStream, streamFrameChanges2,
                            { videoScenario: Object.assign( {}, this.streamingAstronautJourney.frameToStream.videoScenario, streamFrameChanges2.videoScenario )  }
                        )}
                        , { frameLabel: { localTime: this.returnNowsTime(), text: 'DEPLOYED: Astronaut goes to Rover\'s Deployment compartment'} }
                    )
                );

            } , timerToStartStreaming.astronautTime);
            // In a moment - exactly (cockpitMoments.totalLandRoverOnMarsTime - cockpitMoments.astronautTime) time
            // astronaut will start REALLY exploring Mars, walking on Mars soil, @ "this.startJourney()" method.

            // =================================================================
            // Video stream of "totalLandRoverOnMarsTime" - Astronaut is (NOT walking, yet! Still inside Rover, on the air) just about to step into Mars soil.
            //  Astronaut is about to abandon 'isDeployed' status - that's the question! See commented "astronautStatus" @ "streamFrameChanges"
            // =================================================================
            setTimeout(() => {
                /**
                 * Well.. really there's nothing to broadcast 'cause nothing CHANGED on MARS!
                 * The only thing happening was TIME consuming (we've reached "cockpitMoments.totalLandRoverOnMarsTime"),
                 * to come to currently reached "init position", STILL inside the Rover (now on Cockpit's automatic pilot)
                 * (remeber Pythagoras Theorem....? On Cockpit's "landRoverOnMars()", to calculate "timeToMoveToInitPos"...? ;-))
                 * If you actually watch our little Rover, it's doing an astonishing "sliding" movement to adjust ORIENTATION to init position!
                 *
                 * So, INSIDE Rover space shuttle, we have changed ASTRONAUT's:
                 * POSITION (or not! Deployment could be on Mars center...): "this.instructions.deployment.coordinates" => "this.instructions.roverPosition.coordinates"
                 * ORIENTATION (or not! Could be the same...): FontAwsome natural icon orientation ('E') => "this.instructions.roverPosition.orientation"
                 *
                 * So... later on, if you can think of any "significant" streaming video possibility here... coder would much appreciated it! You have my number...
                 */

                const astronautOnInitPos: StreamConditions = { cId: 3, nId: 0, tImg: 0 }; // The same...? Yes! We need, at least, the "switch" background noise! Add noise?
                const streamFrameChanges: VideoStreamFrame = {
                    astronautPos: {
                        onMarsSoil: { coordinates: { x: 0, y: 0 }, orientation: withThisInstructions.roverPosition.orientation } // '?' }
                        , byEarthCompass: withThisInstructions.roverPosition
                        // , deploymentPlace
                        // ,exploringTime
                    }
                    , astronautStatus: { isOnCockpit: false, isDeployed: true } // the SAME or { isOnCockpit: true }, if only now Cockpit's image was to show!
                    , videoScenario: { videoStreamSetUp: astronautOnInitPos }
                };
                this.appComponent.receiveStreamingComm(
                    Object.assign( {}, this.streamingAstronautJourney,
                        { frameToStream: Object.assign( {}, this.streamingAstronautJourney.frameToStream, streamFrameChanges,
                            { astronautPos: Object.assign( {}, this.streamingAstronautJourney.frameToStream.astronautPos, streamFrameChanges.astronautPos) }
                            , { astronautStatus: Object.assign( {}, this.streamingAstronautJourney.frameToStream.astronautStatus, streamFrameChanges.astronautStatus) }
                            , { videoScenario: Object.assign( {}, this.streamingAstronautJourney.frameToStream.videoScenario, streamFrameChanges.videoScenario )  }
                        )}
                        , {
                            frameLabel: {
                                localTime: this.returnNowsTime()
                                , text: 'LANDING\'s FINISHED: Astronaut sill inside Rover, is on Instructions INIT POSITION @ Rover\'s Deployment compartment'
                            }
                        }
                    )
                );

            }, timerToStartStreaming.totalLandRoverOnMarsTime );
    }

    private broadcastExploringPlanetStep () {
        const frameId = this.eachMoveVideoStream.frameId;
        // We'll define here, at <network-video-point /> (if you think about it, has all the logic!), which Camera will top monitor screen switched into.
        // Alltough we'll use the same camera at each planet soil step, we need to declare it at each step, to have the background "switch" camera communication noise!
        // We'll use inside Deployed compartment camera on last step - Astronaut get's "deployed" again, inside Rover, and left Mars soil
        let thisStepStreamConditions: StreamConditions = { cId: 1, nId: null, tImg: 0 };

        switch (frameId) {
            case 0:
                Object.assign( this.eachMoveVideoStream,
                    { frameLabel: { localTime: this.returnNowsTime(), text: 'EXPLORING: 1st stream on planet soil!' } }
                );
                break;

            case 1:
                Object.assign( this.eachMoveVideoStream,
                    { frameLabel: { localTime: this.returnNowsTime() , text: 'EXPLORING STEP: streaming Astronaut\'s planet exploration...' } }
                );
                break;

            case 2:
                thisStepStreamConditions = { cId: 3, nId: 0, tImg: 0 };
                Object.assign( this.eachMoveVideoStream,
                    { frameLabel: { localTime: this.returnNowsTime(), text: 'FINISH EXPLORING: Astronaut returning to DEPLOYED status, inside Rover.' } }
                );
                break;

            default:
                break;
        }
        // Emit to 'monitor-comms', from 'app' component, adding same 'thisStepStreamConditions' for all exploration steps:
        this.appComponent.receiveStreamingComm(
            Object.assign({}, this.eachMoveVideoStream, {
                frameToStream: Object.assign( {}, this.eachMoveVideoStream.frameToStream, {
                    videoScenario:  Object.assign( {}, this.eachMoveVideoStream.frameToStream.videoScenario, {
                        videoStreamSetUp: thisStepStreamConditions
                    })
                })
            })
        );
    }

    private returnNowsTime() {
        const now = new Date();
        return now.toLocaleTimeString() + '.' + now.getMilliseconds();

    }

}
