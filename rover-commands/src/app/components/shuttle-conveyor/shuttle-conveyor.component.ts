import { Component, Input } from '@angular/core';

@Component({
    selector: 'shuttle-conveyor',
    templateUrl: './shuttle-conveyor.component.html',
    styleUrls: ['./../../child-containers/deployed-expedition/deployed-expedition.css']
})
export class ShuttleConveyorComponent {

    @Input() nXcells: number;

    constructor() { }

}
