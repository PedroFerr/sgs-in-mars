import { Component, Optional, ViewChild, Input, Output, EventEmitter, OnInit, OnChanges, SimpleChanges, ElementRef, Renderer2 } from '@angular/core';

// Import the Parent where <section class="ui-ux-controller"> is:
import { SpaceExpeditionComponent } from './../../app-containers/space-expedition/space-expedition';
// Import the Engine and the Cockpit childrens of this Component:
import { VehicleMercedesEngineComponent } from './../vehicle-mercedes-engine/vehicle-mercedes-engine.component';
import { VehicleCockpitComponent } from './../vehicle-cockpit/vehicle-cockpit.component';

// Types used through out the entire App:
import {
    Position, UnitPathMove, VehicleInstructions, JourneyRoadMap, JourneyRoadMapInitializer
    , CockpitDriveInstructions, MoveReport
    , LandingOperationsTimmings
    , SpeakerVoice, MonitorData, MonitorAstronaut
} from 'src/app/app.interfaces';

// Plain JS function utilities to plug them in:
import * as JsPlugIns from './../../_js-utils';

// Use JQuery on this component...?
declare var $: any;

@Component({
    selector: 'exploring-vehicle',
    templateUrl: './exploring-vehicle.component.html',
    styleUrls: ['./../../child-containers/deployed-expedition/deployed-expedition.css']
})
export class ExploringVehicleComponent implements OnInit, OnChanges {

    @ViewChild(VehicleMercedesEngineComponent) MercedesBenzEngine: VehicleMercedesEngineComponent;
    @ViewChild(VehicleCockpitComponent) Cockpit: VehicleCockpitComponent;
    // Grab previous Component's main HTML tags actors:
    exploringVehicleIcon: HTMLElement;
    // We don't specify them here because DOM is always changing in many Components at the same time - make it WHEN needed!

    // All shuttle landing moments, will allow us to do 1st camera switch to Rover's Cockpit (STILL inside Shuttle; not deployed yet!)
    // and then to "interior-deploy" camera and finally to the one installed outside, pointing to Mar's planet soil + Astronaut:
    @Input() getLandingMomentsTimerObj: LandingOperationsTimmings;

    @Input() startExplorationJourney: boolean;
    // ====================================
    instructions: VehicleInstructions;
    // ====================================
    engineSpeedPerMove: number;

    // Defined, and re-defined, from the begining, at 'space-expedition', on EACH user SUBITED command.
    // it's here, once each journey starts, that "currentJourneyRoadMap"s "position"/"nextPosition" and "journeysMoves" will be UPDATED:
    @Input() currentJourneyRoadMap: JourneyRoadMap;
    position: Position;
    nextPosition: Position;
    // ...precisely by the Mercedes Engine (outputing cumulated "journeysMoves" RoadMap)
    journeysMoves: Array<UnitPathMove>;
    // and Cockpit (once RoadMap is totally achieved) by travelling it, UnitPathMove by UnitPathMove, doing a REAL changing Position - Rover's icon CSS - steps JOURNEY.
    journeyCockpitDriveInstructions: CockpitDriveInstructions;

    // ====================================

    // Every cockpit talk, will be dipslayed at 'speaker-comms' BS4 Tooltip:
    exploringMovementTalk: SpeakerVoice;
    // Every exchanged Data will be printed on 'monitor-comms' BOTTOM monitor:
    exploringInitData: MonitorData;
    exploringMovementData: MonitorData;
    exploringLastMovementData: MonitorData;
    // Every step of vehicle's planet exploration, will be on 'monitor-comms' TOP monitor:
    exploringMovementStream: MonitorAstronaut;

    // Once real Rover gets born ;-), carrying an Astronaut inside, to explore Mars,
    // will get to Instructions "init position" => the astronaut is considered deployed on Mars
    // isAstronautDeployed = false;
    // After being deployed, next boolean can be:
    // * TRUE: if Astronaut is touching Mars soil, it will have to stick with Mars cartesian cardinals
    //         - if MARS rotates (translates, zoom), Astronaut (and Rover) rotates (translates, zoom) the same with it.
    // * FALSE: if Rover is still travelling, on the air, not having Astronaut fixed at Mars soil, it will follow Earth compass
    //         - it doesn't matter if Mars rotates (translates, zoom), it should NEVER alter its EARTH (#deployment-point-compass) relative position.
    @Output() private isAstronautOnMarsSoil = new EventEmitter<boolean>();

    // In the end, on last "eachMove", final journey results are to be sent back:
    @Output() private finishExplorationJourney = new EventEmitter<MoveReport>();

    constructor(
        private thisDOM: ElementRef,
        private renderer: Renderer2,
        /**
         * Have access to Host SpaceExpeditionComponent component (parent of this) Methods AS A SERVICE!
         * BUT BE CAREFULL!
         *
         * If "AppComponent" can not (still...) be found by Angular, an error will be thrown, on mounting it!
         * And it can happen NO ERROR is thrown at all - simply THIS Component won't work at all, which is even more scary...
         *
         * "Yes you can use (and MUST, here)
         *      constructor (@Host() @SkipSelf() @Optional() toys: ToysService) {...}"
         * " - check it here: https://medium.com/frontend-coach/self-or-optional-host-the-visual-guide-to-angular-di-decorators-73fbbb5c8658
         *
         * We're NOT "calling" a component!
         * We are INJECTING a SERVICE, that will bring all 'app' properties, at this Component's Class constructor!
         *
         * This also makes MANDATORY to have ANY 'template' (even if empty!) declared @Component!
         */
        // @Host() private appComponent: AppComponent <= throws an ERROR:
        //      Template parse errors: No provider for AppComponent (...): ng:///AppModule/CommandCenterComponent.html@302:4 (first <network-data-point /> component call)
        @Optional() private spaceExpeditionComponent: SpaceExpeditionComponent

    ) { }

    ngOnInit() {

        // Try Mercedez Benz engine's MOVE with a cardinal point and a single command unit (A, L, R)... and test it.
        const isIToTry = false;
        // Put upper flag to 'true' and Mercedez will do just the FIRST char of the sent command string
        if (this.MercedesBenzEngine) {    // Might be mounting!
        // ---------------------------------------------------------------------------------------
            this.MercedesBenzEngine.tryEngineMove(
                {
                    thisOrientation: 'S'              // this.position.orientation
                    , thisPosition: { x: 1, y: 0 }    // this.position
                    , commandChar: 'A'                // this.instructions.charCommand[0]
                }, isIToTry
            );
        // ---------------------------------------------------------------------------------------
        }

    }

    ngOnChanges(propChanging: SimpleChanges) {
        if (propChanging && propChanging.getLandingMomentsTimerObj && !propChanging.getLandingMomentsTimerObj.firstChange) {
            // All landing timmings were calculated @ this.calculateLandingMoments() from 'vehicle-fleet-operations'
            // (ORIGINALly @ 'space-expedition' ngOnChanges() of "this.instructions" var)
            // and as soon as they arrive HERE, we can start to video broadcast using <network-stream-point /> [landingVideoStreamMoments] Input,
            // from 1st moments of Rover's "life", still inside the Shutle Conveyor wich will deploy it
            // (so Astronaut, inside Rover, can start really exploring Mars planet!)
            //
            // MIND YOU:
            // ORIGINAL "this.renderer.setStyle(this.rover, 'transition-duration', efectsCSStimer);" @ 'space-expedition' ngOnChages()
            // that will synchronize EVERYTHIING - rover movements, Data printing, Tooltips and Streaming Video Moments
            // is precisely SET further down, on this same ngOnChanges() - if "propChanging.startExplorationJourney"

            // This "getLandingMomentsTimerObj" var is only used at Template's HTML.
        }

        if (propChanging && propChanging.startExplorationJourney && !propChanging.startExplorationJourney.firstChange) {
            // This means that Landing operations are over!
            // And Instructions should be alreay available (@ propChanging.currentJourneyRoadMap, below)
            /**
             * ============================================================================================================
             * Instructions came long time ago, for first Shuttle landing on 'planet'
             * - we had "totalLandRoverOnMarsTime" time, while Shuttle was performing a superb landing animation... or none!
             * ============================================================================================================
             * If user just has submitted a SECOND journey, with "this.landingData.isToCumulateJourneys === 'true'" (starting point is the last arrived one),
             * method "calculateLandingMoments()"( @ 'vehicle-fleet-operations') has returned all timers to ZERO
             *         { shuttleTime: 0, roverTime: 0, astronautTime: 0, totalLandRoverOnMarsTime: 0 };
             * => no animation was performed at all!
             *
             * So, REMEMBER, in this extreme "fast" case, oposite to the longer App's beginning of times one,
             * instructions will be available, here, at the exact SAME TIME as 'startExplorationJourney' was set to true
             * on "ngOnChanges()" @ the same 'vehicle-fleet-operations' component!
             * Angular does NOT HAVE exact time... :( Be aware; be VERY aware...
             */
            // Just before, set Rover's pace on planet's exploration (came form far away 'space-expedition', ngOnChanges()):
            if (this.startExplorationJourney) {
                this.exploringVehicleIcon = this.Cockpit.exploringVehicleIcon;

                const
                    userSetSpeed = this.instructions.roverLimits.lagTime
                    , efectsCSStimer: string = ( userSetSpeed / 1000).toFixed(2) + 's'

                ;
                this.renderer.setStyle(this.exploringVehicleIcon, 'transition-duration', efectsCSStimer);
                // --------------------------------------------------------------------
                // Set the Rover's movement "pace" with a FINE TUNNING
                // (remember, @ #rover CSS, we have a non-linear 'transition-timing-function: ease-in-out')
                // --------------------------------------------------------------------
                // --------------------------------------------------------------------
                this.engineSpeedPerMove = userSetSpeed * 1.2;
                // --------------------------------------------------------------------
                // --------------------------------------------------------------------

                this.startJourney();
            }
        }

        if (propChanging && propChanging.currentJourneyRoadMap && !propChanging.currentJourneyRoadMap.firstChange) {
            // If "currentJourneyRoadMap" has arrived, we can collect user submitted Instructions (@ 'command-center' form), on 'space-expeditions's @Input() instructions:
            this.instructions = Object.assign( {}, this.spaceExpeditionComponent.instructions);
            // ==================================================================
            // CAREFULL with this Object vars "called from outside" as a SERVICE! Remember THIS is NOT an ngOnChange() property changing!
            // ==================================================================
            // - isolate Objects! You could, from here, modified the (already sent!) entire Instructions @ SpaceExpeditionComponent - and from there, even back!
            // For instances, "this.currentJourneyRoadMap" (below) is OK to manipulate - it's an @Input() and NOT a getter from a Service!
            // ==================================================================

            let echoStr = '"Hi folks!" - says Rover @ \'exploring-vehicle\'. "My positioning order has come!';
            if (this.currentJourneyRoadMap.lastPosIndex === JourneyRoadMapInitializer.lastPosIndex) {
                echoStr = echoStr + ' Cooool - let\'s GO to 1st position!"';
            } else {
                echoStr = echoStr + ' Ok; we\'ll start from THIS just arrived position."';
            }

            this.exploringInitData = { msg: echoStr, data: this.currentJourneyRoadMap };
        }

    }

    /**
     * Translate received command line string into a Position where Rover must reach
     * => decode each letter (driving command unit) into a
     *     * GPS type that will take the Rover to, in Mars,
     *     * facing a certain cardinal point orientation, from a pretty simple compass = ['N', 'E', 'S', 'W']
     *
     * Each time user submits a new set of JOURNEY values, that could be CHANGED from previous one, (reflected @ currentJourneyRoadMap prop CHANGES),
     * we have "this.instructions" and "this.currentJourneyRoadMap" Objects available - last one with either Instructions (1st user submision), or LAST journey known values
     *
     * REMEMBER before Rover (icon), in fact, moves to 1st position through Cockpit driving, we already computed it @ Rover's Engine!
     * Carefull with GPS vars content...
     * Each "nextPosition" WILL happen immediately you put "MercedesBenzEngine" to work - NOT at each real driving moment - much before Cockpit drives to anywhere!
     *
     * Just before emitting back to 'app-root', last move flag is on "eachMove.data.isFinalMove"
     */
    private startJourney() {
        this.journeysMoves = this.currentJourneyRoadMap.journeysMoves;
        this.position = this.currentJourneyRoadMap.lastPos;
        // Next is achieved inside Rover's Mercedes Benz Engine, by processing EACH of THIS journey's "this.instructions.charCommand" char
        // - so far, at each journey begining.... is ALWAYS null!
        this.nextPosition = { coordinates: null, orientation: null };

        for (let n = 0; n < this.instructions.charCommand.length; n++) {
            // ==============================================
            // COMPUTE IT step by step!
            // ==============================================
            // The Rover's heart  - move its Mercedes Benz ENGINE so COCKPIT can, then, drive Rover through this Mars journey!

            // As the user can cumulate, or not, journeys, we should also cumulate, or not, the engine's movements - pair "this.journeysMoves" / "this.indexMoves".
            // It's a job for both ENGINE and COCKPIT to do:
            //    1) current 'this.journeysMoves' is passed in to MercedesBenzEngine that naturaly increments it - through "movesList" - after ENTIRE journey is processed;
            //    2) at EACH REAL step by step Rover's Cockpit MOVE - Cockpit's "thisJourneyStepTimeMoment" moment in TIME cycle,
            //    we increment Cockpit's "roadMap_nMove" counter to cumulate (or not!) with each JOURNEY's arrived set of "this.instructions.charCommand"
            // ==============================================
            this.nextPosition.orientation = this.MercedesBenzEngine.rotateTo(this.instructions.charCommand[n], this.position.orientation);
            this.MercedesBenzEngine.commandToMovement_translator(this.position, this.nextPosition, this.journeysMoves);
            // ==============================================
            this.position = Object.assign({}, this.nextPosition);
            // ==============================================
        }
        // ONCE we have the engine with ALL moves COMPUTED from commands, just sit back and enjoy travelling your JOURNEY Rover through Mars
        // - there will be VOICE (BS4 Tooltips), DATA (printed on 'monitor-comms' bottom monitor) and streaming VIDEO (on top one) of EACH step to enjoy!
        const cockpitInstructions: CockpitDriveInstructions = {
            currentJourneyRoadMap: this.currentJourneyRoadMap
            , submitedInstructions: this.instructions
            , engineSpeedPerMove: this.engineSpeedPerMove
        };
        // console.error('"cockpitInstructions" sent to Cokpit, AFTER Mercedes Engine calculous', cockpitInstructions);
        // "cockpitInstructions.currentJourneyRoadMap" SHOULD contain UPDATED "journeysMoves" by previous Mercedes Benz Engine cycle!
        // On the other hand, initial "lastPos" and "lastPosIndex" props from LAST JOURNEY, SHOULD enter Cockpit's "receiveJourneyDriveInstructions" INTACT!
        this.journeyCockpitDriveInstructions = Object.assign( {}, cockpitInstructions);
        // BUT should enter each this.onEachMoveReport() below method UPDATED on each Cockpit's "eachMoveReport" emitter to there!
    }

    /**
     * On each 'thisDataStep' a few affairs must be done, concerning this component's parents
     *
     * Please note that at EACH of this "stepReport.journeyRoadMap":
     *     1) "journeysMoves" prop was already set at Rover's Mercedes Engine (on upper "startJourney()") FULL journey's MOVE cycle, BEFORE Cockpit drives it.
     *     2) But "lastPos" and "lastPosIndex" shoud be seen its init (journey) values INCREMENTING step by step,
     *         as Cocckpit is driving the journey and emitting (and streaming, speaking, etc.).
     *     This value is of most importance to emit back to 'space-expedition' where, combined with received Instructions from parent 'command-center',
     *         the "currentJourneyRoadMap" (sent to here, on each user submission!) can be set DIFFERENTLY on (this.instructions.isToCumulateJourneys === 'false') CONDITION.
     *     This EMITTER is, precisely, "this.finishExplorationJourney.emit(stepReport)", find below when LAST MOVE condition is veryfied.
     *
     * Concerning Mars/Earth relative COMPASS move coordinates, our Astronaut explorer is finally being pull down, from Deployed compartment into Mars soil - see it on tely!
     * Its Position coordinates are definately sticked to Mars compass GPS, and till the Astronaut gets lifted up, have "NOTHING" to do with ABSOLUTE Earth GPS!
     * Well... wouldn't say "nothing"; better is saying "are, from now on, co-related to". ;-)
     *
     * When this journey ends, you should be able to see on 'monitor-comms' top monitor, Astronaut explorer being pulled up, again, to Rover's Deployed compartment.
     * Then, this "isAstronautOnMarsSoil" should emit (false); ;-)
     *
     * @param withThisData data produced on each JOURNEY's step, in particularly the MoveReport.journeyRoadMap's "journeysMoves" array
     *                         and "lastPosIndex" number and "lastPos" Position properties
     */
    onEachMoveReport(withThisData: { eachMove: MonitorData, thisJourneyNmove: number }) {
        // Clarify "MonitorData.data" (Position | Instructions | VehicleInstructions | JourneyRoadMap | MoveReport | null), so TS can continue to help our coding.
        // Btw, now is a pretty fancy init moment to update, for each "withThisData", if restricted area was crossed or not.
        const
            originalStepReport = withThisData.eachMove.data as MoveReport
            , outOfRestrictedArea = this.hasCrossRestrictedArea(originalStepReport, this.instructions)
            , stepReportData: MoveReport = Object.assign( {}, originalStepReport, { isRestrictedAreaCrossed: outOfRestrictedArea})

        ;
        // For future debug/scability, DON'T affect BACK each "withThisData" DATA ("eachMove.data" in particularly), to keep track of "what's hapening, where and when":
        // So we clone this coming "withThisData", with the same Type, into a separate "new" one, but with "isRestrictedAreaCrossed" affected at the right moment:
        const
            thisDataStep: { eachMove: MonitorData, nMove: number } = {
                eachMove: { msg: withThisData.eachMove.msg, data: stepReportData }
                , nMove: withThisData.thisJourneyNmove
            }
            // To simplify namespaces, and maintain THIS component's coherence, we rename the movie stars:
            , data = thisDataStep.eachMove as MonitorData
            , report = thisDataStep.eachMove.data as MoveReport
            , n = thisDataStep.nMove

        ;
        // --------------------------------------------------------------------
        if (n === 0) {
            // Emit only once, to save some communications bandwidth: ;-))))))))
            this.isAstronautOnMarsSoil.emit(true);
            // Allow user set of clicks @ 'space-expedition's "section.ui-ux-controller > buttons-container", for 'planet' to be CSS transform:
            this.spaceExpeditionComponent.enableToClickTransformationButtons = true;
            // On each new user Instructions submited, buttons will be disabled again... till we reach here.
        }
        // --------------------------------------------------------------------

        // Once this MoveReport has "outOfRestrictedArea" flag, plot journey's road map @ 'space-expedition',
        // so we can keep listing there, step by step, the full RoadMap being happening:
        this.spaceExpeditionComponent.plotJourneyJourneyRoadMap(n, report);

        // Everything's ready to tell to the user (by bottom 'monitor-comms' DATA monitor screen) a MOVE is completed - could be the last one (eachMove.data.isFinalMove)!
        this.exploringMovementData = data;

        // Astronaut is about to abandon Mars soil - AFTER this FINAL 'engineSpeedPerMove' (return to 'isDeployed' stream video status, being lifted to Rover's Deployed door):
        if (n === this.instructions.charCommand.length - 1) {
            setTimeout( () => {
                // Time to send back the journey's final results, on last of "eachMove"
                // and to tell to Earth ('monitor-comms' bottom monitor, through closest <network-data-point /> - see template) the LAST MOVE is completed:
                this.finishExplorationJourney.emit(report);
                this.exploringLastMovementData = data;
                // --------------------------------------------------------------------
                this.isAstronautOnMarsSoil.emit(false);
                // --------------------------------------------------------------------
            }, this.engineSpeedPerMove);
        }
    }

    /**
     * Each move 'exploring-vehicle' does on planet expedition, a stream of video is sent to 'monitor-comms',
     * via 'app''s "receiveStreamingComm()" dispatcher, through the nearest 'network-data-point'
     *
     * @param dataStream the video stream Object content
     */
    onEachMoveStream(dataStream: MonitorAstronaut) {
        this.exploringMovementStream = dataStream;
    }

    /**
     * Receives a 'talk' (object with params),
     * once each 'vehicle-cockpit's doMoveRover() method, called from ngOnChanges()'s driveEachMove() cycle step, gets accomplished.
     *
     * @param dataVoice the 'talk', on each journey step, emitted by 'vehicle-cockpit'
     */
    onEachMoveSpeak(dataVoice: SpeakerVoice) {
        this.exploringMovementTalk = dataVoice;
    }


    // ====================
    // AUX funtions:
    // ====================

    private hasCrossRestrictedArea(thisMoveData: MoveReport, instructionsData: VehicleInstructions ): boolean {
        // "lastPosIndex" has been updated (since last journey one, received @ this.startJourney()) @ Cockpit's "eachMoveReport" emitter
        const
            currentIndexMoves = thisMoveData.journeyRoadMap.lastPosIndex
            , lastReachedPos = thisMoveData.journeyRoadMap.journeysMoves[currentIndexMoves].to

        ;
        let wasItCrossed = false;
        if (
            lastReachedPos.coordinates.x > instructionsData.roverLimits.area.width || lastReachedPos.coordinates.x < 0
                ||
            lastReachedPos.coordinates.y > instructionsData.roverLimits.area.height || lastReachedPos.coordinates.y < 0
        ) {
            wasItCrossed = true;
        }

        return wasItCrossed;

    }
}
