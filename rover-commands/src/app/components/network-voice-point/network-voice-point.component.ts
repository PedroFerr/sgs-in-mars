import { Component, Optional, Input, OnChanges } from '@angular/core';

import { DeployedExpeditionComponent } from 'src/app/child-containers/deployed-expedition/deployed-expedition';

import { SpeakerVoice } from 'src/app/app.interfaces';

@Component({
    selector: 'network-voice-point',
    template: ``
})
export class NetworkVoicePointComponent implements OnChanges {

    @Input() eachMoveTalk: SpeakerVoice;

    constructor(
        /**
         * Have access to Host 'deployed-expedition' component Methods AS A SERVICE!
         * BUT BE CAREFULL!
         *
         * We're NOT "calling" a component!
         * We are INJECTING a SERVICE, that will bring all 'deployed-expedition' properties, at this Component's Class constructor!
         * If "DeployedExpeditionComponent" can not (still...) be found by Angular, an error will be thrown, on mounting it!
         * And it can happen NO ERROR is thrown at all - simply THIS Component won't work at all, which is even more scary...
         *
         * "Yes you can use (and MUST, here)
         *      constructor (@Host() @SkipSelf() @Optional() toys: ToysService) {...}"
         * " - check it here: https://medium.com/frontend-coach/self-or-optional-host-the-visual-guide-to-angular-di-decorators-73fbbb5c8658
         *
         * This also makes MANDATORY to have ANY 'template' (even if empty!) declared @Component!
         */
        // @Host() private deployedExpeditionComponent: DeployedExpeditionComponent <= throws an ERROR:
        //      Template parse errors: No provider for AppComponent (...): ng:///AppModule/CommandCenterComponent.html@302:4 (first <network-data-point /> component call)
        @Optional() private deployedExpeditionComponent: DeployedExpeditionComponent
    ) { }

    ngOnChanges() {

        if (this.eachMoveTalk) {
            /**
             * MIND YOU
             * there can NOT EXIST any <network-VOICE-point /> (HTML markup) access point BEFORE 'deployed-expedition' component!!!
             *
             * Which, in fact, would make no sense at all... it's at this component's HTML Template we mount our 'vehicle-fleet-operations'
             * => it's not possible to exist any vehicle operation (implying some vehicle "talk"ing to planet's atmosphere) BEHIND 'vehicle-fleet-operations' component...
             *
             * Even the Shuttle Conveyor - the 1st vehicle to animate, still carrying Rover (the 'vehicle-exploration' component) inside
             * has it's markup precisely @ 'vehicle-fleet-operations' HTML Template - CHILDREN of the 'deployed-expedition'.
             */
            this.deployedExpeditionComponent.sendVoiceToSpeakerComms(this.eachMoveTalk);
        }
    }
}
