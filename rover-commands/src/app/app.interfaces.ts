// =================================================================
// @ Earth's 'command-center's submit INSTRUCTIONS moment:
// =================================================================

export interface AreaMeasures {
    width: number;
    height: number;
}
export interface LimitedBy {
    area: AreaMeasures;
    lagTime: number;
}

export interface GPS {
    x: number;
    y: number;
}

export type OrientationChar = 'N' | 'S' | 'E' | 'W';
export interface Position {
    coordinates: GPS;
    orientation: OrientationChar;
}

/**
 * And now user can choose its 'favourite' deployment Place - rather then a cold GPS object,
 * its a better attention caller, easier to UI/UX follow and much better, and faster, understood!
 */
export interface DeploymentPlace {
    place: string;
    coordinates: GPS;
    color: string;
}

// In some components, "command" props are:
// - a unique command of letters (string), like AAAALRRLRA
// - an array of (each) letters (also strings!) like ['A', 'L', 'R', 'R']
interface CoreInstructions {
    deployment: DeploymentPlace;
    roverLimits: LimitedBy;
    roverPosition: Position;
    isToCumulateJourneys: boolean;
}
export interface Instructions extends CoreInstructions {
    command: string;
}
export interface VehicleInstructions extends CoreInstructions {
    charCommand: string[];
}

// =================================================================
// @ 'planet's 'deployed-expedition', the 'vehicle-fleet-operations's LANDING moments
// =================================================================

/**
 * Before reching Mars, Rover is being carried throughout the space, on a Shuttle Conveyor,
 * that is responsible to Deploy Rover on Mars, from DeploymentPlace to CoreInstructions.roverPosition "init position",
 * that, in turns, will Deploy, at this point, an Astronaut to do the walking-on-mars expedition.
 *
 * In order to synchronize landing oprations timmings with the streaming video,
 * there are certain "points", in time, that need an emission to 'monitor-comms'
 *
 * It's the way to communicate from 'rover-cockpit' to the outside
 * (method "landCockpitOnPLanet()" returns all these different timmings, while still cumulating them, to do a landing animations, itself, on Mars planet)
 * whenever some different stream is awaited at 'monitor-comm' top Monitor
 */
export interface LandingOperationsTimmings {
    shuttleTime: number;
    roverTime: number;
    astronautTime: number;
    totalLandRoverOnMarsTime: number;
    // Needed, at some point, this Pythagoras theorem calculated, and isolated, time for a nice UX!
    autoPilotTime?: number;
}

/**
 * To achieve this, we pass LandingOperationsData to <shuttle-conveyor />,
 * so shuttle deploys Rover, on Planet, precisely at Init Position inputted by the user:
 */
export interface LandingOperationsData {
    // Next just MIGHT not be "this.instructions.roverPosition", Instructions init position...
    // Could be the END point of last journey. If so, Rover MIGHT have to go... to Instructions init position (or start from where it stopped)
    startingPointOnPlanet: Position;
    // We also need, to accomplish synchronized landing operations:
    speedOnPlanet: number;
    isToCumulateJourneys: boolean;
}

/**
 * At App's init of times (only happens ONCE, in App's loading life time), we also have special streaming moments,
 * to broadcast @ 'monitor-comms' top monitor's screen:
 */
export interface InitOfTimesTimmings {
    tryingToConnectTime: number;
    hugeNoiseTime: number;
}
// =================================================================
// @ 'vehicle-fleet-operations's 'exploring-vehicle' EXPLORATION of 'planet' moments
// =================================================================

/**
 * For Rover ('vehicle-cockpit') to do its expedition,
 * it has to move, step-by-step => journey's "UnitPathMove" unity, to build a RoadMap of all journeys!
 *
 * @param pointToGoTo the already computed position to reach
 * @param msg msg to afix at Rover's tooltip through 'speaker-comms's "voiceToBeRead" @Input().
 * @param isLastMove if it is, Rover tooltip will varnish, after few seconds
 * @param delayCSSTransition the vehicle's position, to reach, after some (CSS transition) delay time
 * @param waitForCSSTransition if "true" (the default) 'msg' to show is to be synchronized wih rover's (any) movement CSS transition value
 *             If "false", means it's to execute right now; any needed delayed time was already provided by some setTimeout(), at the outside caller
 */
export interface VehicleMove {
    pointToGoTo: Position;
    msg: string;
    isLastMove: boolean;
    delayCSSTransition: number;
    waitForCSSTransition: boolean;
}
export const VehicleMoveInitializer = {
    pointToGoTo: { coordinates: {x: 0, y: 0}, orientation: 'E'  as OrientationChar}
    , msg: 'No "msg" was step up for this Vehicle\'s Move!'
    , isLastMove: false
    , delayCSSTransition: -1
    , waitForCSSTransition: true
};

export interface UnitPathMove {
    from: Position;
    to: Position;
}

export interface JourneyRoadMap {
    journeysMoves: Array<UnitPathMove>;
    lastPos: Position;
    lastPosIndex: number;
}
export const JourneyRoadMapInitializer = {
    journeysMoves: [] as Array<UnitPathMove>
    , lastPos: { coordinates: null, orientation: null } as { coordinates: GPS, orientation: OrientationChar }
    , lastPosIndex: -1
};

// Keeps all positions of the on going journey(s), that cumulate until user sets "Start at last inputted init position?"
export interface RoadMap {
    movesList: Array<UnitPathMove>;
}

// Each Engine ENTIRELY computed 'movesList' output will be sent to Rover's Cockpit, to execute each exploration step move:
export interface CockpitDriveInstructions {
    currentJourneyRoadMap: JourneyRoadMap;
    submitedInstructions: VehicleInstructions;
    engineSpeedPerMove: number;    // NOT the same Instructions "lagTime" - is affected by a small factor!
}

// At each Rover's move on Mars, a report can be comunicated, printed or analysed:
// Final one is the same as the others - only sent on last move's timming (so we have deleted former "FinalReport" type!)
export interface MoveReport {
    journeyRoadMap: JourneyRoadMap;
    // reachedPosition: Position;
    isRestrictedAreaCrossed: boolean;
    isFinalMove: boolean;
}

/**
 * Before, or after, deploying our Rover in Mars, in order to explore Mars entirely in a confined window
 * we should be able to move/zoom/rotate '.grid-container' - has the entire gear deployed by 'command-center' command operations of "Rover in Mars" expedition! ;-)
 * It's the equivalent, on Mars, as the Position type is at vehicle's deployed point @ Earth's referential '#deployment-point-compass'
 *
 * You can re-position anything INSIDE '<section class="mars" />', manipulating CSS style 'transform' property,
 * not loosing, by any means, or any circumstances, (related) position to each different related referentials.
 *
 * In all of them you should be able to apply all, or each, of the CSS properties:
 *     * scale - zoom in/out the whatever markup, related to its cartesian reference compass HTML markup.
 *     * rotate - turn whatever markup to any ['N', 'E', 'S', 'W' cardinal positions] or [left/right turns direction], related to its cartesian reference compass HTML markup.
 *     * translate(X,Y) - move whatever markup either along the [N/S or W/E] or ['x' or the 'y'] axis, related to its cartesian reference compass HTML markup.
 *
 * So, in particularly for what's coded, this CSS (new) position can have different related {0,0} cartesian points:
 *     @ <div class="grid-center" />, related to (the center of) '<section class="mars" />' (#deployment-point-compass)
 *     @ <div id="rover" /> (each) position related to fix <div class="grid-center" />
 *     @ <div class="grid-items-wrapper" /> (center) position @ Rover's init position, also related to fix <div class="grid-center" />,
 *         wrapper of "n" <div class="grid-item" /> mapping (NOT calculating - this is done @ Rover's MercedesBenzEngine!) Rover's movements into a grid
 *     @ <div class="grid-container" />, that has all the stuff deployed @ Mars ('.grid-items-wrapper' + '#rover' + '.waiting-orbit' + '.grid-center'),
 *         user manipulated postition related to '<section class="mars" />' - the browser's window to see what's happening in Mars!
 */
export interface CSStransformPos {
    x: number;        // represents a number in CSS 'px'/ 'rem' / whatever suits your scale factor
    y: number;        // represents a number in CSS 'px'/ 'rem' / whatever suits your scale factor
    scale: number;    // represents a number in CSS between 0 (display: none) and 1 (scale is reducing; '1' means no-scalling) and > 1 (scale is augmenting)
    rotate: number;   // represents a number in CSS 'xdeg', where 'x' can be positive (rotating right) or negative (to the left).
                      // MIND YOU sign is of most importance, here - using Math.abs(x), will produce entirely different CSS 'transform' outcomes!
}
// So, any moving layer, deployed @ {0,0}, has an init CSSzeroPos, which we'll CSS manipulate it from there:
export const CSSzeroPos: CSStransformPos = {
    x: 0, y: 0,
    scale: 1,
    rotate: 0,
};

// =================================================================
// @ 'XX-comms's 'space-expedition' COMMUNICATION (3 different NETWORKs) moments
// =================================================================
/**
 * We have a complex NETWORK installed on several strategic "points", both convenient to HTML markup and to TS logic.
 * Each network can carry VOICE, DATA or VIDEO information Objects, ALL through ANY Angular Component, whose parent of all is 'app' component.
 *
 * VOICE one will be "displayed" on several BS4 Tooltips, whose markup is @ 'speaker-comms'
 * (inside 'deployed-expedition's HTML template, right next to 'vehicle-fleet-operations').
 * 'speaker-comms' HTML Template has <div class="rover-tooltip"></div> inside, specific marked up as a DIRECT CHILD of '.grid-container',
 * our fixed, central compass where the ENTIRE gear was deployed at Mars, and will move from there.
 *
 * Remember Rover, or any expedition's required vehicle, will move FROM this point on, according to the received user setup Instructions...
 * So, with this HTML markup, relative movements will be the SAME for ANY pair 'deployed-expedition' vehicle / Tooltip - the vehicle "talking" voice to planet's atmosphere.

 * DATA/VIDEO will be displayed at 'monitor-comms' container's BOTTOM/TOP monitor's screen!
 *
 * -----------------------------------------------------------------
 * ONE RULE: NO component sends/receives data, to/from ANY Network, DIRECTLY to any component!
 * -----------------------------------------------------------------
 * ALL voice, data and video network comms MUST be sent through (as many as needed) ACCESS END POINTS, to 'app' (data and video) / 'deployed-expedition' (voice) component
 * and, from there, dispatched to the proper 'monitor-comms' or 'speaker-comms' component.
 * Implementing this strict rule, we can better control the entire INFORMATION FLOW (@Input/@Output()), circulating in the WHOLE App!
 *
 * This way It will be easy, latter on, to implement REDUX, since all App's (information) state change is, from the beginning, concentrated on one single (parent) component.
 *
 * It's cool, this 'deployed-expedition'! ;-)
 *
 */

// =================================================================
// VOICE, through <network-voice-point /> NETWORK
// =================================================================
/**
 * The 'network-voice-point' data, so BS4 Tooltip appears with some voice 'msg', on the right 'transformAtPos' place and at the right 'waitForCSSTransition' time moment:
 *
 * @param msg the text tooltip that BS4 Tooltip should change to, right NOW
 * @param transformAtPos the tooltip's Rover position, to achieve (could be the same as previous one...)
 * @param delayCSSTransition the tooltip's time to appear after some (CSS transition) delay time
 * @param waitForCSSTransition if "true" (the default) 'msg' to show is to be synchronized wih rover's (any) movement CSS transition value
 *             If "false", means it's to execute right now; any needed delayed time was already provided by some setTimeout(), at the outside caller
 * @param toShow tooltip can be ordered to hide (false) or to shown (true). Last, if not passed in, is the default.
 */
export interface SpeakerVoice {
    msg: string;
    transformAtPos: Position;
    delayCSSTransition: number;
    waitForCSSTransition: boolean;
    toShow: boolean;
}
export const SpeakerVoiceInitializer = {
    msg: 'No "msg" was set up to show on Tooltip!'
    , transformAtPos: { coordinates: {x: 0, y: 0}, orientation: 'E' as OrientationChar}
    , delayCSSTransition: -1
    , waitForCSSTransition: true
    , toShow: true
};

// =================================================================
// DATA, through <network-data-point /> NETWORK
// =================================================================
// We can print all DATA JSONs at the 'monitor-comms' DATA bottom monitor:
export interface MonitorData {
    msg: string | null;
    data: Position | Instructions | VehicleInstructions | JourneyRoadMap | MoveReport | null;
}

// =================================================================
// VIDEO STREAMING, through <network-video-point /> NETWORK
// =================================================================
/**
 * 'monitor-comms' is also receiveing Rover's astronaut status to update Cockpit's camera image streaming on Sony big wide screen.
 *
 * Over there, to be able to set, and animate, any Cockpit camera / expedition scenario in a realistic way,
 * we also need information of what's happening on Mars - CSS 'transform'ations, HTML tag selectors and timmings envolved,
 * besides the Astronaut current (on this streaming frame) status (isDeployed...? isExploringMars...? ).
 */
export interface MonitorAstronaut {
    frameId?: number;    // Usefull when you have prepared some frame happening on a typical (Id indexed) ocasion/timming
    frameLabel: {
        localTime: string;
        text: string
    };
    frameToStream: VideoStreamFrame;
}
// Each complex MonitorAstronaut.frameToStream is made of:
export interface VideoStreamFrame {
    astronautPos?: AstronautPosition;
    astronautStatus?: AstronautStatus;
    videoScenario?: AstronautScenario;
}
// Init the MonitorAstronaut Object,
// so we don't have to keep enumerating all properties and to better trace debuging:
export const MonitorAstronautzeroPos: MonitorAstronaut = {
    frameId: -1
    , frameLabel: { localTime: '??', text: 'ERROR!, No strem set up, yet!'}
    , frameToStream: {
        astronautPos: {
            deploymentPlace: {
                place: 'NO place yet!'
                , coordinates: { x: 0, y: 0}
                , color: 'NO place color yet!'
            }
            , onMarsSoil: {
                coordinates: { x: 0, y: 0}
                , orientation: null
            }
            , byEarthCompass: {
                coordinates: { x: 0, y: 0}
                , orientation: null
            }
            , exploringTime: -1
        }
        , astronautStatus: {
            firstChange: false
            , isOnOrbiting: false
            , isOnCockpit: false
            , isDeployed: false
            , isExploringMars: false
        }
        , videoScenario: {
            videoStreamSetUp: {
                cId: -1
                , nId: -1
                , tImg: -1
            }
            , cssTags: '??'
            , cssTransforms: '??'
            , cssTimer: -1
        }
    }
};
/**
 * Astronaut Position is much complex than a common Position Earth one...
 * It depends if the Astronaut, at this current cumulated 'exploringTime' (time elapsed after Rover is deployed and have started its Mars exploration journey)
 * is touching, or not, Mars soil.
 *
 * Remember Mars is constantly (or can be, by user set of clicks) rotating/translating/zooming.
 * So the Astronaut should follow Mars soil cartesian or, if he/she is currently lifted up from the ground, the Earth compass.
 */
// The Types of each upper MonitorAstronaut.frameToStream:
export interface AstronautPosition {
    deploymentPlace?: DeploymentPlace;
    onMarsSoil: Position;
    byEarthCompass?: Position;
    exploringTime?: number;
}
export interface AstronautStatus {
    firstChange?: boolean;
    // ---------------- //
    isOnOrbiting?: boolean;
    isOnCockpit?: boolean;
    isDeployed?: boolean;
    isExploringMars?: boolean;
}
export interface AstronautScenario {
    videoStreamSetUp?: StreamConditions;
    cssTags?: string;
    cssTransforms?: string;
    cssTimer?: number;    // To be CSS usefull, needs to be converted in 'ms', or 's', string @ 'monitor-comms'
}

// =================================================================
// SPECIFIC Cockpit's VIDEO STREAMING, through <network-video-point /> NETWORK, moments
// =================================================================

/**
 * Each Rover's Cockpit 'camera' is defined by next interface.
 * On CSS sheet we define which image (CSS Class) goes to each Cockpit Camera, by seting its CSS 'background-image' url string.
 */
export interface CockpitCamera {
    id: number;
    cssImgClass: string;
    name: string;
}
/**
 * ... that can be streamed with some communication random noise type, on top.
 *
 * Again, on CSS sheet, you define each "noise" by setting different 'tunning-effect-X' classes
 * (animation: 'ImgComesAndGoes', with different 'animation-iteration-count' on 'opacity' property)
 * to then be added to (combined with) the CSS 'background-image' camera's Img Class of the (same) monitor's '.screen' content.
 */
export interface StreamCommNoise {
    id: number;
    cssNoiseEffectClass: string;
    cssAnimationTime: string;
}
/**
 * Any 'cId' Cockpit's Camera can be switched into, at any moment.
 *
 * Caller can decide if it has some 'nId' communications random noise to start with, or not,
 * allowing for the background scenario image to appear after also controlled 'tImg' ms.
 * So, besides noise, any 'StreamConditions' can already include some user set delay to, effectively, the screen has its image switched into.
 * ({..., nId: null, tImg: 1000})
 *
 * If 'nId'=null, no class noise is applied to the '.screen', and background img is removed from container.
 * Note that 'tImg' time still has to be set - minimum is right away, when method is called (0 ms)
 *
 * Communication's noise duration, and intensity, depend on the CSS code of 'tunning-effect-X' class.
 * It starts, if nId !==null, at the very moment camera is switched into.
 *
 */
export interface StreamConditions {
    cId: number;
    nId: number | null;
    tImg: number;
}
