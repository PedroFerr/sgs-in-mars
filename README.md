It´s needed a program to validate instructions that will be used by a new Rover in Mars.

Each Rover are included in a square and can receive the next commands:  
Advance (A), Turn left (L), Turn Right (R).  
The program must validate that the Rover be included into the edges of the square and must indicate the final orientation.

The program will receive the dimensions of the square (width x height) and it assumes that the coordinate (0,0) is the bottom left corner.  
Additionally, will receive initial coordinates of the Rover and it’s initial orientation (N, S, E, W).

Also it will receive a set of commands like the next one; “AALAARALA”.  
There is not fixed limit of number of input commands. It can be assumed that there is not obstacles into the square.

The program must validate that all the commands can be executed without be out of pre-defined initial limits and also must return True or False indicating if the commands are valid.  
Moreover, also must return the orientation and final coordinates of the Rover. As example: True, N, (4,5).

To run `rover-commands` Angular 7 App, go to that directory (`cd rover-commands`) and type `ng serve` command.  
Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### CONCLUSIONS: Architecture reached, at the end

Almost at the end of developing the whole thing, already reaching at the desire results, I thought:  
"How about using Angular component's Input/Output as if reality of the proposed scenario was happening in fact...?  
Like in a scientific fiction picture, with `command-center` communicating, both ways, with `app-root` and, in turns, `app-root` communicating with our `rover-in-mars`...?

Remember all we do @ `rover-in-mars` is driving through Mars!  
We receive Instructions and say "Journey is finish" at the end. Nothing more!  

On the other hand, `command-center` is dictating the Instructions to follow through the journey, and receiving long distance Mars FinalReport interface through `app-root`.  
They are blind, those scientific geeks; can't see our Rover, so far away from planet Earth it is now...

The Management Center is `app-root` which is able to manage both parts in this expedition: the scientific 'command-center' and the Rover vehicle @ Mars!

### New sub-domain opened @ Miles-NET server
http://rover-in-mars.miles-net.com/

Enjoy it!